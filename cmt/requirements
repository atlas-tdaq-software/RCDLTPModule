package RCDLTPModule

author  Thilo.Pauly@cern.ch
manager Thilo.Pauly@cern.ch

public
use TDAQPolicy
use Boost * TDAQCExternal
use vme_rcc
use DFDebug
use DataflowPolicy
use ROSCore
use ROSInterruptScheduler
use ROSIO
use DFSubSystemItem
use DFExceptions
use DFConfiguration
use ROSMemoryPool
use ROSEventFragment
use ROSBufferManagement
use is
use owl	
use cmdl
use omni
use oks
use ipc
use RCDMenu
use RCDVme
use RCDLtp
use rcc_time_stamp
use rcc_error
use genconfig
use dal
use ers
use RODBusyModule
use RunControl
use TriggerCommander
use TTCInfo
use asyncmsg

private
branches RCDLTPdal
macro generate-config-classes "LTPModule LTPConfigBase LTPPatternBase LTPSlaveConfig LTPBypassConfig LTPExpertConfig LTPPatgenBypassedConfig LTPLemoBypassedConfig LTPExpertBypassedConfig LTPLemoConfig LTPPatgenConfig LTPMasterTriggerSetup LTPCTPParasiteConfig LTPPattern LTPDeadtimePattern LTPPatternFile"
macro generate-config-include-dirs "${TDAQ_INST_PATH}/share/data/dal ${TDAQ_INST_PATH}/share/data/DFConfiguration ${TDAQ_INST_PATH}/share/data/RODBusydal ${TDAQ_INST_PATH}/share/data/RCDLTPdal $(datadir)/RODBusydal $(datadir)/share/data/RCDLTPdal"

document generate-config  RCDLTPdal -s=../schema/ namespace="RCDLTPdal" include="RCDLTPdal" packagename="RCDLTPdal" rcdltp.schema.xml

macro   gdir  "$(bin)RCDLTPdal.tmp"
library rcdltpdal $(gdir)/*.cpp
macro rcdltpdal_dependencies "RCDLTPdal RODBusydal"
macro rcdltpdal_shlibflags "-lrodbusydal"

application RCDLTPdal_dump -no_prototypes $(gdir)/dump/dump_RCDLTPdal.cpp
macro       RCDLTPdal_dumplinkopts "-lrodbusydal -lrcdltpdal -lipc"
macro       RCDLTPdal_dump_dependencies "rodbusydal rcdltpdal generate-dal"

ignore_pattern install_headers_bin_auto
apply_pattern install_headers src_dir="$(bin)/RCDLTPdal" files="*.hh *.h *.java" target_dir="../RCDLTPdal/"
apply_pattern install_libs files="librcdltpdal.so"
apply_pattern install_apps files="RCDLTPdal_dump"

private

library LTPModule -suffix=local "LTPMasterTTC2LAN.cpp LTPMasterBusy.cpp LTPMasterECR.cpp LTPMasterLumiBlock.cpp LTPModule.cpp"
macro LTPModule_dependencies   "rcdltpdal"

application checkLTP-slave-settings  "test/checkLTP-slave-settings.cpp"
application testLTPMasterBusy  "test/testLTPMasterBusy.cpp"
application testLTPMasterECR  "test/testLTPMasterECR.cpp"

macro LTPModule_shlibflags "-lDFDebug -lRCDVme -lvme_rcc -lRCDLtp -lrcdltpdal -lcmem_rcc -lio_rcc -lrcc_error -lROSCore -lROSUserActionScheduler -lis -lipc -lrcc_time_stamp -lrcc_error -lers -ltrgCommander -lasyncmsg"

macro LTPModule_cppflags "-DTSTAMP"

public

macro checkLTP-slave-settings_dependencies "RCDLtp"
macro checkLTP-slave-settingslinkopts "-lRCDLtp -lipc -lomniORB4 -lowl -lomnithread -lcmdline"

macro testLTPMasterBusy_dependencies "RCDLtp LTPModule"
macro testLTPMasterBusylinkopts "-lasyncmsg -lrcdltpdal -lRCDLtp -lLTPModule -lipc -lomniORB4 -lowl -lomnithread -lcmdline"

macro testLTPMasterECR_dependencies "RCDLtp LTPModule"
macro testLTPMasterECRlinkopts "-lasyncmsg -lrcdltpdal -lRCDLtp -lLTPModule -lipc -lomniORB4 -lowl -lomnithread -lcmdline"

apply_pattern install_libs files="libLTPModule.so"

apply_pattern install_data name=patgen     src_dir="../patgen"      files="*.dat" target_dir="patgen"

apply_pattern install_db_files name=segments   src_dir="../segments"   files="*.xml" target_dir="segments"
apply_pattern install_db_files name=partitions src_dir="../partitions" files="*.xml" target_dir="partitions"
apply_pattern install_db_files name=sw         src_dir="../sw"         files="*.xml" target_dir="sw"
apply_pattern install_db_files name=hw         src_dir="../hw"         files="*.xml" target_dir="hw"
apply_pattern install_db_files name=oks_schema src_dir="../schema"     files="rcdltp.schema.xml" target_dir="schema"

apply_pattern install_apps files="checkLTP-slave-settings"
apply_pattern install_apps files="testLTPMasterBusy"
apply_pattern install_apps files="testLTPMasterECR"

