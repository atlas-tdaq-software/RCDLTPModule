// -*- c++ -*-

/*
  ATLAS RCD Software

  Class: LTPModule: RCD Example Readout Module
  Authors: ATLAS RCD group 	

*/

#ifndef LTPMODULE_H
#define LTPMODULE_H

#include <string>
#include <vector>
#include <iostream>

#include "RCDLtp/RCDLtp.h"
#include "ROSCore/ReadoutModule.h"
#include "DFSubSystemItem/Config.h"

#include "TriggerCommander/MasterTrigger.h"

#include "RCDLTPModule/LTPMasterBusy.h"
#include "RCDLTPModule/LTPMasterECR.h"
#include "RCDLTPModule/LTPMasterLumiBlock.h"
#include "RCDLTPModule/LTPMasterTTC2LAN.h"

#include "RODBusydal/BusyChannel.h"
#include "RCDLTPdal/LTPBypassConfig.h"
#include "RCDLTPdal/LTPConfigBase.h"
#include "RCDLTPdal/LTPExpertConfig.h"
#include "RCDLTPdal/LTPLemoBypassedConfig.h"
#include "RCDLTPdal/LTPLemoConfig.h"
#include "RCDLTPdal/LTPPatgenConfig.h"
#include "RCDLTPdal/LTPMasterTriggerSetup.h"
#include "RCDLTPdal/LTPPatgenBypassedConfig.h"
#include "RCDLTPdal/LTPExpertBypassedConfig.h"
#include "RCDLTPdal/LTPSlaveConfig.h"
#include "RCDLTPdal/LTPCTPParasiteConfig.h"
#include "RCDLTPdal/LTPModule.h"
#include "RCDLTPdal/LTPPatternBase.h"

#include "ipc/partition.h"
#include "is/info.h"

namespace daq {
   namespace trigger {
      class CommandedTrigger;
   }
}

namespace RCD {

  using namespace ROS;

  class LTPModule : public ReadoutModule, public daq::trigger::MasterTrigger
  {
   public:

    /** The constructor should have no arguments */
    LTPModule();

    /** The destructor should have no arguments */
    virtual ~LTPModule() noexcept;

    /**
     * Set/update the values of the class internal variables
     * Called after the creator to initialize the class.
     * @param configuration contains the configuration parameters according
     *        to the class structure in the database schema
     */

    virtual const std::vector<DataChannel *> *channels ();

    virtual void setup (DFCountedPointer<Config> configuration);
    
    /** Reset internal statistics */

    virtual void clearInfo () ;
 
    /** Get performance statistics */

    virtual DFCountedPointer<Config> getInfo () ;

    /**
     * Inherited from the Controllable class. This methods
     * are called at state transitions. In general, only
     * needed methods have to be overloaded.
     * State transitions not related to RCD are omitted from this
     * list, but they are available if needed via the Controllable
     * base class.
     */

    virtual void configure(const daq::rc::TransitionCmd&);
    virtual void connect(const daq::rc::TransitionCmd&) ;
    virtual void prepareForRun(const daq::rc::TransitionCmd&);

    virtual void stopROIB(const daq::rc::TransitionCmd&);
    virtual void stopArchiving(const daq::rc::TransitionCmd&);

    virtual void publish();
    virtual void publishFullStats();
    
    virtual void disconnect(const daq::rc::TransitionCmd&);
    virtual void unconfigure(const daq::rc::TransitionCmd&);

    virtual void userCommand(std::string& commandName,
			     std::string& argument);

    // MasterTrigger related methods
    virtual daq::trigger::HoldTriggerInfo hold(const std::string& dm);
    virtual void resume(const std::string& dm);
    virtual void setPrescales(uint32_t, uint32_t hltp);
    virtual void setL1Prescales(uint32_t l1p);
    virtual void setHLTPrescales(uint32_t hltp);
    virtual void setBunchGroup(uint32_t bg);
    virtual void setConditionsUpdate(uint32_t folderIndex, uint32_t lb);
    virtual void increaseLumiBlock(uint32_t runno);
    virtual void setLumiBlockInterval(uint32_t seconds);
    virtual void setMinLumiBlockLength(uint32_t seconds);

   private:

    // helper function for disable/enable
    std::vector<std::string> parseCommaDeliminatedStrings(std::string s);

    void configureSlave(const RCDLTPdal::LTPSlaveConfig*);
    void configureBypass(const RCDLTPdal::LTPBypassConfig*);
    void configureExpert(const RCDLTPdal::LTPExpertConfig*);
    void configurePatgen(const RCDLTPdal::LTPPatgenConfig*);
    void configureLemo(const RCDLTPdal::LTPLemoConfig*);
    void configureExpertBypassed(const RCDLTPdal::LTPExpertBypassedConfig*);
    void configurePatgenBypassed(const RCDLTPdal::LTPPatgenBypassedConfig*);
    void configureLemoBypassed(const RCDLTPdal::LTPLemoBypassedConfig*);
    void configureCTPParasite(const RCDLTPdal::LTPCTPParasiteConfig*);

    void loadPattern(const RCDLTPdal::LTPPatternBase*);

    void publishTTC2LANInfo();
    void publishLTPInfo();

    void updateBusy(); // updates busy channels according to m_Busy_current vector
    void disableBusy(string& argument);
    void enableBusy(string& argument);

    // for database navigation
    IPCPartition m_ipcpartition;
    LTPMasterBusy* m_masterbusy;
    LTPMasterECR* m_masterecr;
    LTPMasterLumiBlock* m_masterlumiblock;
    LTPMasterTTC2LAN* m_masterttc2lan;

    daq::core::Partition* m_partition;
    Configuration* m_confDB;
    RCDLTPdal::LTPModule* m_dbModule;
    DFCountedPointer<Config> m_configuration;
    std::string m_ltpConfigType;

    // variables for busy
    bool m_Busy_local_with_linkin;
    bool m_Busy_local_with_ttlin;
    bool m_Busy_local_with_nimin;
    std::vector<bool> m_Busy_current;
    std::vector<std::string> m_Busy_uid;

    // control variables
    bool m_mastertrigger;
    // m_mastertrigger is the variable that decides whether or not the m_masterbusy object is
    // being used or not
    // m_mastertrigger is set to true in case the Module is registered as MasterTrigger in the
    // partition object, otherwise it is false

    bool m_ecr_at_resume;
    bool m_ecr_periodic;
    int m_ecr_period;
    bool m_useECR;

    u_int m_lumiblock_period;
    u_int m_lumiblock_mindistance;

    bool m_use_patgen;
    string m_PatGen_runmode;
    bool m_useTTC2LAN;
    bool m_useLB;

    // configuration parameters
    std::string m_UID;
    std::string m_rcd_uid;
    unsigned int m_PhysAddress;

    // configuration parameters
    bool m_boardReset;
    bool m_useConfiguration;
    bool m_useMonitoring; 
    std::string m_Monitoring_ISServerName;
    std::string m_Counter_selection;

    bool m_TTC2LAN_Activate;
    std::string  m_TTC2LAN_PollFrequency;
    u_int m_ttc2lan_pollperiod;
    bool m_TTC2LAN_DEBUG;
    std::string  m_TTC2LAN_DEBUG_Node;
    unsigned int m_TTC2LAN_DEBUG_PortNumber;

    unsigned int m_TTC2LAN_delta_time_ms ;
    unsigned int m_ECR_delta_time_ms ;

    // Status (0 if a given method was successful)
    unsigned int m_status;

    LTP* m_ltp;

    unsigned int m_runnumber;

    bool m_in_running_state;

    bool m_I_am_masterTriggerApp;
    bool m_I_am_TriggerModule;

    bool m_forceCommandedTrigger;
    daq::trigger::CommandedTrigger* m_commandedTrigger;


  };

  inline const std::vector<DataChannel *> *LTPModule::channels ()
  {
    return 0;
  }
}


#endif //LTPMODULE_H
