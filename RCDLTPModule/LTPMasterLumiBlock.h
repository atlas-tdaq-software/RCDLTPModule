#ifndef LTPMASTERLUMIBLOCK_H
#define LTPMASTERLUMIBLOCK_H

#include "ROSCore/ScheduledUserAction.h"

#include <stdint.h>
#include <mutex>
#include "owl/time.h"
#include "RCDLTPModule/LTPMasterLumiBlock.h"
#include "RCDLTPModule/LTPMasterBusy.h"
#include "RCDLTPModule/LTPMasterTTC2LAN.h"

#include "ipc/partition.h"



class LTPMasterLumiBlock : public ROS::ScheduledUserAction {

 public:

  LTPMasterLumiBlock(LTPMasterBusy& mb, LTPMasterTTC2LAN& mt, IPCPartition& p);
  virtual ~LTPMasterLumiBlock();

  virtual void requestIncreaseLBN(); // places a request
  virtual void getCurrentLBN(uint32_t& lbn, uint64_t& timestamp);
  virtual void setPeriod(uint32_t period_ms);
  virtual void setMinimumDistance(uint32_t distance_ms);
  virtual void enablePeriodicLumiBlocks();
  virtual void disablePeriodicLumiBlocks();
  virtual void enableLumiBlocks();
  virtual void disableLumiBlocks();
  virtual void reset(uint32_t run_number);
  virtual uint32_t getRunNumber();

  // called by the ScheduledUserAction
  virtual void reactTo();
  virtual void publishLumiBlockToIS();
  virtual void publishCounterDataToIS();
  
 private:
  void publishLBSettingsToIS();
  uint64_t getTimeNow();
  void increaseLBN();

  LTPMasterBusy* m_mb;
  LTPMasterTTC2LAN* m_mt;
  IPCPartition m_ipcpartition;

  bool m_lbchange_in_progress;

  uint64_t m_lb_period;
  uint64_t m_lb_mindistance;

  uint64_t m_next_lb_update;
  bool     m_next_lb_update_is_a_request;
  uint64_t m_veto_lb_update;
  bool m_periodic_lumiblocks_enabled;
  bool m_lumiblocks_enabled;

  uint64_t m_current_lb_starttime;
  uint32_t m_lbn;

  uint32_t m_runnumber;
  uint64_t m_nl1a_last;
  std::string m_dictname;

  std::mutex m_mutex;
};

#endif // LTPMASTERLUMIBLOCK_H
