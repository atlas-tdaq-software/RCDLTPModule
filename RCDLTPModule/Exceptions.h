/*
 * Exceptions.h
 *
 */

#ifndef LTPMODULE_EXCEPTIONS_H_
#define LTPMODULE_EXCEPTIONS_H_

#include "ers/ers.h"

namespace LTP {
  ERS_DECLARE_ISSUE(LTPModule, Exception, ERS_EMPTY, ERS_EMPTY)

    ERS_DECLARE_ISSUE_BASE
    (LTPModule,
     VMEError,
     LTPModule::Exception,
     "VME Error: Reason: " << reason,
     ERS_EMPTY,
     ((const char *)reason)
     )

    ERS_DECLARE_ISSUE_BASE
    (LTPModule,
     IPCLookup,
     LTPModule::Exception,
     "Failed to lookup " << server << " in IPC.",
     ERS_EMPTY,
     ((const char*) server)
     )

    ERS_DECLARE_ISSUE_BASE
    (LTPModule,
     LTPModuleIdentification,
     LTPModule::Exception,
     "LTPModule check failed. This seems not to be an LTP module. Reason: " << reason,
     ERS_EMPTY,
     ((const char *)reason )
     )

    ERS_DECLARE_ISSUE_BASE
    (LTPModule,
     LTPModulePublishToIS,
     LTPModule::Exception,
     "LTPModule could not publish to IS. Reason: " << reason,
     ERS_EMPTY,
     ((const char *)reason )
     )

    ERS_DECLARE_ISSUE_BASE
    (LTPModule,
     LTPModuleFailedFirstLumiBlock,
     LTPModule::Exception,
     "LTPModule didn't retrieve reasonable RunParams.LumiBlock publication for first LumiBlock LBN=0. Reason: " << reason,
     ERS_EMPTY,
     ((const char *)reason )
     )


    ERS_DECLARE_ISSUE_BASE
    (LTPModule,
     LTPModuleFailedIncreaseLBN,
     LTPModule::Exception,
     "Could not increase LBN because LumiBlocks are disabled: " << reason,
     ERS_EMPTY,
     ((const char*) reason)
     )


    /* ERS_DECLARE_ISSUE_BASE */
    /* (LTPModule, */
    /*  CmdlParsing, */
    /*  LTPModule::Exception, */
    /*  "Parsing of command line parameters failed: " << reason, */
    /*  ERS_EMPTY, */
    /*  ((const char*) reason) */
    /* ) */
    }

#endif /* LTPMODULE_EXCEPTIONS_H_ */
