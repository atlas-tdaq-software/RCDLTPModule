#ifndef LTPMASTERTTC2LAN_H
#define LTPMASTERTTC2LAN_H

#include "ROSCore/ScheduledUserAction.h"

#include <vector>
#include <stdint.h>
#include <mutex>
#include "ipc/partition.h"

#include "owl/time.h"

#include "RCDLTPModule/LTPMasterECR.h"
#include "RCDLTPModule/LTPMasterBusy.h"

#include "asyncmsg/Server.h"
#include "asyncmsg/Session.h"
#include "asyncmsg/Message.h"
#include "asyncmsg/NameService.h"
#include "asyncmsg/UDPSession.h"

#include <memory>
#include <thread>
#include <atomic>

#include "ers/ers.h"

#include "is/infoT.h"
#include "is/infodictionary.h"


namespace RCD {
  class LTP;
}


/** ********************************************************************
 * helper classes for daq::asyncmsg communication used by TTC2LAN
 * ********************************************************************/

enum class XONOFFStatus : uint32_t { OFF = 0, ON = 1 };

/**
 * Message from HLTSV to TTC2LAN.
 *
 * Change XON/XOFF status. Used to throttle the sender.
 * Payload: a single 32bit word, 0 = OFF, 1 == ON.
 */
class XONOFF : public daq::asyncmsg::InputMessage {
 public:
  
  // This must agree with what the HLTSV uses
  static const uint32_t ID = 0x00DCDF50;
  
 XONOFF()
   : m_on_off(XONOFFStatus::ON)
    {}
  
  // Boiler plate for messages.
  
  // The message identifier
  uint32_t typeId() const override
  {
    return ID;
  }
  
  // The transaction ID, not used.
  uint32_t transactionId() const override
  {
    return 0;
  }
  
  // Provide receive buffers.
  void toBuffers(std::vector<boost::asio::mutable_buffer>& buffers) override
  {
    buffers.push_back(boost::asio::buffer(&m_on_off, sizeof(m_on_off)));
  }
  
  // Message specific
  
  // Access received data, could also make variable just public in such
  // a simple case.
  XONOFFStatus status() const
  {
    return m_on_off;
  }
  
 private:
  XONOFFStatus m_on_off;
};

//
// The Session class to talk to the HLTSV.
//
// The constructor takes a reference to a variable that it
// updates with the XON/XOFF status as it receives messages.
// This is the way it interacts with the rest of the application.
//
class Session : public daq::asyncmsg::Session {
 public:
 Session(boost::asio::io_service& service,
	 std::atomic<XONOFFStatus>& status, uint32_t &xonCnt, uint32_t &xoffCnt,
	 LTPMasterBusy* mb)
   : daq::asyncmsg::Session(service),
    m_status(status),
    m_hltsv_xon_count(xonCnt),
    m_hltsv_xoff_count(xoffCnt),
    m_mb(mb)
    {
    }

  // no copying please, just use the shared_ptr<Session>
  Session(const Session&) = delete;
  Session& operator=(const Session& ) = delete;

 protected:

  void onOpen() noexcept override 
  {
    ERS_LOG("Session is open:" << remoteEndpoint());

    // initiate first receive
    asyncReceive();
  }

  void onOpenError(const boost::system::error_code& error) noexcept override 
  {
    ERS_LOG("Open error..." << error);
  }

  void onClose() noexcept override
  {
    ERS_LOG("Session is closed");
  }

  void onCloseError(const boost::system::error_code& error) noexcept override
  {
    ERS_LOG("Close error..." << error);
  }

  //
  // This is called when the message header has been parsed. 
  // It gives us the chance to create an object specific for the message type.
  //
  // We only expect a XONOFF message, so that's all we check.
  //
  std::unique_ptr<daq::asyncmsg::InputMessage> 
    createMessage(std::uint32_t typeId,
		  std::uint32_t /* transactionId */, 
		  std::uint32_t size) noexcept override
    {
      ERS_ASSERT_MSG(typeId == XONOFF::ID, "Unexpected type ID in message: " << typeId);
      ERS_ASSERT_MSG(size == sizeof(uint32_t), "Unexpected size: " << size);
      ERS_DEBUG(3,"createMessage, size = " << size);

      // all ok, create a new XONOFF object
      return std::unique_ptr<XONOFF>(new XONOFF());
    }

  // 
  // This is called when the full message has been received.
  void onReceive(std::unique_ptr<daq::asyncmsg::InputMessage> message) override
  {
    ERS_DEBUG(3,"onReceive");
    auto msg = std::unique_ptr<XONOFF>(dynamic_cast<XONOFF*>(message.release()));
        
    if(msg == 0) {
      ERS_LOG("Received invalid message type ");
      return;
    }

    m_status = msg->status();
    if (m_status == XONOFFStatus::OFF){
      ERS_DEBUG(3, "Activating LTP master busy: setHLTSVBusy()");
      m_mb->setHLTSVBusy();
      m_hltsv_xoff_count++;
    } else {
      ERS_DEBUG(3, "Releasing LTP master busy: unsetHLTSVBusy()");
      m_mb->unsetHLTSVBusy();
      m_hltsv_xon_count++;
    }
    ERS_DEBUG(1, "Got XONOFF message, new status = " << (m_status == XONOFFStatus::ON ? "ON" : "OFF"));
        
    // Initiate next receive
    asyncReceive();
  }

  void onReceiveError(const boost::system::error_code& error,
		      std::unique_ptr<daq::asyncmsg::InputMessage> /* message */) noexcept override
  {
    ERS_LOG("Receive error: " << error);
  }

  void onSend(std::unique_ptr<const daq::asyncmsg::OutputMessage> /* message */) noexcept override
  {
    // Let the unique_ptr<> delete the message
  }

  void onSendError(const boost::system::error_code& error,
		   std::unique_ptr<const daq::asyncmsg::OutputMessage> /* message */) noexcept override
  {
    ERS_LOG("Send error: " << error);
    // throwing in the towel here, for this test program...
    abort();
  }

 private:
  std::atomic<XONOFFStatus>& m_status;
  std::atomic<bool>          m_running;
  uint32_t &m_hltsv_xon_count;
  uint32_t &m_hltsv_xoff_count;
  LTPMasterBusy* m_mb;
};



class LTPMasterTTC2LAN : public ROS::ScheduledUserAction {

 public:

  LTPMasterTTC2LAN(LTPMasterBusy& mb, LTPMasterECR& me, IPCPartition& p);
  LTPMasterTTC2LAN(RCD::LTP& ltp, LTPMasterBusy& mb, LTPMasterECR& me, IPCPartition& p);
  virtual ~LTPMasterTTC2LAN();
  virtual void setLTP(RCD::LTP& ltp);

  // will need a few methods to set up the connection

  void connect();
  void disconnect();
  
  virtual void reset();
  virtual void enableSending();
  virtual void disableSending();
  virtual bool firstL1IDIsSet();
  virtual uint32_t getFirstL1ID();
  virtual void unsetFirstL1ID();
  virtual uint32_t getLastL1ID();
  virtual uint64_t getNumberOfTriggers();

  // called by the ScheduledUserAction
  virtual void reactTo();
  virtual void publishToIS();

 private:

  void sendMessage(uint32_t l1id);

  LTPMasterBusy* m_mb;
  LTPMasterECR* m_me;
  RCD::LTP* m_ltp;
  IPCPartition m_ipcpartition;
  /* std::unique_ptr<ISInfoDictionary> m_dict; */
  std::vector<uint32_t> m_l1id_list;
  std::vector<uint32_t> m_lbn_list;
  std::mutex m_mutex; // used for general shared member variables

  bool m_enable_sending;
  
  // statistics
  uint64_t m_l1a;
  uint32_t m_first_l1id;
  bool m_first_l1id_is_set;
  uint32_t m_last_l1id;
  uint32_t m_number_of_messages;
  bool m_hltsv_xon;
  uint32_t m_hltsv_xon_count;
  uint32_t m_hltsv_xoff_count;
  
  // TTC2LAN objects
  std::thread                                    m_io_thread;
  boost::asio::io_service                        m_service;
  std::unique_ptr<boost::asio::io_service::work> m_work;
  std::shared_ptr<Session>                       m_session;
  std::atomic<XONOFFStatus>                      m_status;
};

#endif // LTPMASTERTTC2LAN_H
