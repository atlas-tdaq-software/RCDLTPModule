#ifndef LTPMASTERECR_H
#define LTPMASTERECR_H

#include "ROSCore/ScheduledUserAction.h"

#include <vector>
#include <stdint.h>
#include <mutex>
#include "ipc/partition.h"

#include "owl/time.h"


namespace RCD {
  class LTP;
}

class LTPMasterBusy;

class LTPMasterECR : public ROS::ScheduledUserAction {

 public:

  LTPMasterECR(uint32_t period_ms, LTPMasterBusy& mb, IPCPartition& p);
  LTPMasterECR(uint32_t period_ms, RCD::LTP& ltp, LTPMasterBusy& mb, IPCPartition& p);
  virtual ~LTPMasterECR();

  virtual void setLTP(RCD::LTP& ltp);
  virtual void issueECR();
  virtual bool ECRInProgress(); // returns true, if the class is in the progress of issueing an ECR, including the pre- and post- deadtime
  virtual void startAutomaticECR();
  virtual void stopAutomaticECR();
  virtual bool isInAutomaticMode();
  virtual uint32_t numberOfECRs();
  virtual void reset();
  virtual void resetNumberOfECRs(uint32_t necr=0);
  virtual void setDeadtime(uint32_t pre_ecr_deadtime_bc, uint32_t post_ecr_deadtime_bc);

  // pollL1ID is a request to poll the L1ID register and add
  // the current L1ID to the L1ID list.
  virtual void pollL1ID();

  virtual void getL1IDList(std::vector<uint32_t>& l1id_list, std::vector<uint32_t>& lbn_list);
  virtual void resetL1IDList();
  virtual size_t getSizeL1IDList();
  virtual void getLatestL1ID(uint32_t& l1id, uint32_t& lbn);
  virtual void setLBN(uint32_t lbn);

  // called by the ScheduledUserAction
  virtual void reactTo();

  virtual void publishToIS();

 private:
  void readCounter();


  LTPMasterBusy* m_mb;
  RCD::LTP* m_ltp;
  IPCPartition m_ipcpartition;
  std::vector<uint32_t> m_lbn_list;
  std::vector<uint32_t> m_l1id_list;
  uint32_t m_nl1a;
  uint32_t m_last_ec;
  uint32_t m_necr;
  bool m_ecr_in_progress;
  bool m_automatic_mode;
  uint32_t m_pre_ecr_deadtime_bc;
  uint32_t m_post_ecr_deadtime_bc;
  uint32_t m_latest_l1id;
  uint32_t m_lbn;

  std::mutex m_mutex; // used for general shared member variables
};

#endif // LTPMASTERECR_H
