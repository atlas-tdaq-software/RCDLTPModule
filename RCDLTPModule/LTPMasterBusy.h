#ifndef LTPMASTERBUSY_H
#define LTPMASTERBUSY_H


#include <list>
#include <stdint.h>
#include <mutex>
#include "owl/time.h"
#include "ipc/partition.h"


namespace RCD {
  class LTP;
}

class LTPMasterBusy {

 public:
  LTPMasterBusy(IPCPartition& p);
  LTPMasterBusy(RCD::LTP& ltp, IPCPartition& p);
  virtual ~LTPMasterBusy();

  virtual void setLTP(RCD::LTP& ltp);

  // resets the busies to a safe state, with
  // triggerbusy on and runcontrolbusy=1
  virtual void reset();
  virtual void dump();

  virtual void setTriggerBusy();
  virtual void unsetTriggerBusy();
  virtual void setRunControlBusy();
  virtual void setRunControlBusy(int counter);
  virtual void unsetRunControlBusy();
  virtual void setLumiBlockBusy();
  virtual void unsetLumiBlockBusy();
  virtual void setECRBusy();
  virtual void unsetECRBusy();
  virtual void setHLTCounterBusy();
  virtual void unsetHLTCounterBusy();
  virtual void setHLTSVBusy();
  virtual void unsetHLTSVBusy();

  virtual bool isBusy();
  virtual bool isTriggerBusy();
  virtual uint32_t isRunControlBusy();
  virtual bool isLumiBlockBusy();
  virtual bool isECRBusy();
  virtual bool isHLTCounterBusy();
  virtual bool isHLTSVBusy();
  virtual bool isBusy(bool& trigger, 
		      uint32_t& runcontrol, 
		      bool& lumiblock, 
		      bool& ecr,
		      bool& hltcounter,
		      bool& hltsv);
		      
  virtual bool ltpConstantBusyAsserted();
  virtual void publishToIS();

 private:
  RCD::LTP* m_ltp;
  IPCPartition m_ipcpartition;

  // internal busy channels
  bool m_bc_trigger;
  uint32_t m_bc_runcontrol;
  bool m_bc_lumiblock;
  bool m_bc_ecr;
  bool m_bc_hltcounter;
  bool m_bc_hltsv;
  bool m_globalbusy;

  void updateGlobalBusy();
  std::mutex m_mutex;

};

#endif // LTPMASTERBUSY_H
