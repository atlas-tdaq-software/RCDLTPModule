#include "RunControl/Common/OnlineServices.h"

#include "RCDLtp/RCDLtp.h"
#include "RCDLTPModule/Exceptions.h"
#include "RCDLTPModule/LTPMasterTTC2LAN.h"

#include "is/info.h"
#include "is/infodictionary.h"
#include "TTCInfo/TTC2LAN_IS.h"

using namespace std;


/** ********************************************************************
 * helper classes for daq::asyncmsg communication used by TTC2LAN
 * ********************************************************************/


/**
 * Message from TTC2LAN to HLTSV.
 *
 * Event update.
 * Payload: a single extendend L1 ID (32bit)
 */
class Update : public daq::asyncmsg::OutputMessage {
public:
  
  // Must be same in HLTSV
  static const uint32_t ID = 0x00DCDF51;
  
  explicit Update(uint32_t l1id)
    : m_l1_id(l1id)
  {
  }
  
  // Message boiler plate
  uint32_t typeId() const override
  {
    return ID;
  }
  
  uint32_t transactionId() const override
  {
    return 0;
  }
  
  void toBuffers(std::vector<boost::asio::const_buffer>& buffers) const override
  {
    buffers.push_back(boost::asio::buffer(&m_l1_id, sizeof(m_l1_id)));
  }
  
private:
  
  uint32_t m_l1_id;
  
};


/** ********************************************************************
 * class implementing the TTC2LAN sending of L1IDs
 * ********************************************************************/


LTPMasterTTC2LAN::LTPMasterTTC2LAN(LTPMasterBusy& mb,
                                   LTPMasterECR& me,
                                   IPCPartition& p)
  : ScheduledUserAction(100), // 10 Hz default
    m_mb(&mb),
    m_me(&me),
    m_ltp(nullptr),
    m_ipcpartition(p),
    // m_dict(new ISInfoDictionary(m_ipcpartition)),
    m_l1id_list(0),
    m_lbn_list(0),
    m_enable_sending(false),
    m_l1a(0),
    m_first_l1id(0xffffffff),
    m_first_l1id_is_set(false),
    m_last_l1id(0x00ffffff),
    m_number_of_messages(0),
    m_hltsv_xon(true),
    m_hltsv_xon_count(0),
    m_hltsv_xoff_count(0),
    m_status(XONOFFStatus::ON)
{
  ERS_LOG("LTPMasterTTC2LAN::LTPMasterTTC2LAN(): m_status = "<< (m_status == XONOFFStatus::ON) );
  if (m_mb == nullptr) {
    ERS_LOG("LTPMasterTTC2LAN::LTPMasterTTC2LAN(): m_mb == nullptr! No valid master Busy object passed to constructor! Aborting ...");
    throw LTP::LTPModule::LTPModulePublishToIS(ERS_HERE, "LTPMasterTTC2LAN::LTPMasterTTC2LAN() ", "Invalid master busy object passed to constructor");
  }
}

LTPMasterTTC2LAN::LTPMasterTTC2LAN(RCD::LTP& ltp,
                                   LTPMasterBusy& mb,
                                   LTPMasterECR& me,
                                   IPCPartition& p)
  : ScheduledUserAction(100), // 10 Hz default
    m_mb(&mb),
    m_me(&me),
    m_ltp(&ltp),
    m_ipcpartition(p),
    m_l1id_list(0),
    m_lbn_list(0),
    m_enable_sending(false),
    m_l1a(0),
    m_first_l1id(0xffffffff),
    m_first_l1id_is_set(false),
    m_last_l1id(0x00ffffff),
    m_number_of_messages(0),
    m_hltsv_xon(true),
    m_hltsv_xon_count(0),
    m_hltsv_xoff_count(0),
    m_status(XONOFFStatus::ON)

{
  ERS_LOG("LTPMasterTTC2LAN::LTPMasterTTC2LAN(): m_status = "<< (m_status == XONOFFStatus::ON) );
  if (m_mb == nullptr) {
    ERS_LOG("LTPMasterTTC2LAN::LTPMasterTTC2LAN(): m_mb == nullptr! No valid master Busy object passed to constructor! Aborting ...");
    throw LTP::LTPModule::LTPModulePublishToIS(ERS_HERE, "LTPMasterTTC2LAN::LTPMasterTTC2LAN() ", "Invalid master busy object passed to constructor");
  }
}

LTPMasterTTC2LAN::~LTPMasterTTC2LAN() {
  reset();
}


void LTPMasterTTC2LAN::disconnect()
{
  ERS_LOG("Closing async message session ...");
  m_session->asyncClose();
  ERS_LOG("... asyncClose() called. Waiting until session reports CLOSED.");
  
  // We should not return from this transition before the session is open;
  // this is rather clumsy but since we have only one connection...
  while(m_session->state() != daq::asyncmsg::Session::State::CLOSED) {
    usleep(10000);
  }
  ERS_LOG(" ... Session closed.");
  
  
  m_service.stop();
  
  ERS_LOG("joining io_thread");
  m_io_thread.join();
  ERS_LOG("resetting worker");
  m_work.reset();
}

void LTPMasterTTC2LAN::connect() {
  ERS_LOG("setting up daq::asyncmesg connection with TTC2LANReceiver (in hltsv)");
  
  // Create the name service object to lookup the HLTSV address
  daq::asyncmsg::NameService name_service(m_ipcpartition, std::vector<std::string>());
  
  
  // Do the lookup, the HLTSV will publish its endpoint under 'TTC2LANReceiver'.
  // In IS you can see it as the object 'DF.MSG_TTC2LANReceiver'.
  // This will throw an exception if it can't find the name entry.
  auto addr = name_service.resolve("TTC2LANReceiver");
  
  m_work.reset(new boost::asio::io_service::work(m_service));
  
  // Start the Boost.ASIO thread
  m_io_thread = std::thread([&]() { ERS_LOG("io_service starting"); m_service.run(); ERS_LOG("io_service finished"); });
  
  // Create the session with the endpoint we just got
  m_session = std::make_shared<Session>(m_service, m_status, m_hltsv_xon_count, m_hltsv_xoff_count, m_mb);
  m_session->asyncOpen(daq::rc::OnlineServices::instance().applicationName(), addr);
  
  // We should not return from this transition before the session is open;
  // this is rather clumsy but since we have only one connection...
  
  while(m_session->state() != daq::asyncmsg::Session::State::OPEN) {
    usleep(10000);
  }
  ERS_LOG("LTPMasterTTC2LAN::connect(): m_status = "<< (m_status == XONOFFStatus::ON) );
  ERS_LOG("completed!");
}

void LTPMasterTTC2LAN::setLTP(RCD::LTP& ltp) {
  m_mutex.lock();
  m_ltp = &ltp;
  m_mutex.unlock();
}

void LTPMasterTTC2LAN::reset() {
  m_mutex.lock();
  m_l1id_list.clear();
  m_lbn_list.clear();
  m_enable_sending = false;
  m_l1a = 0;
  m_first_l1id = 0xffffffff;
  m_first_l1id_is_set = false;
  m_last_l1id = 0x00ffffff;
  m_number_of_messages = 0;
  m_hltsv_xon = true;
  m_hltsv_xon_count = 0;
  m_hltsv_xoff_count = 0;
  m_mutex.unlock();
}

void LTPMasterTTC2LAN::reactTo() {
  m_mutex.lock();
  if (m_me != nullptr) {
     m_me->pollL1ID();
    m_me->getL1IDList(m_l1id_list, m_lbn_list);
  }
  // go through the list of l1ids
  // send one message to HLTSV per ECR period and for the last L1ID
  // keep statistics
  
  // in case a new lumiblock has been started make the information
  // available for pick-up
  
  int list_size = m_l1id_list.size();
  int send=false;
  for (int i = 0; i<list_size; ++i) {
    uint32_t l1id = m_l1id_list[i];
    uint32_t ecrc = (l1id>>24)&0xffu;
    uint32_t ec = (l1id & 0xffffffu);
    uint32_t last_ecrc = (m_last_l1id>>24)&0xffu;
    if (!m_first_l1id_is_set) {
      if (ec==0xffffffu) {
        // in the case we haven't received any trigger yet, we keep the current
        // l1id, but keep the flag false, so that the first trigger can still
        // recorded in m_first_l1id
        m_first_l1id = l1id;
        m_first_l1id_is_set = false;
      } else {
        // ERS_LOG(" xxx setting first_l1id = " << l1id);
        // compare with last l1id
        if (last_ecrc == ecrc) {
          uint32_t last_ec = (m_last_l1id & 0xffffffu);
          m_first_l1id = ((last_ec+1) | ((ecrc<<24)&0xff000000u));
        } else {
          m_first_l1id = (l1id&0xff000000u);
        }
        m_first_l1id_is_set = true;
      }
    }
    // do only something if we get a new l1id
    if ((l1id != m_last_l1id) || (ec != 0xffffffu)) {
      if (i==(list_size-1) && (l1id<<8 != 0xffffff00) ) {
        // last element in the list: send message, but only if the EC updated. Do nothing if only an ECR happened
        send=true;
      } else {
        // check the next l1id
        uint32_t next_l1id = m_l1id_list[i+1];
        uint32_t next_ecrc = (next_l1id>>24)&0xff;
        
        if (ecrc != next_ecrc && l1id<<8 != 0xffffff00) {
          send=true;
          // ERS_LOG("ECR detected: will send l1id = "<<std::hex<<l1id);
        }
      }
      // printf("xxx l1id = 0x%08x   %d  send=%d\n", l1id, ec, send?1:0);
      
      if (send) {
        // we are going to send the L1ID via TTC2LAN
        
        // do some accounting first
        // event counter (+1 in order to get the real count)
        uint32_t n_ec = ((l1id+1) & 0xffffffu);
        uint32_t n_ec_last = ((m_last_l1id+1) & 0xffffffu);
        
        if (ecrc == last_ecrc) {
          // same ECR period: subtract the two ECs from each other to get
          // the number of triggers
          m_l1a += static_cast<uint64_t>(n_ec-n_ec_last);
        } else {
          // different ECR period: take the EC to get the number of triggers
          m_l1a +=  static_cast<uint64_t>(n_ec);
        }
        if (m_enable_sending) {
          this->sendMessage(l1id);
          ++m_number_of_messages;
        }
        m_last_l1id = l1id;
        send=false;
      }
      // printf("xxx m_l1a = %d\n", m_l1a);
    }
  }
  
  m_l1id_list.clear();
  m_lbn_list.clear();
  m_mutex.unlock();
  return;
}

void LTPMasterTTC2LAN::sendMessage(uint32_t l1id) {
  // this method is not allowed to use m_mutex nor methods that use m_mutex
  
  string sout("LTPMasterTTC2LAN::sendMessage");
  char helper[30];
  sprintf(helper,"0x%08x", l1id);
  ERS_LOG(sout << " called with L1ID = " << helper);
  
  //if(m_status == XONOFFStatus::ON) { // ALWAYS sent! XOFF must set LTP Busy, which stops L1A generation. Remaining L1IDs must be seint!
  std::unique_ptr<Update> msg(new Update(l1id));
  m_session->asyncSend(std::move(msg));
  
  //} else {
  //ERS_LOG("new L1ID not sent becaus XONOFF = off");
  //}
}

void LTPMasterTTC2LAN::publishToIS() {
  m_mutex.lock();
  string sout("LTPMasterTTC2LAN::publishToIS() ");
  try {
    TTC2LAN_IS is_entry;
    is_entry.L1A = m_l1a;
    is_entry.Last_L1ID = m_last_l1id;
    is_entry.TTC2LAN_enabled = m_enable_sending;
    is_entry.Number_of_messages = m_number_of_messages;
    is_entry.XON = (m_status == XONOFFStatus::ON);
    is_entry.XON_count = m_hltsv_xon_count;
    is_entry.XOFF_count = m_hltsv_xoff_count;

    ISInfoDictionary dict(m_ipcpartition);
    dict.checkin("DF.TTC2LAN", is_entry, true);
  } catch (CORBA::SystemException& ex) {
    ERS_LOG(sout << "Could not publish to IS: CORBA::SystemException ex._name()=" << ex._name());
    
    throw LTP::LTPModule::LTPModulePublishToIS(ERS_HERE, "LTPMasterTTC2LAN::publishToIS() ", ex._name());
    
  } catch (daq::is::Exception & ex) {
    ERS_LOG(sout << "Could not publish to IS: daq::is::Exception ex.what()=" << ex.what());
    throw LTP::LTPModule::LTPModulePublishToIS(ERS_HERE, "LTPMasterTTC2LAN::publishToIS() ", ex.what());
  } catch (std::exception& ex) {
    ERS_LOG(sout << "Could not publish to IS: std::exception ex.what()=" << ex.what());
    throw LTP::LTPModule::LTPModulePublishToIS(ERS_HERE, "LTPMasterTTC2LAN::publishToIS() ", ex.what());
  } catch (...) {
    ERS_LOG(sout << "Could not publish to IS. Unknown exception. Rethrowing.");
    throw LTP::LTPModule::LTPModulePublishToIS(ERS_HERE, "LTPMasterTTC2LAN::publishToIS() ", "Unknown Exception");
  }
 
  m_mutex.unlock();
  return;
}

void LTPMasterTTC2LAN::enableSending() {
  m_mutex.lock();
  m_enable_sending = true;
  m_mutex.unlock();
}

void LTPMasterTTC2LAN::disableSending() {
  m_mutex.lock();
  m_enable_sending = false;
  m_mutex.unlock();
}

bool LTPMasterTTC2LAN::firstL1IDIsSet() {
  m_mutex.lock();
  bool ret = m_first_l1id_is_set;
  m_mutex.unlock();
  return(ret);
}

uint32_t LTPMasterTTC2LAN::getFirstL1ID() {
  m_mutex.lock();
  uint32_t ret = m_first_l1id;
  m_mutex.unlock();
  return(ret);
}

void LTPMasterTTC2LAN::unsetFirstL1ID() {
  m_mutex.lock();
  m_first_l1id_is_set = false;
  m_first_l1id = 0xffffffff;
  m_mutex.unlock();
}

uint32_t LTPMasterTTC2LAN::getLastL1ID() {
  m_mutex.lock();
  uint32_t ret = m_last_l1id;
  m_mutex.unlock();
  return(ret);
}

uint64_t LTPMasterTTC2LAN::getNumberOfTriggers() {
  m_mutex.lock();
  uint64_t ret = m_l1a;
  m_mutex.unlock();
  return(ret);
}

