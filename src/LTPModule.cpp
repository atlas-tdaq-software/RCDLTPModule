/*  
    ATLAS RCD Software

    Class: LTPModule
    Author: Thilo Pauly

*/

#include <iostream>
#include <sstream>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <list>

#include "config/Configuration.h"
#include "dal/MasterTrigger.h"
#include "dal/Resource.h"
#include "dal/Detector.h"
#include "dal/Partition.h"
#include "dal/util.h"
#include "DFdal/TTC2LAN.h"
#include "DFdal/RCD.h"

#include "RCDLTPdal/LTPPattern.h"
#include "RCDLTPdal/LTPPatternFile.h"
#include "RCDLTPdal/LTPDeadtimePattern.h"

#include "RCDLTPModule/Exceptions.h"
#include "RCDLTPModule/LTPModule.h"

#include "TTCInfo/LTP_ISNamed.h"

#include "RCDLtp/RCDLtp.h"

#include "TriggerCommander/CommandedTrigger.h"
#include "TriggerCommander/Exceptions.h"

#include "ers/ers.h"
#include "owl/time.h"
#include "rcc_time_stamp/tstamp.h"
#include "rcc_error/rcc_error.h"

using ::LTP::LTPModule::LTPModuleFailedFirstLumiBlock;
using ::LTP::LTPModule::LTPModuleFailedIncreaseLBN;
using namespace RCD;
using namespace ROS;

/******************************************************/
LTPModule::LTPModule () :
  m_ipcpartition(getenv("TDAQ_PARTITION")),
  m_masterbusy(nullptr),
  m_masterecr(nullptr),
  m_masterlumiblock(nullptr),
  m_masterttc2lan(nullptr),
  m_partition(nullptr),
  m_confDB(nullptr),
  m_dbModule(nullptr),
  m_configuration(),
  m_ltpConfigType(""),
  m_Busy_local_with_linkin(false),
  m_Busy_local_with_ttlin(false),
  m_Busy_local_with_nimin(false),
  m_Busy_current(3,false),
  m_Busy_uid(3,"not_used"),
  m_mastertrigger(false),
  m_ecr_at_resume(false),
  m_ecr_periodic(false),
  m_ecr_period(5),
  m_useECR(false),
  m_lumiblock_period(60),
  m_lumiblock_mindistance(10),
  m_use_patgen(false),
  m_PatGen_runmode(RCDLTPdal::LTPExpertConfig::PatGen_runmode::Off),
  m_useTTC2LAN(false),
  m_UID (""),
  m_rcd_uid(""),
  m_PhysAddress(0),
  m_boardReset(false),
  m_useConfiguration(false),
  m_useMonitoring(false), 
  m_Monitoring_ISServerName(""),
  m_Counter_selection(""),
  m_TTC2LAN_Activate(false),
  m_TTC2LAN_PollFrequency(""),
  m_ttc2lan_pollperiod(0),
  m_TTC2LAN_DEBUG(false),
  m_TTC2LAN_DEBUG_Node(""),
  m_TTC2LAN_DEBUG_PortNumber(0),
  m_TTC2LAN_delta_time_ms(0),
  m_status(0),
  m_ltp(nullptr),
  m_runnumber(0),
  m_in_running_state(false),
  m_I_am_masterTriggerApp(false),
  m_I_am_TriggerModule(false),
  m_forceCommandedTrigger(false),
  m_commandedTrigger(0)
{
  std::string sout = m_UID + "@LTPModule::LTPModule() ";
  ERS_LOG(sout << "Entered");

  m_TTC2LAN_delta_time_ms = 0;

  char* gfm = getenv("FORCE_MASTERTRIGGER");
  if (gfm) {
    string sgfm(gfm);
    ERS_LOG(sout << "Found FORCE_MASTERTRIGGER = \"" << sgfm << "\"");
    if ( (sgfm!="") && (sgfm==getenv("TDAQ_APPLICATION_NAME")) ) {
      ERS_LOG(sout << "Forcing this module to instantiate a CommandedTrigger object");
      m_forceCommandedTrigger=true;
    }
  }

  ERS_LOG(sout << "Done");
}

/******************************************************/
LTPModule::~LTPModule () noexcept
/******************************************************/
{
  std::string sout = m_UID + "@LTPModule::~LTPModule() ";
  ERS_LOG(sout << "Entered");

  if (m_commandedTrigger) {
    m_commandedTrigger->_destroy();
    m_commandedTrigger=0;
  }
  
  if (m_masterbusy != nullptr) {
    ERS_LOG(sout << "About to delete m_masterbusy = " << std::hex << m_masterbusy);
    delete m_masterbusy;
    m_masterbusy=nullptr;
  }
  
  if (m_masterecr != nullptr) {
    ERS_LOG(sout << "About to delete m_masterecr = " << std::hex << m_masterecr);
    delete m_masterecr;
    m_masterecr=nullptr;
  }

  if (m_masterlumiblock != nullptr) {
    ERS_LOG(sout << "About to delete m_masterlumiblock = " << std::hex << m_masterlumiblock);
    delete m_masterlumiblock;
    m_masterlumiblock=nullptr;
  }
  
  if (m_ltp != nullptr) {
    ERS_LOG(sout << "About to delete m_ltp = " << std::hex << m_ltp);
    delete m_ltp;
    m_ltp=nullptr;
  }
  
  if (m_masterttc2lan != nullptr){
    ERS_LOG(sout << "About to delete m_masterttc2lan = " << std::hex << m_ltp);
    delete m_masterttc2lan;
    m_masterttc2lan = nullptr;
  }
  
  ERS_LOG(sout << " Done");
}

/******************************************************************/
void LTPModule::setup(DFCountedPointer<Config> configuration)
/******************************************************************/
{
  std::string sout = m_UID + "@LTPModule::setup() ";
  ERS_LOG(sout << "Entered");

  m_in_running_state = false;
  m_configuration = configuration;
  m_UID = m_configuration->getString("UID");
  if (m_UID == "") {
    ERS_LOG(sout << "Name parameter is \"\", setting to default: \"LTPModule_without_name\"");
    m_UID = "LTPModule_without_name";
  }
  sout = m_UID + "@LTPModule::setup() ";
  ERS_LOG(sout << "Name = " << m_UID);

  m_rcd_uid = configuration->getString("appName");
  ERS_LOG(sout << "m_rcd_uid = " << m_rcd_uid);

  // interaction with database
  std::string ModuleUID = m_configuration->getString("UID");
  try {
    m_confDB = configuration->getPointer<Configuration>("configurationDB");
  } catch (CORBA::SystemException& ex) {
    ostringstream text;
    text << sout << "Creating configrationDB error: CORBA::SystemException ex._name()= " << ex._name();
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
  } catch (daq::is::Exception & ex) {
    ostringstream text;
    text << "Creating configurationDB error: daq::is::Exception ex.what()=" << ex.what();
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
  } catch (std::exception& ex) {
    ostringstream text;
    text << "Creating configrationDB error: std::exception ex.what()=" << ex.what();
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
  } catch (...) {
    ostringstream text;
    text << "Creating configrationDB error: caught unknown exception. Rethrowing.";
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
    throw;
  }

  // get partition object
  m_partition= const_cast<daq::core::Partition*>(daq::core::get_partition(*m_confDB, m_ipcpartition.name()));
  m_dbModule = const_cast<RCDLTPdal::LTPModule*>(m_confDB->get<RCDLTPdal::LTPModule>(ModuleUID));

  m_mastertrigger = false;
  m_use_patgen = false;
  m_PatGen_runmode = RCDLTPdal::LTPExpertConfig::PatGen_runmode::Off;
  m_useTTC2LAN = false;

  m_PhysAddress = m_configuration->getUInt("PhysAddress");
  ERS_LOG(sout << "VMEbus address = 0x" << std::hex << m_PhysAddress << std::dec);
  
  m_useConfiguration = m_configuration->getBool("useConfiguration");
  ERS_LOG(sout << "useConfiguration = " << m_useConfiguration);

  m_useMonitoring = m_configuration->getBool("useMonitoring");
  ERS_LOG(sout << "useMonitoring = " << m_useMonitoring);

  m_Monitoring_ISServerName = m_configuration->getString("Monitoring_ISServerName");
  ERS_LOG(sout << "Monitoring_ISServerName = " << m_Monitoring_ISServerName);

  m_Counter_selection = m_configuration->getString("Counter_selection");
  ERS_LOG(sout << "Counter_selection = " << m_Counter_selection);

  // configure busy mask from database
  m_Busy_local_with_linkin = false;
  m_Busy_local_with_ttlin = false;
  m_Busy_local_with_nimin = false;

  m_Busy_uid[0] = "not_used";
  m_Busy_uid[1] = "not_used";
  m_Busy_uid[2] = "not_used";

  if (m_useConfiguration) {
    ERS_LOG(sout << "Configuring");

    // first check whether the module itself is enabled
    if (!m_dbModule->daq::core::ResourceSet::disabled(*m_partition)) {
      ERS_LOG(sout << "LTPModule \"" << m_dbModule->UID() << "\" itself is enabled. Configuring busy mask.");
      // get the busy channels linked to "Contains"
      std::vector<const daq::core::ResourceBase*> items = m_dbModule->get_Contains();
      std::vector<const daq::core::ResourceBase*>::iterator it = items.begin();
      for(;it != items.end();it++){
	//check that it is a BusyChannel
	const RODBusydal::BusyChannel* bc = m_confDB->cast<RODBusydal::BusyChannel> (*it);
	if(bc){
	  int channelnumber = bc->get_Id();
	  ERS_LOG(sout << "LTPModule::setup: found BusyChannel \"" << bc->UID() << "\" for channel=" << channelnumber);
	  
	  bool resource_enabled(false);
	  
	  // first check whether the busy channel is enabled
	  if (!bc->disabled(*m_partition)) {
	    ERS_LOG(sout << "BusyChannel \"" << bc->UID() << "\" is enabled. Checking corresponding BusySource.");
	    
	    const daq::core::ResourceBase* rb = bc->get_BusySource();
	    if (rb) {
	      if (!rb->disabled(*m_partition)) {
		// resource is enabled
		ERS_LOG(sout << "LTPModule::setup: Resource for " << bc->UID() << "@BusyChannel (Id=" << channelnumber << ") is enabled -> enabling channel");
		resource_enabled = true;
	      } else {
		ERS_LOG(sout << "Resource for BusyChannel \"" << bc->UID() << "\" (" << channelnumber << ") is disabled -> disabling channel.");
		resource_enabled = false;
	      }
	    }
	  } else {
	    ERS_LOG(sout << "BusyChannel \"" << bc->UID() << "\" is disabled. Disabling this channel in the LTP.");
	  }
	  
	  if (channelnumber == 0) {
	    m_Busy_local_with_linkin = resource_enabled;
	    m_Busy_uid[0] = bc->UID();
	  } else if (channelnumber == 1) {
	    m_Busy_local_with_ttlin = resource_enabled;
	    m_Busy_uid[1] = bc->UID();
	  } else if (channelnumber == 2) {
	    m_Busy_local_with_nimin = resource_enabled;
	    m_Busy_uid[2] = bc->UID();
	  } else {
	    ostringstream text;
	    text << sout << "Invalid Id (" << channelnumber << ") for " << bc->UID() << "@BusyChannel.  Must be 0, 1, or 2.";
	    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
	  }
	}
      }
    }  
    ERS_LOG(sout << "BusyChannel(0): \"" << m_Busy_uid[0] << "\" - Busy_local_with_linkin = " << m_Busy_local_with_linkin);
    ERS_LOG(sout << "BusyChannel(1): \"" << m_Busy_uid[1] << "\" - Busy_local_with_ttlin = " << m_Busy_local_with_ttlin);
    ERS_LOG(sout << "BusyChannel(2): \"" << m_Busy_uid[2] << "\" - Busy_local_with_nimin = " << m_Busy_local_with_nimin);
  }

  // setting up the hardware already in setup to make sure the clock is
  // available before we enter into configure

  // Open LTP
  m_ltp = new LTP();
  if ( ( (m_status = m_ltp->Open(m_PhysAddress)) != VME_SUCCESS) ) {
    ostringstream text;
    text << sout << "Could not open LTP at PhysAddress 0x" << std::hex << m_PhysAddress << std::dec;
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
  }
  if (!(m_ltp->CheckLTP())) {
    ostringstream text;
    text << sout << "LTP at PhysAddress 0x" << std::hex << m_PhysAddress << std::dec << " is not an LTP module.";
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
  }

  // Get LTPConfig configuration
  const RCDLTPdal::LTPConfigBase* ltpConfBase = m_dbModule->get_Configuration();

  // apply configuration
  // REMEMBER: some of the configure methods reset the module

  if (m_useConfiguration) {
    if (ltpConfBase != nullptr) {

      if (const RCDLTPdal::LTPBypassConfig* ltpConf = m_confDB->cast<RCDLTPdal::LTPBypassConfig>(ltpConfBase)) {
	ERS_LOG(sout << "Found LTPBypassConfig");
	m_ltpConfigType = "LTPBypassConfig";
	this->configureBypass(ltpConf);
      } 
      // else if LTPExpertConfig
      else if (const RCDLTPdal::LTPExpertConfig* ltpConf = m_confDB->cast<RCDLTPdal::LTPExpertConfig>(ltpConfBase)) {
	ERS_LOG(sout << "Found LTPExpertConfig");
	m_ltpConfigType = "LTPExpertConfig";
	this->configureExpert(ltpConf);
      } 
      // else if LTPExpertBypassedConfig
      else if (const RCDLTPdal::LTPExpertBypassedConfig* ltpConf = m_confDB->cast<RCDLTPdal::LTPExpertBypassedConfig>(ltpConfBase)) {
	ERS_LOG(sout << "Found LTPExpertBypassedConfig");
	m_ltpConfigType = "LTPExpertBypassedConfig";
	this->configureExpertBypassed(ltpConf);
      } 
      else if (const RCDLTPdal::LTPLemoBypassedConfig* ltpConf = m_confDB->cast<RCDLTPdal::LTPLemoBypassedConfig>(ltpConfBase)) {
	ERS_LOG(sout << "Found LTPLemoBypassedConfig");
	m_ltpConfigType = "LTPLemoBypassedConfig";
	this->configureLemoBypassed(ltpConf);
      }
      else if (const RCDLTPdal::LTPPatgenBypassedConfig* ltpConf = m_confDB->cast<RCDLTPdal::LTPPatgenBypassedConfig>(ltpConfBase)) {
	ERS_LOG(sout << "Found LTPPatgenBypassedConfig");
	m_ltpConfigType = "LTPPatgenBypassedConfig";
	this->configurePatgenBypassed(ltpConf);
      }
      else if (const RCDLTPdal::LTPLemoConfig* ltpConf = m_confDB->cast<RCDLTPdal::LTPLemoConfig>(ltpConfBase)) {
	ERS_LOG(sout << "Found LTPLemoConfig");
	m_ltpConfigType = "LTPLemoConfig";
	this->configureLemo(ltpConf);
      }
      else if (const RCDLTPdal::LTPCTPParasiteConfig* ltpConf = m_confDB->cast<RCDLTPdal::LTPCTPParasiteConfig>(ltpConfBase)) {
	ERS_LOG(sout << "Found LTPCTPParasiteConfig");
	m_ltpConfigType = "LTPCTPParasiteConfig";
	this->configureCTPParasite(ltpConf);
      }
      else if (const RCDLTPdal::LTPPatgenConfig* ltpConf = m_confDB->cast<RCDLTPdal::LTPPatgenConfig>(ltpConfBase)) {
	ERS_LOG(sout << "Found LTPPatgenConfig");
	m_ltpConfigType = "LTPPatgenConfig";
	this->configurePatgen(ltpConf);
      }
      else if (const RCDLTPdal::LTPSlaveConfig* ltpConf = m_confDB->cast<RCDLTPdal::LTPSlaveConfig>(ltpConfBase)) {
	ERS_LOG(sout << "Found LTPSlaveConfig");
	m_ltpConfigType = "LTPSlaveConfig";
	this->configureSlave(ltpConf);
      } else {   // else: no configuration
	ostringstream text;
	text << sout << "No LtpConfig provided in configuration database";
	ERS_LOG(text.str());
      } 
    } else {
      ostringstream text;
      text << sout << "No LtpConfigBase provided in configuration database";
      ERS_LOG(text.str());
    }
  }

  // setup counter
  if (m_Counter_selection == RCDLTPdal::LTPModule::Counter_selection::L1A) {
    ERS_LOG(sout << "Setting counter to L1A");
    m_ltp->COUNTER_L1A();
  } else if (m_Counter_selection == RCDLTPdal::LTPModule::Counter_selection::TestTrigger1) {
    ERS_LOG(sout << "Setting counter to TestTrigger1");
    m_ltp->COUNTER_TR1();
  } else if (m_Counter_selection == RCDLTPdal::LTPModule::Counter_selection::TestTrigger2) {
    ERS_LOG(sout << "Setting counter to TestTrigger2");
    m_ltp->COUNTER_TR2();
  } else if (m_Counter_selection == RCDLTPdal::LTPModule::Counter_selection::TestTrigger3) {
    ERS_LOG(sout << "Setting counter to TestTrigger3");
    m_ltp->COUNTER_TR3();
  } else if (m_Counter_selection == RCDLTPdal::LTPModule::Counter_selection::L1A_ECR) {
    ERS_LOG(sout << "Setting counter to L1A_ECR");
    m_ltp->COUNTER_L1A_ecr();
  } else if (m_Counter_selection == RCDLTPdal::LTPModule::Counter_selection::TestTrigger1_ECR) {
    ERS_LOG(sout << "Setting counter to TestTrigger1_ECR");
    m_ltp->COUNTER_TR1_ecr();
  } else if (m_Counter_selection == RCDLTPdal::LTPModule::Counter_selection::TestTrigger2_ECR) {
    ERS_LOG(sout << "Setting counter to TestTrigger2_ECR");
    m_ltp->COUNTER_TR2_ecr();
  } else if (m_Counter_selection == RCDLTPdal::LTPModule::Counter_selection::TestTrigger3_ECR) {
    ERS_LOG(sout << "Setting counter to TestTrigger3_ECR");
    m_ltp->COUNTER_TR3_ecr();
  } else if (m_Counter_selection == RCDLTPdal::LTPModule::Counter_selection::BGo0) {
    ERS_LOG(sout << "Setting counter to BGo0");
    m_ltp->COUNTER_BG0();
  } else if (m_Counter_selection == RCDLTPdal::LTPModule::Counter_selection::BGo1) {
    ERS_LOG(sout << "Setting counter to BGo1");
    m_ltp->COUNTER_BG1();
  } else if (m_Counter_selection == RCDLTPdal::LTPModule::Counter_selection::BGo2) {
    ERS_LOG(sout << "Setting counter to BGo2");
    m_ltp->COUNTER_BG2();
  } else if (m_Counter_selection == RCDLTPdal::LTPModule::Counter_selection::BGo3) {
    ERS_LOG(sout << "Setting counter to BGo3");
    m_ltp->COUNTER_BG3();
  } else if (m_Counter_selection == RCDLTPdal::LTPModule::Counter_selection::Orbit) {
    ERS_LOG(sout << "Setting counter to Orbit");
    m_ltp->COUNTER_orb();
  } else if (m_Counter_selection == RCDLTPdal::LTPModule::Counter_selection::Orbit_ECR) {
    ERS_LOG(sout << "Setting counter to Orbit_ECR");
    m_ltp->COUNTER_orb_ecr();
  } else if (m_Counter_selection == RCDLTPdal::LTPModule::Counter_selection::None) {
    ERS_LOG(sout << "Setting counter to none");
    m_ltp->COUNTER_none();
  } else {
    ERS_LOG(sout << "Leaving counter unchanged");
  }

  // Get MasterTriggerSetup configuration
  const RCDLTPdal::LTPMasterTriggerSetup* ltpMasterTriggerSetup = m_dbModule->get_MasterTriggerSetup();

  if (ltpMasterTriggerSetup) {
    ltpMasterTriggerSetup->print(1,true,std::cout);

    m_ecr_at_resume = ltpMasterTriggerSetup->get_ECR_AtResume();
    m_ecr_periodic = ltpMasterTriggerSetup->get_ECR_Periodic();
    m_ecr_period = ltpMasterTriggerSetup->get_ECR_Period();

    m_lumiblock_period = ltpMasterTriggerSetup->get_LBPeriod();
    m_lumiblock_mindistance = ltpMasterTriggerSetup->get_LBMinimumDistance();

    m_TTC2LAN_Activate = ltpMasterTriggerSetup->get_TTC2LAN_Activate();
    m_TTC2LAN_PollFrequency = ltpMasterTriggerSetup->get_TTC2LAN_PollFrequency();
    // convert poll frequency to a period in ms
    m_ttc2lan_pollperiod = 10; // default 10ms
    // frequency Hz:     10Hz, 100Hz, 1kHz
    //           ms:     100,    10,    1
    if (m_TTC2LAN_PollFrequency == RCDLTPdal::LTPMasterTriggerSetup::TTC2LAN_PollFrequency::_10Hz) {
      m_ttc2lan_pollperiod = 100;
    }
    else if (m_TTC2LAN_PollFrequency == RCDLTPdal::LTPMasterTriggerSetup::TTC2LAN_PollFrequency::_100Hz) {
      m_ttc2lan_pollperiod = 10;
    }
    else if (m_TTC2LAN_PollFrequency == RCDLTPdal::LTPMasterTriggerSetup::TTC2LAN_PollFrequency::_1kHz) {
      m_ttc2lan_pollperiod = 1;
    }
    else {
      ERS_LOG(sout << "Invalid Range for m_TTC2LAN_PollFrequency" << m_TTC2LAN_PollFrequency << ". Using default 100Hz");
    }
    
    m_TTC2LAN_DEBUG = ltpMasterTriggerSetup->get_TTC2LAN_DEBUG();
    m_TTC2LAN_DEBUG_Node = ltpMasterTriggerSetup->get_TTC2LAN_DEBUG_Node();
    m_TTC2LAN_DEBUG_PortNumber = ltpMasterTriggerSetup->get_TTC2LAN_DEBUG_PortNumber();

    // find out who is master
    const daq::core::MasterTrigger* masterTrigger = m_partition->get_MasterTrigger();
    ERS_LOG(sout << "Check if we got a MasterTrigger object");
    if (masterTrigger) {
      ERS_LOG(sout << "We got a MasterTrigger object");

      std::string this_appname = m_configuration->getString("appName");
      const daq::df::RCD* this_app  = m_confDB->get<daq::df::RCD>(this_appname);

      const  daq::df::RCD* masterltp_rcd = m_confDB->cast<daq::df::RCD>(masterTrigger->get_Controller());
      if (masterltp_rcd) {
	uint8_t master_detid = masterltp_rcd->get_Detector()->get_LogicalId() ;
	ERS_LOG(sout << "Found master trigger " << masterltp_rcd->UID());
	ERS_LOG(sout << "Detector id of this RCD is " << std::hex << master_detid << std::dec);

	if (this_app->UID() == masterltp_rcd->UID()) {
	  ERS_LOG(sout << "MasterTrigger_Controller application is my application: \"" << this_app->UID() << "\"");
	  m_I_am_masterTriggerApp = true;
	} else {
	  ERS_LOG(sout << "MasterTrigger RCD is \"" << masterltp_rcd->UID() << "\", this application = \"" << this_app->UID() << "\"");
	}
      }

      // get master trigger module
      const RCDLTPdal::LTPModule* masterModule = m_confDB->cast<RCDLTPdal::LTPModule>(masterTrigger->get_TriggerModule());
    
      if (masterModule) {
	// check wether this is me!
	if (masterModule->UID() == m_dbModule->UID()) {
	  ERS_LOG(sout << "TriggerModule in the MasterTrigger is myself: \"" << m_dbModule->UID() << "\"");
	  m_I_am_TriggerModule = true;
	} else {
	  ERS_LOG(sout << "TriggerModule is \"" << masterModule->UID() << "\", myself = \"" << m_dbModule->UID() << "\"");
	}
      }
    } else {
      ERS_LOG(sout << "No MasterTrigger object found in Partition object");
    }
  }
   
  // set m_mastertrigger variable
  m_mastertrigger = true;
  if (!m_I_am_masterTriggerApp || !m_I_am_TriggerModule) {
    m_mastertrigger = false;
  }
  
  m_useTTC2LAN = false;
  m_useECR = false;
  if (m_mastertrigger && m_TTC2LAN_Activate) {
    m_useTTC2LAN = true;
    m_useECR = true;
  }

  // enable TTC2LAN for L1Calo (on Murrough Landon's request)
  if (m_forceCommandedTrigger) {
    m_useTTC2LAN = true;
    m_mastertrigger = true;
    m_useECR = true;
  }

  if (m_mastertrigger && (m_ecr_periodic || m_ecr_at_resume)) {
    m_useECR = true;
    // setup BGo-1 register
    m_ltp->IO_local_lemo_pfn(LTP::BG1);
    m_ltp->IO_lemoout_from_local(LTP::BG1);
    m_ltp->IO_linkout_from_local(LTP::BG1);
    m_ltp->IO_disable_busy_gating(LTP::BG1);
    m_ltp->IO_disable_inhibit_gating(LTP::BG1);
    m_ltp->IO_disable_pg_gating(LTP::BG1);
    m_ltp->IO_pfn_width_1bc(LTP::BG1);
  }

  // printouts 
  ERS_LOG(sout << "m_useConfiguration = " << m_useConfiguration);
  ERS_LOG(sout << "m_useMonitoring = " << m_useMonitoring); 
  ERS_LOG(sout << "m_mastertrigger = " << m_mastertrigger);
  ERS_LOG(sout << "m_ecr_at_resume = " << m_ecr_at_resume);
  ERS_LOG(sout << "m_ecr_periodic = " << m_ecr_periodic);
  ERS_LOG(sout << "m_ecr_period = " << m_ecr_period);
  ERS_LOG(sout << "m_useECR = " << m_useECR);
  ERS_LOG(sout << "m_lumiblock_period = " << m_lumiblock_period);
  ERS_LOG(sout << "m_lumiblock_mindistance = " << m_lumiblock_mindistance);
  ERS_LOG(sout << "m_TTC2LAN_Activate = " << m_TTC2LAN_Activate);
  ERS_LOG(sout << "m_useTTC2LAN = " << m_useTTC2LAN);
  ERS_LOG(sout << "m_TTC2LAN_PollFrequency = " << m_TTC2LAN_PollFrequency);
  ERS_LOG(sout << "m_ttc2lan_pollperiod = " << m_ttc2lan_pollperiod);
  ERS_LOG(sout << "m_TTC2LAN_DEBUG = " << m_TTC2LAN_DEBUG);
  ERS_LOG(sout << "m_TTC2LAN_DEBUG_Node = " << m_TTC2LAN_DEBUG_Node);
  ERS_LOG(sout << "m_TTC2LAN_DEBUG_PortNumber = " << m_TTC2LAN_DEBUG_PortNumber);

  // instantiating a master
  
  if (m_mastertrigger) {
    ERS_LOG(sout << "Creating LTPMasterBusy object");
    m_masterbusy = new LTPMasterBusy(*m_ltp, m_ipcpartition);

    if (m_useECR) {
      ERS_LOG(sout << "Creating LTPMasterECR object");
      m_masterecr = new LTPMasterECR(m_ecr_period*1000, *m_ltp, *m_masterbusy, m_ipcpartition);
      // set deadtime to 1ms before and after: 1 ms = 40000 BC
      m_masterecr->setDeadtime(40000,40000);
    }

    // need a LTPMasterTTC2LAN in any case for L1A monitoring
    ERS_LOG(sout << "creating MasterTTC2LAN object");
    m_masterttc2lan = new LTPMasterTTC2LAN(*m_ltp, *m_masterbusy, *m_masterecr, m_ipcpartition);
    m_masterttc2lan->disableSending(); // disable sending for now

    ERS_LOG(sout << "Creating LTPMasterLumiBlock object");
    m_masterlumiblock = new LTPMasterLumiBlock(*m_masterbusy, *m_masterttc2lan, m_ipcpartition);
    m_masterlumiblock->setPeriod(m_lumiblock_period*1000);
    m_masterlumiblock->setMinimumDistance(m_lumiblock_mindistance*1000);
    m_masterlumiblock->disableLumiBlocks();
    m_masterlumiblock->disablePeriodicLumiBlocks();

    ERS_LOG(sout << "Creating CommandedTrigger object for application \"" << m_rcd_uid << "\" and module \"" << m_UID << "\"");
    m_commandedTrigger = new daq::trigger::CommandedTrigger(m_ipcpartition, m_rcd_uid, this);

    if (m_useTTC2LAN) {
      // moved to the CONNECT transition
     }

  }

  // handle busy
  if (m_mastertrigger) {
    // reset all busy channels
    // not that the reset will set the trigger busy, which will be unset during prepareForRun,
    // as well as the runcontrol busy, which will be unset with the first resume command
    ERS_LOG(sout << "calling m_masterbusy->reset()");
    m_masterbusy->reset();
    m_masterbusy->dump();
  } else {
    m_ltp->BUSY_disable_constant_level();
    ERS_LOG(sout << "m_mastertrigger = false; Disabling constant busy since I'm not in charge of the busy");
  }

  // check that the clock is running
  if ( !m_ltp->BC_selected_is_running()) {
    ostringstream text;
    text << sout << "No valid clock detected. Settings will be undetermined.";
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
  }

  // publish once to IS
  this->publishLTPInfo();

  ERS_LOG(sout << "Done");
}

/******************************************************/
void LTPModule::configure(const daq::rc::TransitionCmd&)
/******************************************************/
{
  std::string sout = m_UID + "@LTPModule::configure() ";
  ERS_LOG(sout << "Entered");
  m_in_running_state = false;

  if (m_mastertrigger) {
    m_masterbusy->dump();
  }
  
  ERS_LOG(sout << "Done");
}


/**********************************************************/
void LTPModule::unconfigure(const daq::rc::TransitionCmd&)
/**********************************************************/
{
  std::string sout = m_UID + "@LTPModule::unconfigure() ";
  ERS_LOG(sout << "Entered");

  if (m_mastertrigger) {
    ERS_LOG(sout << "calling m_masterbusy->reset()");
    m_masterbusy->reset();
    m_masterbusy->dump();
  }

  // masterbusy and masterecr and masterlumiblock and m_ltp are created in 'setup()', which is called in the initialize transistion. Hence if they are deleted here the program will creash when doing an unconfig -> config transisition! Moved to destructor.
  /*
  if (m_masterbusy != nullptr) {
    ERS_LOG(sout << "About to delete m_masterbusy = " << std::hex << m_masterbusy);
    delete m_masterbusy;
    m_masterbusy=nullptr;
  }

  if (m_masterecr != nullptr) {
    ERS_LOG(sout << "About to delete m_masterecr = " << std::hex << m_masterecr);
    delete m_masterecr;
    m_masterecr=nullptr;
  }
  
  if (m_masterlumiblock != nullptr) {
    ERS_LOG(sout << "About to delete m_masterlumiblock = " << std::hex << m_masterlumiblock);
    delete m_masterlumiblock;
    m_masterlumiblock=nullptr;
  }

  if (m_ltp != nullptr) {
    ERS_LOG(sout << "About to delete m_ltp = " << std::hex << m_ltp);
    delete m_ltp;
    m_ltp=nullptr;
  }
*/

  ERS_LOG(sout << "Done");
}

/**********************************************************/
void LTPModule::connect(const daq::rc::TransitionCmd&)
/**********************************************************/
{
  std::string sout = m_UID + "@LTPModule::connect() ";
  ERS_LOG(sout << "Entered");

  int status(0);
  m_in_running_state = false;

  if (m_mastertrigger) {
    m_masterlumiblock->disableLumiBlocks();
    m_masterlumiblock->disablePeriodicLumiBlocks();
  }

  // check that the clock is set up correctly
  if (!m_ltp->BC_selected_is_running()) {
    ostringstream text;
    text << sout << "No valid clock detected. Settings will be undetermined.";
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
  }

  if (m_mastertrigger) {
    // reset all busy channels
    // note that the reset will set the trigger busy, which will be unset during prepareForRun,
    // as well as the runcontrol busy, which will be unset with the first resume command
    ERS_LOG(sout << "calling m_masterbusy->reset()");
    m_masterbusy->reset();
    m_masterbusy->dump();
  }

  // set pattern generator run mode
  if (m_use_patgen) {
    if (m_PatGen_runmode == RCDLTPdal::LTPExpertConfig::PatGen_runmode::Continuous) {
      ERS_LOG(sout << "Setting pattern generator mode to continuous");
      status |= m_ltp->PG_runmode_cont();
      status |= m_ltp->PG_start();
    }
    else if (m_PatGen_runmode == RCDLTPdal::LTPExpertConfig::PatGen_runmode::Fixed_Value) {
      ERS_LOG(sout << "Setting pattern generator mode to fixed_value");
      status |= m_ltp->PG_runmode_fixedvalue();
    }
    else if (m_PatGen_runmode == RCDLTPdal::LTPExpertConfig::PatGen_runmode::Single_Shot_Orbit) {
      ERS_LOG(sout << "Setting pattern generator mode to single_shot_orbit");
      status |= m_ltp->PG_runmode_singleshot_orb();
    }
    else if (m_PatGen_runmode == RCDLTPdal::LTPExpertConfig::PatGen_runmode::Single_Shot_BGo3) {
      ERS_LOG(sout << "Setting pattern generator mode to single_shot_bgo3");
      status |= m_ltp->PG_runmode_singleshot_bgo();
    } else {
      ostringstream text;
      text << sout << "Unknown value \"" << m_PatGen_runmode << "\" for attribute PatGen_runmode";
      ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
    }
  } else {
    // pattern generator
    status |= m_ltp->PG_reset();
    status |= m_ltp->PG_runmode_notused();
  }

  // setup daq::asyncmsg connection for TTC2LAN communication
  if (m_useTTC2LAN) {
    ERS_LOG(sout << "calling m_masterttc2lan->enableSending()");
    m_masterttc2lan->enableSending();
    ERS_LOG(sout << "calling m_masterttc2lan->connect()");
    m_masterttc2lan->connect();
  }
  // end: TTC2LAN setup
  
  m_runnumber=0;

  if (m_mastertrigger) {
    m_masterbusy->dump();
  }
  
  ERS_LOG(sout << "Done");
}

/**********************************************************/
void LTPModule::disconnect(const daq::rc::TransitionCmd&)
/**********************************************************/
{
  std::string sout = m_UID + "@LTPModule::disconnect() ";
  ERS_LOG(sout << "Entered");

  m_in_running_state = false;

  // to be safe, set busy
  if (m_mastertrigger) {
    m_masterbusy->setTriggerBusy();
    m_masterbusy->setRunControlBusy(1); // we be unset with the first resume command
  }
  if (m_useTTC2LAN && m_masterttc2lan != nullptr) {
    ERS_LOG(sout << "calling m_masterttc2lan->disconnect()");
    m_masterttc2lan->disconnect();
  }

  ERS_LOG(sout << "Done");
}

/**********************************************************/
void LTPModule::prepareForRun(const daq::rc::TransitionCmd&)
/**********************************************************/
{
  std::string sout = m_UID + "@LTPModule::prepareForRun() ";
  ERS_LOG(sout << "Entered");
  m_in_running_state = false;
  // read counter and log value
  u_int cntValue = 0;
  m_ltp->COUNTER_read_32(&cntValue);
  ERS_LOG(sout << " counter value = "<<cntValue);
  
  // to be safe, set busy
  if (m_mastertrigger) {
    // reset all busy channels
    // note that the reset will set the trigger busy, which will be unset during prepareForRun,
    // as well as the runcontrol busy, which will be unset with the first resume command
    ERS_LOG(sout << "Busy status before reset");
    m_masterbusy->dump();
    ERS_LOG(sout << "calling m_masterbusy->reset()");
    m_masterbusy->reset();
    ERS_LOG(sout << "Busy status after reset");
    m_masterbusy->dump();
  }

  if (m_useECR) {
    ERS_LOG(sout << "calling m_masterecr->reset()");
    m_masterecr->reset();
  }

  m_runnumber = m_runParams->getUInt("runNumber");
  ERS_LOG(sout << "Retrieved run number " << m_runnumber);

  if (m_mastertrigger) {
    try {
      ERS_LOG(sout << "calling m_masterlumiblock->reset("<< m_runnumber << ")");
      m_masterlumiblock->reset(m_runnumber);
    } catch (LTPModuleFailedFirstLumiBlock& ex) {
      std::ostringstream text;
      text << sout << "Couldn't retrieve reasonable LumiBlock information for LBN=0. " << ex.what();
      ERS_REPORT_IMPL(ers::error, ers::Message, text.str(),);
    }
    m_masterlumiblock->enableLumiBlocks();
    m_masterlumiblock->enablePeriodicLumiBlocks();
    if (m_useTTC2LAN) {
      ERS_LOG(sout << "calling m_masterttc2lan->disableSending()");
      m_masterttc2lan->disableSending();
      ERS_LOG(sout << "calling m_masterttc2lan->reset()");
      m_masterttc2lan->reset();
    }
  }

  if (m_useConfiguration) {
    // reset busy mask to configuration parameters
    m_Busy_current[0] = m_Busy_local_with_linkin;
    m_Busy_current[1] = m_Busy_local_with_ttlin;
    m_Busy_current[2] = m_Busy_local_with_nimin;
    this->updateBusy();
  }

  if (m_useConfiguration || m_useMonitoring) {
    // reset counters and fifos
    m_ltp->TTYPE_reset_fifo();
    m_ltp->COUNTER_reset();
  }

  if (m_useTTC2LAN) {
    ERS_LOG(sout << "calling m_masterttc2lan->enableSending()");
    m_masterttc2lan->enableSending();
  }


  if (m_mastertrigger) {
    // unset trigger busy in prepareForRun
    ERS_LOG(sout << "Unsetting trigger busy");
    m_masterbusy->unsetTriggerBusy();
    // RunControl busy will be reset with the first resume
    ERS_LOG(sout << "Busy status at completion of prepareForRun");
    m_masterbusy->dump();
  }

  // read counter and log value
  cntValue = 0;
  m_ltp->COUNTER_read_32(&cntValue);
  ERS_LOG(sout << " counter value = "<<cntValue);
  
  ERS_LOG(sout << "Done");
}


/******************************************************/
DFCountedPointer<Config> LTPModule::getInfo ()
/******************************************************/
{
  std::string sout = m_UID + "@LTPModule::getInfo() ";
  ERS_DEBUG(1, sout << "Entered");

  DFCountedPointer<Config> configuration = Config::New();
  
  ERS_DEBUG(1, sout << "Done");
  return configuration;
}

/******************************************************/
void LTPModule::clearInfo ()
/******************************************************/
{
  std::string sout = m_UID + "@LTPModule::clearInfo() ";
  ERS_DEBUG(1, sout << "Entered");

  ERS_DEBUG(1, sout << "Done");
}

/******************************************************/
void LTPModule::publish()
/******************************************************/
{
  std::string sout = m_UID + "@LTPModule::publish() ";
  ERS_DEBUG(1, sout << "Entered");

  // publish counter value
  if (m_useMonitoring) {
    this->publishLTPInfo();
  }

  if (m_mastertrigger) {
    m_masterttc2lan->publishToIS();
  }

  ERS_DEBUG(1, sout << "Done");
}


/******************************************************/
void LTPModule::stopROIB(const daq::rc::TransitionCmd&)
/******************************************************/
{
  std::string sout = m_UID + "@LTPModule::stopROIB() ";
  ERS_LOG(sout << "Entered");

  if (m_mastertrigger) {
    ERS_LOG(sout << "Calling m_masterlumiblock->disablePeriodicLumiBlocks()");
    m_masterlumiblock->disablePeriodicLumiBlocks();
  }

  m_in_running_state = false;

  ERS_LOG(sout << "Done");
}


/******************************************************/
void LTPModule::stopArchiving(const daq::rc::TransitionCmd&)
/******************************************************/
{
  std::string sout = m_UID + "@LTPModule::stopArchiving() ";
  ERS_LOG(sout << "Entered");

  if (m_mastertrigger) {
    ERS_LOG(sout << "Calling m_masterlumiblock->disableLumiBlocks()");
    m_masterlumiblock->disableLumiBlocks();
    ERS_LOG(sout << "Calling m_masterlumiblock->disablePeriodicLumiBlock()");
    m_masterlumiblock->disablePeriodicLumiBlocks();

    // at this point it is wise to reset the busy
    ERS_LOG(sout << "calling m_masterbusy->reset()");
    m_masterbusy->reset();
    m_masterbusy->dump();

  }

  m_in_running_state = false;

  ERS_LOG(sout << "Done");
}




/******************************************************/
void LTPModule::publishFullStats ()
/******************************************************/
{  
  std::string sout = m_UID + "@LTPModule::publishFullStats() ";
  ERS_DEBUG(1, sout << "Entered");

  // publish counter value
  this->publish();

  ERS_DEBUG(1, sout << "Done");
}    

/******************************************************/
void LTPModule::publishTTC2LANInfo()
/******************************************************/
{
  std::string sout = m_UID + "@LTPModule::publishTTC2LANInfo() ";
  ERS_DEBUG(1, sout << "Entered");

  if (m_useTTC2LAN) {
    m_masterttc2lan->publishToIS();
  }

  ERS_DEBUG(1, sout << "Done");
}



/******************************************************/
void LTPModule::publishLTPInfo()
/******************************************************/
{
  std::string sout = m_UID + "@LTPModule::publishLTPInfo() ";
  ERS_DEBUG(1, sout << "Entered");

  const std::string entryname_ltpstatus = m_Monitoring_ISServerName + "." + m_UID + "/LTPStatus";
  ERS_DEBUG(1, sout << entryname_ltpstatus.c_str());

  LTP_ISNamed is_entry(m_ipcpartition, entryname_ltpstatus);
  u_int countervalue;
  m_ltp->COUNTER_read_32(&countervalue);
  is_entry.CounterValue  = static_cast<unsigned long>(countervalue);
    
  enum LTP::counter_selection sel;
  m_ltp->COUNTER_selection(&sel);
  std::string sel_str;
  switch (sel) {
  case LTP::CNT_ORBIT: sel_str="ORBIT"; break;
  case LTP::CNT_ORBIT_ECR: sel_str="ORBIT_ECR"; break;
  case LTP::CNT_BG0: sel_str="BG0"; break;
  case LTP::CNT_BG1: sel_str="BG1"; break;
  case LTP::CNT_BG2: sel_str="BG2"; break;
  case LTP::CNT_BG3: sel_str="BG3"; break;
  case LTP::CNT_L1A: sel_str="L1A"; break;
  case LTP::CNT_TR1: sel_str="TR1"; break;
  case LTP::CNT_TR2: sel_str="TR2"; break;
  case LTP::CNT_TR3: sel_str="TR3"; break;
  case LTP::CNT_L1A_ECR: sel_str="L1A_ECR"; break;
  case LTP::CNT_TR1_ECR: sel_str="TR1_ECR"; break;
  case LTP::CNT_TR2_ECR: sel_str="TR2_ECR"; break;
  case LTP::CNT_TR3_ECR: sel_str="TR3_ECR"; break;
  default: sel_str="UNKNOWN";
  }
   
  is_entry.CounterName   = sel_str;

  if (m_ltp->BC_linkout_is_from_local()) {
    if (m_ltp->BC_local_is_from_lemoin()) {
      is_entry.BC_linkout="LEMO";
    } else {
      is_entry.BC_linkout="Internal";
    }
  } else {
    is_entry.BC_linkout="linkin";
  }

  if (m_ltp->BC_lemoout_is_from_local()) {
    if (m_ltp->BC_local_is_from_lemoin()) {
      is_entry.BC_local_LEMO_out="LEMO";
    } else {
      is_entry.BC_local_LEMO_out="internal";
    }
  } else {
    is_entry.BC_local_LEMO_out="linkin";
  }

  if (m_ltp->BC_selected_is_running()) {
    is_entry.BC_status=true;
  } else {
    is_entry.BC_status=false;
  }

  // local
  std::string orb_local;
  enum LTP::orbit_source src;
  m_ltp->ORB_local_selection(&src);
  switch (src) {
  case LTP::ORB_NO_SOURCE:
    orb_local="no_source";
    break;
  case LTP::ORB_INTERNAL:
    orb_local="internal";
    break;
  case LTP::ORB_FP_LEMO_INPUT:
    orb_local="LEMO";
    break;
  case LTP::ORB_PATGEN:
    orb_local="pattern generator";
    break;
  default:
    orb_local="invalid";
    break;
  }    

  if (m_ltp->ORB_linkout_is_from_local()) {
    is_entry.ORBIT_linkout=orb_local;
  } else {
    is_entry.ORBIT_linkout="linkin";
  }
    
  if (m_ltp->ORB_linkout_is_from_local()) {
    is_entry.ORBIT_LEMO_out=orb_local;
  } else {
    is_entry.ORBIT_LEMO_out="linkin";
  }

  if (m_ltp->ORB_selected_is_running()) {
    is_entry.ORBIT_LEMO_out_running=true;
  } else {
    is_entry.ORBIT_LEMO_out_running=false;
  }

  // L1A
  std::string l1a_local;
  enum LTP::io_source iosrc;
  m_ltp->IO_local_selection(LTP::L1A, &iosrc);
  switch (iosrc) {
  case LTP::IO_NO_SOURCE:
    l1a_local = "no_source";
    break;
  case LTP::IO_FP_LEMO:
    l1a_local = "LEMO";
    break;
  case LTP::IO_FP_LEMO_PFN:
    l1a_local = "LEMO_PFN";
    break;
  case LTP::IO_PATGEN:
    l1a_local = "pattern generator";
    break;
  default:
    l1a_local = "invalid";
    break;
  }
  if (m_ltp->IO_linkout_is_from_local(LTP::L1A)) {
    is_entry.L1A_linkout=l1a_local;
  } else {
    is_entry.L1A_linkout="linkin";
  }
  if (m_ltp->IO_lemoout_is_from_local(LTP::L1A)) {
    is_entry.L1A_LEMO_out=l1a_local;
  } else {
    is_entry.L1A_LEMO_out="linkin";
  }
  if (m_ltp->IO_busy_gating_is_enabled(LTP::L1A)) {
    is_entry.L1A_busy_gating_enabled = true;
  } else {
    is_entry.L1A_busy_gating_enabled = false;
  }
  if (m_ltp->IO_inhibit_timer_gating_is_enabled(LTP::L1A)) {
    is_entry.L1A_inhibit_gating_enabled = true;
  } else {
    is_entry.L1A_inhibit_gating_enabled = false;
  }
  if (m_ltp->IO_pg_gating_is_enabled(LTP::L1A)) {
    is_entry.L1A_patgen_gating_enabled = true;
  } else {
    is_entry.L1A_patgen_gating_enabled = false;
  }
  if (m_ltp->IO_pfn_width_is_1bc(LTP::L1A)) {
    is_entry.L1A_PFN_duration = "1BC";
  } else {
    is_entry.L1A_PFN_duration = "2BC";
  }

  // Test Trigger 1
  std::string tt1_local;
  m_ltp->IO_local_selection(LTP::TR1, &iosrc);
  switch (iosrc) {
  case LTP::IO_NO_SOURCE:
    tt1_local = "no_source";
    break;
  case LTP::IO_FP_LEMO:
    tt1_local = "LEMO";
    break;
  case LTP::IO_FP_LEMO_PFN:
    tt1_local = "LEMO_PFN";
    break;
  case LTP::IO_PATGEN:
    tt1_local = "pattern generator";
    break;
  default:
    tt1_local = "invalid";
    break;
  }
  if (m_ltp->IO_linkout_is_from_local(LTP::TR1)) {
    is_entry.TestTrigger1_linkout=tt1_local;
  } else {
    is_entry.TestTrigger1_linkout="linkin";
  }
  if (m_ltp->IO_lemoout_is_from_local(LTP::TR1)) {
    is_entry.TestTrigger1_LEMO_out=tt1_local;
  } else {
    is_entry.TestTrigger1_LEMO_out="linkin";
  }
  if (m_ltp->IO_busy_gating_is_enabled(LTP::TR1)) {
    is_entry.TestTrigger1_busy_gating_enabled = true;
  } else {
    is_entry.TestTrigger1_busy_gating_enabled = false;
  }
  if (m_ltp->IO_inhibit_timer_gating_is_enabled(LTP::TR1)) {
    is_entry.TestTrigger1_inhibit_gating_enabled = true;
  } else {
    is_entry.TestTrigger1_inhibit_gating_enabled = false;
  }
  if (m_ltp->IO_pg_gating_is_enabled(LTP::TR1)) {
    is_entry.TestTrigger1_patgen_gating_enabled = true;
  } else {
    is_entry.TestTrigger1_patgen_gating_enabled = false;
  }
  if (m_ltp->IO_pfn_width_is_1bc(LTP::TR1)) {
    is_entry.TestTrigger1_PFN_duration = "1BC";
  } else {
    is_entry.TestTrigger1_PFN_duration = "2BC";
  }

  // Test Trigger 2
  std::string tt2_local;
  m_ltp->IO_local_selection(LTP::TR2, &iosrc);
  switch (iosrc) {
  case LTP::IO_NO_SOURCE:
    tt2_local = "no_source";
    break;
  case LTP::IO_FP_LEMO:
    tt2_local = "LEMO";
    break;
  case LTP::IO_FP_LEMO_PFN:
    tt2_local = "LEMO_PFN";
    break;
  case LTP::IO_PATGEN:
    tt2_local = "pattern generator";
    break;
  default:
    tt2_local = "invalid";
    break;
  }
  if (m_ltp->IO_linkout_is_from_local(LTP::TR2)) {
    is_entry.TestTrigger2_linkout=tt2_local;
  } else {
    is_entry.TestTrigger2_linkout="linkin";
  }
  if (m_ltp->IO_lemoout_is_from_local(LTP::TR2)) {
    is_entry.TestTrigger2_LEMO_out=tt2_local;
  } else {
    is_entry.TestTrigger2_LEMO_out="linkin";
  }
  if (m_ltp->IO_busy_gating_is_enabled(LTP::TR2)) {
    is_entry.TestTrigger2_busy_gating_enabled = true;
  } else {
    is_entry.TestTrigger2_busy_gating_enabled = false;
  }
  if (m_ltp->IO_inhibit_timer_gating_is_enabled(LTP::TR2)) {
    is_entry.TestTrigger2_inhibit_gating_enabled = true;
  } else {
    is_entry.TestTrigger2_inhibit_gating_enabled = false;
  }
  if (m_ltp->IO_pg_gating_is_enabled(LTP::TR2)) {
    is_entry.TestTrigger2_patgen_gating_enabled = true;
  } else {
    is_entry.TestTrigger2_patgen_gating_enabled = false;
  }
  if (m_ltp->IO_pfn_width_is_1bc(LTP::TR2)) {
    is_entry.TestTrigger2_PFN_duration = "1BC";
  } else {
    is_entry.TestTrigger2_PFN_duration = "2BC";
  }

  // Test Trigger 3
  std::string tt3_local;
  m_ltp->IO_local_selection(LTP::TR3, &iosrc);
  switch (iosrc) {
  case LTP::IO_NO_SOURCE:
    tt3_local = "no_source";
    break;
  case LTP::IO_FP_LEMO:
    tt3_local = "LEMO";
    break;
  case LTP::IO_FP_LEMO_PFN:
    tt3_local = "LEMO_PFN";
    break;
  case LTP::IO_PATGEN:
    tt3_local = "pattern generator";
    break;
  default:
    tt3_local = "invalid";
    break;
  }
  if (m_ltp->IO_linkout_is_from_local(LTP::TR3)) {
    is_entry.TestTrigger3_linkout=tt3_local;
  } else {
    is_entry.TestTrigger3_linkout="linkin";
  }
  if (m_ltp->IO_lemoout_is_from_local(LTP::TR3)) {
    is_entry.TestTrigger3_LEMO_out=tt3_local;
  } else {
    is_entry.TestTrigger3_LEMO_out="linkin";
  }
  if (m_ltp->IO_busy_gating_is_enabled(LTP::TR3)) {
    is_entry.TestTrigger3_busy_gating_enabled = true;
  } else {
    is_entry.TestTrigger3_busy_gating_enabled = false;
  }
  if (m_ltp->IO_inhibit_timer_gating_is_enabled(LTP::TR3)) {
    is_entry.TestTrigger3_inhibit_gating_enabled = true;
  } else {
    is_entry.TestTrigger3_inhibit_gating_enabled = false;
  }
  if (m_ltp->IO_pg_gating_is_enabled(LTP::TR3)) {
    is_entry.TestTrigger3_patgen_gating_enabled = true;
  } else {
    is_entry.TestTrigger3_patgen_gating_enabled = false;
  }
  if (m_ltp->IO_pfn_width_is_1bc(LTP::TR3)) {
    is_entry.TestTrigger3_PFN_duration = "1BC";
  } else {
    is_entry.TestTrigger3_PFN_duration = "2BC";
  }

  // BGo-0
  std::string bg0_local;
  m_ltp->IO_local_selection(LTP::BG0, &iosrc);
  switch (iosrc) {
  case LTP::IO_NO_SOURCE:
    bg0_local = "no_source";
    break;
  case LTP::IO_FP_LEMO:
    bg0_local = "LEMO";
    break;
  case LTP::IO_FP_LEMO_PFN:
    bg0_local = "LEMO_PFN";
    break;
  case LTP::IO_PATGEN:
    bg0_local = "pattern generator";
    break;
  default:
    bg0_local = "invalid";
    break;
  }
  if (m_ltp->IO_linkout_is_from_local(LTP::BG0)) {
    is_entry.BGo0_linkout=bg0_local;
  } else {
    is_entry.BGo0_linkout="linkin";
  }
  if (m_ltp->IO_lemoout_is_from_local(LTP::BG0)) {
    is_entry.BGo0_LEMO_out=bg0_local;
  } else {
    is_entry.BGo0_LEMO_out="linkin";
  }
  if (m_ltp->IO_busy_gating_is_enabled(LTP::BG0)) {
    is_entry.BGo0_busy_gating_enabled = true;
  } else {
    is_entry.BGo0_busy_gating_enabled = false;
  }
  if (m_ltp->IO_inhibit_timer_gating_is_enabled(LTP::BG0)) {
    is_entry.BGo0_inhibit_gating_enabled = true;
  } else {
    is_entry.BGo0_inhibit_gating_enabled = false;
  }
  if (m_ltp->IO_pg_gating_is_enabled(LTP::BG0)) {
    is_entry.BGo0_patgen_gating_enabled = true;
  } else {
    is_entry.BGo0_patgen_gating_enabled = false;
  }
  if (m_ltp->IO_pfn_width_is_1bc(LTP::BG0)) {
    is_entry.BGo0_PFN_duration = "1BC";
  } else {
    is_entry.BGo0_PFN_duration = "2BC";
  }

  // BGo-1
  std::string bg1_local;
  m_ltp->IO_local_selection(LTP::BG1, &iosrc);
  switch (iosrc) {
  case LTP::IO_NO_SOURCE:
    bg1_local = "no_source";
    break;
  case LTP::IO_FP_LEMO:
    bg1_local = "LEMO";
    break;
  case LTP::IO_FP_LEMO_PFN:
    bg1_local = "LEMO_PFN";
    break;
  case LTP::IO_PATGEN:
    bg1_local = "pattern generator";
    break;
  default:
    bg1_local = "invalid";
    break;
  }
  if (m_ltp->IO_linkout_is_from_local(LTP::BG1)) {
    is_entry.BGo1_linkout=bg1_local;
  } else {
    is_entry.BGo1_linkout="linkin";
  }
  if (m_ltp->IO_lemoout_is_from_local(LTP::BG1)) {
    is_entry.BGo1_LEMO_out=bg1_local;
  } else {
    is_entry.BGo1_LEMO_out="linkin";
  }
  if (m_ltp->IO_busy_gating_is_enabled(LTP::BG1)) {
    is_entry.BGo1_busy_gating_enabled = true;
  } else {
    is_entry.BGo1_busy_gating_enabled = false;
  }
  if (m_ltp->IO_inhibit_timer_gating_is_enabled(LTP::BG1)) {
    is_entry.BGo1_inhibit_gating_enabled = true;
  } else {
    is_entry.BGo1_inhibit_gating_enabled = false;
  }
  if (m_ltp->IO_pg_gating_is_enabled(LTP::BG1)) {
    is_entry.BGo1_patgen_gating_enabled = true;
  } else {
    is_entry.BGo1_patgen_gating_enabled = false;
  }
  if (m_ltp->IO_pfn_width_is_1bc(LTP::BG1)) {
    is_entry.BGo1_PFN_duration = "1BC";
  } else {
    is_entry.BGo1_PFN_duration = "2BC";
  }

  // BGo-2
  std::string bg2_local;
  m_ltp->IO_local_selection(LTP::BG2, &iosrc);
  switch (iosrc) {
  case LTP::IO_NO_SOURCE:
    bg2_local = "no_source";
    break;
  case LTP::IO_FP_LEMO:
    bg2_local = "LEMO";
    break;
  case LTP::IO_FP_LEMO_PFN:
    bg2_local = "LEMO_PFN";
    break;
  case LTP::IO_PATGEN:
    bg2_local = "pattern generator";
    break;
  default:
    bg2_local = "invalid";
    break;
  }
  if (m_ltp->IO_linkout_is_from_local(LTP::BG2)) {
    is_entry.BGo2_linkout=bg2_local;
  } else {
    is_entry.BGo2_linkout="linkin";
  }
  if (m_ltp->IO_lemoout_is_from_local(LTP::BG2)) {
    is_entry.BGo2_LEMO_out=bg2_local;
  } else {
    is_entry.BGo2_LEMO_out="linkin";
  }
  if (m_ltp->IO_busy_gating_is_enabled(LTP::BG2)) {
    is_entry.BGo2_busy_gating_enabled = true;
  } else {
    is_entry.BGo2_busy_gating_enabled = false;
  }
  if (m_ltp->IO_inhibit_timer_gating_is_enabled(LTP::BG2)) {
    is_entry.BGo2_inhibit_gating_enabled = true;
  } else {
    is_entry.BGo2_inhibit_gating_enabled = false;
  }
  if (m_ltp->IO_pg_gating_is_enabled(LTP::BG2)) {
    is_entry.BGo2_patgen_gating_enabled = true;
  } else {
    is_entry.BGo2_patgen_gating_enabled = false;
  }
  if (m_ltp->IO_pfn_width_is_1bc(LTP::BG2)) {
    is_entry.BGo2_PFN_duration = "1BC";
  } else {
    is_entry.BGo2_PFN_duration = "2BC";
  }

  // BGo-3
  std::string bg3_local;
  m_ltp->IO_local_selection(LTP::BG3, &iosrc);
  switch (iosrc) {
  case LTP::IO_NO_SOURCE:
    bg3_local = "no_source";
    break;
  case LTP::IO_FP_LEMO:
    bg3_local = "LEMO";
    break;
  case LTP::IO_FP_LEMO_PFN:
    bg3_local = "LEMO_PFN";
    break;
  case LTP::IO_PATGEN:
    bg3_local = "pattern generator";
    break;
  default:
    bg3_local = "invalid";
    break;
  }
  if (m_ltp->IO_linkout_is_from_local(LTP::BG3)) {
    is_entry.BGo3_linkout=bg3_local;
  } else {
    is_entry.BGo3_linkout="linkin";
  }
  if (m_ltp->IO_lemoout_is_from_local(LTP::BG3)) {
    is_entry.BGo3_LEMO_out=bg3_local;
  } else {
    is_entry.BGo3_LEMO_out="linkin";
  }
  if (m_ltp->IO_busy_gating_is_enabled(LTP::BG3)) {
    is_entry.BGo3_busy_gating_enabled = true;
  } else {
    is_entry.BGo3_busy_gating_enabled = false;
  }
  if (m_ltp->IO_inhibit_timer_gating_is_enabled(LTP::BG3)) {
    is_entry.BGo3_inhibit_gating_enabled = true;
  } else {
    is_entry.BGo3_inhibit_gating_enabled = false;
  }
  if (m_ltp->IO_pg_gating_is_enabled(LTP::BG3)) {
    is_entry.BGo3_patgen_gating_enabled = true;
  } else {
    is_entry.BGo3_patgen_gating_enabled = false;
  }
  if (m_ltp->IO_pfn_width_is_1bc(LTP::BG3)) {
    is_entry.BGo3_PFN_duration = "1BC";
  } else {
    is_entry.BGo3_PFN_duration = "2BC";
  }

  int dur=0;
  m_ltp->BUSY_get_deadtime_duration(&dur);
  is_entry.BUSY_deadtime_duration = dur;

  std::string deadtime_source;
  enum LTP::busy_dead_source bdts;
  m_ltp->BUSY_deadtime_source(&bdts);
  switch (bdts) {
  case LTP::DEAD_L1A:
    deadtime_source = "L1A";
    break;
  case LTP::DEAD_TR1:
    deadtime_source = "TestTrigger1";
    break;
  case LTP::DEAD_TR2:
    deadtime_source = "TestTrigger2";
    break;
  case LTP::DEAD_TR3:
    deadtime_source = "TestTrigger3";
    break;
  default:
    deadtime_source = "invalid";
    break;
  }
  is_entry.BUSY_deadtime_trigger_source = deadtime_source;
    
  is_entry.BUSY_local_with_deadtime = m_ltp->BUSY_deadtime_is_summed_with_local()? true : false;
  is_entry.BUSY_local_with_patgen = m_ltp->BUSY_from_pg_is_enabled()? true : false;
  is_entry.BUSY_local_with_NIM = m_ltp->BUSY_nimin_is_summed_with_local()? true : false;
  is_entry.BUSY_local_with_TTL = m_ltp->BUSY_ttlin_is_summed_with_local()? true : false;;
  is_entry.BUSY_local_with_linkin = m_ltp->BUSY_linkin_is_summed_with_local()? true : false;
  is_entry.BUSY_asserted = m_ltp->BUSY_constant_level_is_asserted()? true : false;
    
  if (m_ltp->BUSY_linkout_is_from_local()) {
    is_entry.BUSY_linkout_selection = "local";
  } else {
    is_entry.BUSY_linkout_selection = "linkin";
  }

  is_entry.BUSY_status_linkout  = static_cast<bool>(m_ltp->BUSY_linkout_is_busy());
  bool busy_ttl = (m_ltp->BUSY_linkout_is_from_local() 
		   & m_ltp->BUSY_ttlin_is_summed_with_local()
		   & m_ltp->BUSY_ttlin_is_busy());
  is_entry.BUSY_status_TTL      = busy_ttl;
  bool busy_nim = (m_ltp->BUSY_linkout_is_from_local() 
		   & m_ltp->BUSY_nimin_is_summed_with_local()
		   & m_ltp->BUSY_nimin_is_busy());
  is_entry.BUSY_status_NIM      = busy_nim;
  bool busy_linkin = m_ltp->BUSY_linkin_is_busy() 
    & (m_ltp->BUSY_linkout_is_from_linkin() 
       | m_ltp->BUSY_linkin_is_summed_with_local());
  is_entry.BUSY_status_linkin   = busy_linkin;
  is_entry.BUSY_status_internal = static_cast<bool>(m_ltp->BUSY_internal_is_busy());
  is_entry.BUSY_linkin_name = m_Busy_uid[0];
  is_entry.BUSY_ttl_name = m_Busy_uid[1];
  is_entry.BUSY_nim_name = m_Busy_uid[2];

  // Trigger Type
  if (m_ltp->TTYPE_linkout_is_from_local()) {
    is_entry.TTYPE_linkout_selection = "local";
  } else {
    is_entry.TTYPE_linkout_selection = "linkin";
  }
  if (m_ltp->TTYPE_eclout_is_from_local()) {
    is_entry.TTYPE_fp_source = "local";
  } else {
    is_entry.TTYPE_fp_source = "linkin";
  }
  is_entry.TTYPE_fp_out_enable = m_ltp->TTYPE_eclout_is_disabled()? false : true;

  // Pattern generator

  u_short pgfv;
  m_ltp->PG_get_fixed_value(&pgfv);
  is_entry.PATGEN_fixed_value = pgfv;
    
  if (m_ltp->PG_is_in_continuous_mode()) {
    is_entry.PATGEN_operation_mode = "continuous";
  } else {
    if (!m_ltp->PG_single_shot_trigger_source_is_external()) {
      is_entry.PATGEN_operation_mode = "singleshot_vme";
    }
    else {
      if (m_ltp->PG_external_source_is_BGo3()) {
	is_entry.PATGEN_operation_mode = "singleshot_bgo3";
      } else {
	is_entry.PATGEN_operation_mode = "singleshot_orbit";
      }
    }
  }
 
  enum LTP::pg_op_mode runmode;
  m_ltp->PG_runmode(&runmode);
  switch (runmode) {
  case LTP::PG_PRE_LOAD:
    is_entry.PATGEN_state = "load_memory";
    break;
  case LTP::PG_RUNNING:
    is_entry.PATGEN_state = "running";
    break;
  case LTP::PG_FIXED:
    is_entry.PATGEN_state = "fixed_value";
    break;
  case LTP::PG_NOTUSED:
    is_entry.PATGEN_state = "not_used";
    break;
  default:
    is_entry.PATGEN_state = "invalid";
    break;
  }

  try {
    is_entry.checkin();
  } catch (CORBA::SystemException& ex) {
    ERS_LOG(sout << "Could not publish to IS: CORBA::SystemException ex._name()=" << ex._name());
  } catch (daq::is::Exception & ex) {
    ERS_LOG(sout << "Could not publish to IS: daq::is::Exception ex.what()=" << ex.what());
  } catch (std::exception& ex) {
    ERS_LOG(sout << "Could not publish to IS: std::exception ex.what()=" << ex.what());
  } catch (...) {
    ERS_LOG(sout << "Could not publish to IS. Unknown exception. Rethrowing.");
    throw;
  }

  ERS_DEBUG(1, sout << "Done");
}

// ********
// utility
// ********
template <class T>
bool from_string(T& t1, T& t2, 
		 const std::string& s, 
		 std::ios_base& (*f)(std::ios_base&))
{
  std::istringstream iss(s);
  return !(iss >> f >> t1 >> t2).fail();
}

/******************************************************/
void LTPModule::userCommand(std::string &commandName, std::string &argument)
/******************************************************/
{
  std::string sout = m_UID + "@LTPModule::userCommand() ";
  ERS_LOG(sout << "Entered");

  ERS_LOG(sout << std::dec << "Received userCommand \"" << commandName.c_str() << "\" with argument \"" << argument << "\"");

  if (commandName == "COUNTER") {
    if (argument == "L1A") {
      ERS_LOG(sout << "Setting counter to L1A and reset");
      m_ltp->COUNTER_L1A();
      m_ltp->COUNTER_reset();
    } else if (argument == "TT1") {
      ERS_LOG(sout << "Setting counter to TestTrigger1 and reset");
      m_ltp->COUNTER_TR1();
      m_ltp->COUNTER_reset();
    } else if (argument == "TT2") {
      ERS_LOG(sout << "Setting counter to TestTrigger2 and reset");
      m_ltp->COUNTER_TR2();
      m_ltp->COUNTER_reset();
    } else if (argument == "TT3") {
      ERS_LOG(sout << "Setting counter to TestTrigger3 and reset");
      m_ltp->COUNTER_TR3();
      m_ltp->COUNTER_reset();
    } else if (argument == "L1A_ECR") {
      ERS_LOG(sout << "Setting counter to L1A_ECR and reset");
      m_ltp->COUNTER_L1A_ecr();
      m_ltp->COUNTER_reset();
    } else if (argument == "TT1_ECR") {
      ERS_LOG(sout << "Setting counter to TestTrigger1_ECR and reset");
      m_ltp->COUNTER_TR1_ecr();
      m_ltp->COUNTER_reset();
    } else if (argument == "TT2_ECR") {
      ERS_LOG(sout << "Setting counter to TestTrigger2_ECR and reset");
      m_ltp->COUNTER_TR2_ecr();
      m_ltp->COUNTER_reset();
    } else if (argument == "TT3_ECR") {
      ERS_LOG(sout << "Setting counter to TestTrigger3_ECR and reset");
      m_ltp->COUNTER_TR3_ecr();
      m_ltp->COUNTER_reset();
    } else if (argument == "BG0") {
      ERS_LOG(sout << "Setting counter to BGo0 and reset");
      m_ltp->COUNTER_BG0();
      m_ltp->COUNTER_reset();
    } else if (argument == "BG1") {
      ERS_LOG(sout << "Setting counter to BGo1 and reset");
      m_ltp->COUNTER_BG1();
      m_ltp->COUNTER_reset();
    } else if (argument == "BG2") {
      ERS_LOG(sout << "Setting counter to BGo2 and reset");
      m_ltp->COUNTER_BG2();
      m_ltp->COUNTER_reset();
    } else if (argument == "BG3") {
      ERS_LOG(sout << "Setting counter to BGo3 and reset");
      m_ltp->COUNTER_BG3();
      m_ltp->COUNTER_reset();
    } else if (argument == "ORB") {
      ERS_LOG(sout << "Setting counter to Orbit and reset");
      m_ltp->COUNTER_orb();
      m_ltp->COUNTER_reset();
    } else if (argument == "ORB_ECR") {
      ERS_LOG(sout << "Setting counter to Orbit_ECR and reset");
      m_ltp->COUNTER_orb_ecr();
      m_ltp->COUNTER_reset();
    } else if (argument == "OFF") {
      ERS_LOG(sout << "Setting counter to none and reset");
      m_ltp->COUNTER_none();
      m_ltp->COUNTER_reset();
    } else {
      ERS_LOG(sout << "Leaving counter unchanged");
    }
  }
  else if (commandName == "DISABLE") {
    this->disableBusy(argument);
  } 
  else if (commandName == "ENABLE") {
    this->enableBusy(argument);
  }
  else {
    ERS_LOG(sout << "UNKNOWN MESSAGE received");
  }

  ERS_LOG(sout << "Done");
}


// ----------------------------------------------------------------------------------
void  LTPModule::configureSlave(const RCDLTPdal::LTPSlaveConfig* ltpConfig) {
  std::string sout = m_UID + "@LTPModule::configureSlave() ";
  ERS_LOG(sout << "Entered");

  // dump configuration object
  ERS_LOG(sout << "called with LTPSlaveConfig = " << ltpConfig);
  ltpConfig->print(1,true,std::cout);

  // configure according to slave
  m_use_patgen = false;
  m_ecr_at_resume = false;
  m_ecr_periodic = false;
  m_ecr_period = 0;
  m_useTTC2LAN = false;

  int status = 0;

  // configuration attributes
  m_boardReset = ltpConfig->get_BoardReset();
  ERS_LOG(sout << "BoardReset = " << m_boardReset);

  // reset
  if (m_boardReset) {
    status |= m_ltp->Reset();
  }

  // bc
  status |= m_ltp->BC_linkout_from_linkin();
  status |= m_ltp->BC_lemoout_from_linkin();

  // orbit
  status |= m_ltp->ORB_linkout_from_linkin();
  status |= m_ltp->ORB_lemoout_from_linkin();
  status |= m_ltp->ORB_local_no();

  // link in to link out for all signals
  status |= m_ltp->IO_linkout_from_linkin(RCD::LTP::L1A);
  status |= m_ltp->IO_lemoout_from_linkin(RCD::LTP::L1A);
  status |= m_ltp->IO_local_no_source(RCD::LTP::L1A);
  status |= m_ltp->IO_disable_busy_gating(RCD::LTP::L1A);
  status |= m_ltp->IO_disable_inhibit_gating(RCD::LTP::L1A);
  status |= m_ltp->IO_disable_pg_gating(RCD::LTP::L1A);

  status |= m_ltp->IO_linkout_from_linkin(RCD::LTP::TR1);
  status |= m_ltp->IO_lemoout_from_linkin(RCD::LTP::TR1);
  status |= m_ltp->IO_local_no_source(RCD::LTP::TR1);
  status |= m_ltp->IO_disable_busy_gating(RCD::LTP::TR1);
  status |= m_ltp->IO_disable_inhibit_gating(RCD::LTP::TR1);
  status |= m_ltp->IO_disable_pg_gating(RCD::LTP::TR1);

  status |= m_ltp->IO_linkout_from_linkin(RCD::LTP::TR2);
  status |= m_ltp->IO_lemoout_from_linkin(RCD::LTP::TR2);
  status |= m_ltp->IO_local_no_source(RCD::LTP::TR2);
  status |= m_ltp->IO_disable_busy_gating(RCD::LTP::TR2);
  status |= m_ltp->IO_disable_inhibit_gating(RCD::LTP::TR2);
  status |= m_ltp->IO_disable_pg_gating(RCD::LTP::TR2);

  status |= m_ltp->IO_linkout_from_linkin(RCD::LTP::TR3);
  status |= m_ltp->IO_lemoout_from_linkin(RCD::LTP::TR3);
  status |= m_ltp->IO_local_no_source(RCD::LTP::TR3);
  status |= m_ltp->IO_disable_busy_gating(RCD::LTP::TR3);
  status |= m_ltp->IO_disable_inhibit_gating(RCD::LTP::TR3);
  status |= m_ltp->IO_disable_pg_gating(RCD::LTP::TR3);

  status |= m_ltp->IO_linkout_from_linkin(RCD::LTP::BG0);
  status |= m_ltp->IO_lemoout_from_linkin(RCD::LTP::BG0);
  status |= m_ltp->IO_local_no_source(RCD::LTP::BG0);
  status |= m_ltp->IO_disable_busy_gating(RCD::LTP::BG0);
  status |= m_ltp->IO_disable_inhibit_gating(RCD::LTP::BG0);
  status |= m_ltp->IO_disable_pg_gating(RCD::LTP::BG0);

  status |= m_ltp->IO_linkout_from_linkin(RCD::LTP::BG1);
  status |= m_ltp->IO_lemoout_from_linkin(RCD::LTP::BG1);
  status |= m_ltp->IO_local_no_source(RCD::LTP::BG1);
  status |= m_ltp->IO_disable_busy_gating(RCD::LTP::BG1);
  status |= m_ltp->IO_disable_inhibit_gating(RCD::LTP::BG1);
  status |= m_ltp->IO_disable_pg_gating(RCD::LTP::BG1);

  status |= m_ltp->IO_linkout_from_linkin(RCD::LTP::BG2);
  status |= m_ltp->IO_lemoout_from_linkin(RCD::LTP::BG2);
  status |= m_ltp->IO_local_no_source(RCD::LTP::BG2);
  status |= m_ltp->IO_disable_busy_gating(RCD::LTP::BG2);
  status |= m_ltp->IO_disable_inhibit_gating(RCD::LTP::BG2);
  status |= m_ltp->IO_disable_pg_gating(RCD::LTP::BG2);

  status |= m_ltp->IO_linkout_from_linkin(RCD::LTP::BG3);
  status |= m_ltp->IO_lemoout_from_linkin(RCD::LTP::BG3);
  status |= m_ltp->IO_local_no_source(RCD::LTP::BG3);
  status |= m_ltp->IO_disable_busy_gating(RCD::LTP::BG3);
  status |= m_ltp->IO_disable_inhibit_gating(RCD::LTP::BG3);
  status |= m_ltp->IO_disable_pg_gating(RCD::LTP::BG3);

  // busy
  status |= m_ltp->BUSY_lemoout_busy_only();
  status |= m_ltp->BUSY_linkout_from_local();

  // configure busy mask according to values obtained from database during setup()
  m_Busy_current[0] = m_Busy_local_with_linkin;
  m_Busy_current[1] = m_Busy_local_with_ttlin;
  m_Busy_current[2] = m_Busy_local_with_nimin;
  this->updateBusy();

  status |= m_ltp->BUSY_local_without_deadtime  ();
  status |= m_ltp->BUSY_disable_constant_level  ();

  // trigger type
  status |= m_ltp->TTYPE_linkout_from_linkin();
  status |= m_ltp->TTYPE_eclout_from_linkin();
  status |= m_ltp->TTYPE_enable_eclout();
  status |= m_ltp->TTYPE_fifotrigger_L1A();
  status |= m_ltp->TTYPE_reset_fifo();

  // cal req
  status |= m_ltp->CAL_source_link();
  status |= m_ltp->CAL_enable_testpath();
 
  // counter
  status |= m_ltp->COUNTER_reset();

  // pattern generator
  status |= m_ltp->PG_reset();
  status |= m_ltp->PG_runmode_notused();

  if (status != 0) {
    ostringstream text;
    text << sout << "VME error, status="<< status << "(0x" << std::hex << status << std::dec << ")";
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
  }

  ERS_LOG(sout << "Done");
}

// ----------------------------------------------------------------------------------
void  LTPModule::configureLemo(const RCDLTPdal::LTPLemoConfig* ltpConfig) {
  std::string sout = m_UID + "@LTPModule::configureLemo() ";
  ERS_LOG(sout << "Entered");

  // dump configuration object
  ERS_LOG(sout << "called with LTPLemoConfig = " << ltpConfig);
  ltpConfig->print(1,true,std::cout);

  m_use_patgen = false;

  // configuration attributes
  m_boardReset = ltpConfig->get_BoardReset();
  ERS_LOG(sout << "BoardReset = " << m_boardReset);
  std::string a_Trigger = ltpConfig->get_Trigger();
  ERS_LOG(sout << "Trigger = " << a_Trigger);
  u_short a_TriggerType = 0xff & ltpConfig->get_TriggerType();
  ERS_LOG(sout << "TriggerType = 0x" << std::hex <<  a_TriggerType << std::dec << " (" << a_TriggerType << ")");

  int status = 0;

  // reset
  if (m_boardReset) {
    status |= m_ltp->Reset();
  }

  // bc
  status |= m_ltp->BC_local_intern();
  status |= m_ltp->BC_linkout_from_local();
  status |= m_ltp->BC_lemoout_from_local();

  // orbit
  status |= m_ltp->ORB_local_intern();
  status |= m_ltp->ORB_linkout_from_local();
  status |= m_ltp->ORB_lemoout_from_local();

  // link out from local for all signals
  status |= m_ltp->IO_linkout_from_local(RCD::LTP::L1A);
  status |= m_ltp->IO_lemoout_from_local(RCD::LTP::L1A);
  status |= m_ltp->IO_local_lemo_pfn(RCD::LTP::L1A);
  status |= m_ltp->IO_enable_busy_gating(RCD::LTP::L1A);
  status |= m_ltp->IO_disable_inhibit_gating(RCD::LTP::L1A);
  status |= m_ltp->IO_disable_pg_gating(RCD::LTP::L1A);
  status |= m_ltp->IO_pfn_width_1bc(RCD::LTP::L1A);

  status |= m_ltp->IO_linkout_from_local(RCD::LTP::TR1);
  status |= m_ltp->IO_lemoout_from_local(RCD::LTP::TR1);
  status |= m_ltp->IO_local_lemo_pfn(RCD::LTP::TR1);
  status |= m_ltp->IO_enable_busy_gating(RCD::LTP::TR1);
  status |= m_ltp->IO_disable_inhibit_gating(RCD::LTP::TR1);
  status |= m_ltp->IO_disable_pg_gating(RCD::LTP::TR1);
  status |= m_ltp->IO_pfn_width_1bc(RCD::LTP::TR1);

  status |= m_ltp->IO_linkout_from_local(RCD::LTP::TR2);
  status |= m_ltp->IO_lemoout_from_local(RCD::LTP::TR2);
  status |= m_ltp->IO_local_lemo_pfn(RCD::LTP::TR2);
  status |= m_ltp->IO_enable_busy_gating(RCD::LTP::TR2);
  status |= m_ltp->IO_disable_inhibit_gating(RCD::LTP::TR2);
  status |= m_ltp->IO_disable_pg_gating(RCD::LTP::TR2);
  status |= m_ltp->IO_pfn_width_1bc(RCD::LTP::TR2);

  status |= m_ltp->IO_linkout_from_local(RCD::LTP::TR3);
  status |= m_ltp->IO_lemoout_from_local(RCD::LTP::TR3);
  status |= m_ltp->IO_local_lemo_pfn(RCD::LTP::TR3);
  status |= m_ltp->IO_enable_busy_gating(RCD::LTP::TR3);
  status |= m_ltp->IO_disable_inhibit_gating(RCD::LTP::TR3);
  status |= m_ltp->IO_disable_pg_gating(RCD::LTP::TR3);
  status |= m_ltp->IO_pfn_width_1bc(RCD::LTP::TR3);

  status |= m_ltp->IO_linkout_from_local(RCD::LTP::BG0);
  status |= m_ltp->IO_lemoout_from_local(RCD::LTP::BG0);
  status |= m_ltp->IO_local_lemo_pfn(RCD::LTP::BG0);
  status |= m_ltp->IO_disable_busy_gating(RCD::LTP::BG0);
  status |= m_ltp->IO_disable_inhibit_gating(RCD::LTP::BG0);
  status |= m_ltp->IO_disable_pg_gating(RCD::LTP::BG0);
  status |= m_ltp->IO_pfn_width_1bc(RCD::LTP::BG0);

  status |= m_ltp->IO_linkout_from_local(RCD::LTP::BG1);
  status |= m_ltp->IO_lemoout_from_local(RCD::LTP::BG1);
  status |= m_ltp->IO_local_lemo_pfn(RCD::LTP::BG1);
  status |= m_ltp->IO_disable_busy_gating(RCD::LTP::BG1);
  status |= m_ltp->IO_disable_inhibit_gating(RCD::LTP::BG1);
  status |= m_ltp->IO_disable_pg_gating(RCD::LTP::BG1);
  status |= m_ltp->IO_pfn_width_1bc(RCD::LTP::BG1);

  status |= m_ltp->IO_linkout_from_local(RCD::LTP::BG2);
  status |= m_ltp->IO_lemoout_from_local(RCD::LTP::BG2);
  status |= m_ltp->IO_local_lemo_pfn(RCD::LTP::BG2);
  status |= m_ltp->IO_disable_busy_gating(RCD::LTP::BG2);
  status |= m_ltp->IO_disable_inhibit_gating(RCD::LTP::BG2);
  status |= m_ltp->IO_disable_pg_gating(RCD::LTP::BG2);
  status |= m_ltp->IO_pfn_width_1bc(RCD::LTP::BG2);

  status |= m_ltp->IO_linkout_from_local(RCD::LTP::BG3);
  status |= m_ltp->IO_lemoout_from_local(RCD::LTP::BG3);
  status |= m_ltp->IO_local_lemo_pfn(RCD::LTP::BG3);
  status |= m_ltp->IO_disable_busy_gating(RCD::LTP::BG3);
  status |= m_ltp->IO_disable_inhibit_gating(RCD::LTP::BG3);
  status |= m_ltp->IO_disable_pg_gating(RCD::LTP::BG3);
  status |= m_ltp->IO_pfn_width_1bc(RCD::LTP::BG3);

  // busy
  status |= m_ltp->BUSY_linkout_from_local();

  // configure busy mask according to values obtained from database during setup()
  m_Busy_current[0] = m_Busy_local_with_linkin;
  m_Busy_current[1] = m_Busy_local_with_ttlin;
  m_Busy_current[2] = m_Busy_local_with_nimin;
  this->updateBusy();

  status |= m_ltp->BUSY_disable_from_pg();
  // add deadtime of 
  status |= m_ltp->BUSY_local_with_deadtime();
  status |= m_ltp->BUSY_deadtime_duration(1); // corresponds to 5BC of deadtime
  status |= m_ltp->BUSY_lemoout_busy_only();

  // trigger type
  status |= m_ltp->TTYPE_linkout_from_local();
  status |= m_ltp->TTYPE_eclout_from_local();
  status |= m_ltp->TTYPE_enable_eclout();
  status |= m_ltp->TTYPE_local_from_vme();
  status |= m_ltp->TTYPE_regfile_address_0();
  status |= m_ltp->TTYPE_write_regfile0(a_TriggerType);
  status |= m_ltp->TTYPE_write_regfile1(0x0000);
  status |= m_ltp->TTYPE_write_regfile2(0x0000);
  status |= m_ltp->TTYPE_write_regfile3(0x0000);
  status |= m_ltp->TTYPE_fifodelay_2bc();
  status |= m_ltp->TTYPE_reset_fifo();

  // cal req
  status |= m_ltp->CAL_source_fp();
  status |= m_ltp->CAL_enable_testpath();
 
  // counter
  if (a_Trigger == RCDLTPdal::LTPLemoConfig::Trigger::L1A) {
    ERS_LOG(sout << "Trigger = L1A");
    status |= m_ltp->TTYPE_fifotrigger_L1A();
    status |= m_ltp->BUSY_deadtime_source_L1A();
  }
  else if (a_Trigger == RCDLTPdal::LTPLemoConfig::Trigger::TestTrigger1) {
    ERS_LOG(sout << "Trigger = TestTrigger1");
    status |= m_ltp->TTYPE_fifotrigger_TR1();
    status |= m_ltp->BUSY_deadtime_source_TR1();
  }
  else if (a_Trigger == RCDLTPdal::LTPLemoConfig::Trigger::TestTrigger2) {
    ERS_LOG(sout << "Trigger = TestTrigger2");
    status |= m_ltp->TTYPE_fifotrigger_TR2();
    status |= m_ltp->BUSY_deadtime_source_TR2();
  }
  else if (a_Trigger == RCDLTPdal::LTPLemoConfig::Trigger::TestTrigger3) {
    ERS_LOG(sout << "Trigger = TestTrigger3");
    status |= m_ltp->TTYPE_fifotrigger_TR3();
    status |= m_ltp->BUSY_deadtime_source_TR3();
  } else {
    ostringstream text;
    text << sout << "unknown Trigger = \"" << a_Trigger << "\"";
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
  }
  
  status |= m_ltp->COUNTER_reset();

  // pattern generator
  status |= m_ltp->PG_reset();
  status |= m_ltp->PG_runmode_notused();

  if (status != 0) {
    ostringstream text;
    text << sout << "VME error, status="<< status << "(0x" << std::hex << status << std::dec << ")";
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
  }

  ERS_LOG(sout << "Done");
}

// ----------------------------------------------------------------------------------
void  LTPModule::configureCTPParasite(const RCDLTPdal::LTPCTPParasiteConfig* ltpConfig) {
  std::string sout = m_UID + "@LTPModule::configureCTPParasite() ";
  ERS_LOG(sout << "Entered");

  // dump configuration object
  ERS_LOG(sout << "called with LTPCTPParasiteConfig = " << ltpConfig);
  ltpConfig->print(1,true,std::cout);

  m_use_patgen = false;

  // configuration attributes
  m_boardReset = ltpConfig->get_BoardReset();
  ERS_LOG(sout << "BoardReset = " << m_boardReset);
  std::string a_Deadtime = ltpConfig->get_Deadtime();
  ERS_LOG(sout << "Deadtime = " << a_Deadtime);
  u_short a_TriggerType = 0xff & ltpConfig->get_TriggerType();
  ERS_LOG(sout << "TriggerType = 0x" << std::hex <<  a_TriggerType << std::dec << " (" << a_TriggerType << ")");

  int status = 0;

  // reset
  if (m_boardReset) {
    status |= m_ltp->Reset();
  }

  // bc
  status |= m_ltp->BC_linkout_from_linkin();
  status |= m_ltp->BC_lemoout_from_linkin();

  // orbit
  status |= m_ltp->ORB_linkout_from_linkin();
  status |= m_ltp->ORB_lemoout_from_linkin();
  status |= m_ltp->ORB_local_no();

  // link in to link out for all signals
  status |= m_ltp->IO_linkout_from_local(RCD::LTP::L1A);
  status |= m_ltp->IO_lemoout_from_linkin(RCD::LTP::L1A);
  status |= m_ltp->IO_local_no_source(RCD::LTP::L1A);
  status |= m_ltp->IO_enable_busy_gating(RCD::LTP::L1A);
  status |= m_ltp->IO_disable_inhibit_gating(RCD::LTP::L1A);
  status |= m_ltp->IO_disable_pg_gating(RCD::LTP::L1A);

  status |= m_ltp->IO_linkout_from_local(RCD::LTP::TR1);
  status |= m_ltp->IO_lemoout_from_linkin(RCD::LTP::TR1);
  status |= m_ltp->IO_local_no_source(RCD::LTP::TR1);
  status |= m_ltp->IO_disable_busy_gating(RCD::LTP::TR1);
  status |= m_ltp->IO_disable_inhibit_gating(RCD::LTP::TR1);
  status |= m_ltp->IO_disable_pg_gating(RCD::LTP::TR1);

  status |= m_ltp->IO_linkout_from_local(RCD::LTP::TR2);
  status |= m_ltp->IO_lemoout_from_linkin(RCD::LTP::TR2);
  status |= m_ltp->IO_local_no_source(RCD::LTP::TR2);
  status |= m_ltp->IO_disable_busy_gating(RCD::LTP::TR2);
  status |= m_ltp->IO_disable_inhibit_gating(RCD::LTP::TR2);
  status |= m_ltp->IO_disable_pg_gating(RCD::LTP::TR2);

  status |= m_ltp->IO_linkout_from_local(RCD::LTP::TR3);
  status |= m_ltp->IO_lemoout_from_linkin(RCD::LTP::TR3);
  status |= m_ltp->IO_local_no_source(RCD::LTP::TR3);
  status |= m_ltp->IO_disable_busy_gating(RCD::LTP::TR3);
  status |= m_ltp->IO_disable_inhibit_gating(RCD::LTP::TR3);
  status |= m_ltp->IO_disable_pg_gating(RCD::LTP::TR3);

  status |= m_ltp->IO_linkout_from_local(RCD::LTP::BG0);
  status |= m_ltp->IO_lemoout_from_linkin(RCD::LTP::BG0);
  status |= m_ltp->IO_local_no_source(RCD::LTP::BG0);
  status |= m_ltp->IO_disable_busy_gating(RCD::LTP::BG0);
  status |= m_ltp->IO_disable_inhibit_gating(RCD::LTP::BG0);
  status |= m_ltp->IO_disable_pg_gating(RCD::LTP::BG0);

  status |= m_ltp->IO_linkout_from_local(RCD::LTP::BG1);
  status |= m_ltp->IO_lemoout_from_linkin(RCD::LTP::BG1);
  status |= m_ltp->IO_local_no_source(RCD::LTP::BG1);
  status |= m_ltp->IO_disable_busy_gating(RCD::LTP::BG1);
  status |= m_ltp->IO_disable_inhibit_gating(RCD::LTP::BG1);
  status |= m_ltp->IO_disable_pg_gating(RCD::LTP::BG1);

  status |= m_ltp->IO_linkout_from_local(RCD::LTP::BG2);
  status |= m_ltp->IO_lemoout_from_linkin(RCD::LTP::BG2);
  status |= m_ltp->IO_local_no_source(RCD::LTP::BG2);
  status |= m_ltp->IO_disable_busy_gating(RCD::LTP::BG2);
  status |= m_ltp->IO_disable_inhibit_gating(RCD::LTP::BG2);
  status |= m_ltp->IO_disable_pg_gating(RCD::LTP::BG2);

  status |= m_ltp->IO_linkout_from_local(RCD::LTP::BG3);
  status |= m_ltp->IO_lemoout_from_linkin(RCD::LTP::BG3);
  status |= m_ltp->IO_local_no_source(RCD::LTP::BG3);
  status |= m_ltp->IO_disable_busy_gating(RCD::LTP::BG3);
  status |= m_ltp->IO_disable_inhibit_gating(RCD::LTP::BG3);
  status |= m_ltp->IO_disable_pg_gating(RCD::LTP::BG3);

  // busy
  status |= m_ltp->BUSY_linkout_from_local();

  // configure busy mask according to values obtained from database during setup()
  m_Busy_current[0] = m_Busy_local_with_linkin;
  m_Busy_current[1] = m_Busy_local_with_ttlin;
  m_Busy_current[2] = m_Busy_local_with_nimin;
  this->updateBusy();

  status |= m_ltp->BUSY_disable_from_pg();
  // simple deadtime
  if (a_Deadtime == RCDLTPdal::LTPCTPParasiteConfig::Deadtime::None) {
    status |= m_ltp->BUSY_local_without_deadtime();
  } else {
    status |= m_ltp->BUSY_local_with_deadtime();
    if      (a_Deadtime ==  RCDLTPdal::LTPCTPParasiteConfig::Deadtime::_4BC) status |= m_ltp->BUSY_deadtime_duration(0);
    else if (a_Deadtime ==  RCDLTPdal::LTPCTPParasiteConfig::Deadtime::_5BC) status |= m_ltp->BUSY_deadtime_duration(1);
    else if (a_Deadtime ==  RCDLTPdal::LTPCTPParasiteConfig::Deadtime::_6BC) status |= m_ltp->BUSY_deadtime_duration(2);
    else if (a_Deadtime ==  RCDLTPdal::LTPCTPParasiteConfig::Deadtime::_7BC) status |= m_ltp->BUSY_deadtime_duration(3);
    else if (a_Deadtime ==  RCDLTPdal::LTPCTPParasiteConfig::Deadtime::_8BC) status |= m_ltp->BUSY_deadtime_duration(4);
    else if (a_Deadtime ==  RCDLTPdal::LTPCTPParasiteConfig::Deadtime::_9BC) status |= m_ltp->BUSY_deadtime_duration(5);
    else if (a_Deadtime == RCDLTPdal::LTPCTPParasiteConfig::Deadtime::_10BC) status |= m_ltp->BUSY_deadtime_duration(6);
    else if (a_Deadtime == RCDLTPdal::LTPCTPParasiteConfig::Deadtime::_11BC) status |= m_ltp->BUSY_deadtime_duration(7);
  }

  status |= m_ltp->BUSY_lemoout_busy_only();

  // trigger type
  status |= m_ltp->TTYPE_linkout_from_local();
  status |= m_ltp->TTYPE_eclout_from_local();
  status |= m_ltp->TTYPE_enable_eclout();
  status |= m_ltp->TTYPE_local_from_vme();
  status |= m_ltp->TTYPE_regfile_address_0();
  status |= m_ltp->TTYPE_write_regfile0(a_TriggerType);
  status |= m_ltp->TTYPE_write_regfile1(0x0000);
  status |= m_ltp->TTYPE_write_regfile2(0x0000);
  status |= m_ltp->TTYPE_write_regfile3(0x0000);
  status |= m_ltp->TTYPE_fifodelay_2bc();
  status |= m_ltp->TTYPE_reset_fifo();

  status |= m_ltp->TTYPE_fifotrigger_L1A();
  status |= m_ltp->BUSY_deadtime_source_L1A();

  status |= m_ltp->COUNTER_reset();

  // pattern generator
  status |= m_ltp->PG_reset();
  status |= m_ltp->PG_runmode_notused();

  if (status != 0) {
    ostringstream text;
    text << sout << "VME error, status="<< status << "(0x" << std::hex << status << std::dec << ")";
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
  }

  ERS_LOG(sout << "Done");
}



// ----------------------------------------------------------------------------------
void  LTPModule::configureExpert(const RCDLTPdal::LTPExpertConfig* ltpConfig) {
  std::string sout = m_UID + "@LTPModule::configureExpert() ";
  ERS_LOG(sout << "Entered");

  // dump configuration object
  ERS_LOG(sout << "called with LTPExpertConfig = " << ltpConfig);
  ltpConfig->print(1,true,std::cout);

  // configure according to expert
  
  int status = 0;

  m_boardReset = ltpConfig->get_BoardReset();
  ERS_LOG(sout << "BoardReset = " << m_boardReset);

  // reset
  if (m_boardReset) {
    status |= m_ltp->Reset();
  }

  // BC
  std::string a_BC_local = ltpConfig->get_BC_local();
  ERS_LOG(sout << "BC_local = " << a_BC_local);
  std::string a_BC_lemoout_from = ltpConfig->get_BC_lemoout_from();
  ERS_LOG(sout << "BC_lemoout_from = " << a_BC_lemoout_from);
  std::string a_BC_linkout_from = ltpConfig->get_BC_linkout_from();
  ERS_LOG(sout << "BC_linkout_from = " << a_BC_linkout_from);
  if (a_BC_local == RCDLTPdal::LTPExpertConfig::BC_local::Local_oscillator) status |= m_ltp->BC_local_intern();
  else if (a_BC_local == RCDLTPdal::LTPExpertConfig::BC_local::Lemo) status |= m_ltp->BC_local_lemo();
  if (a_BC_lemoout_from == RCDLTPdal::LTPExpertConfig::BC_lemoout_from::Local) status |= m_ltp->BC_lemoout_from_local();
  else if (a_BC_lemoout_from == RCDLTPdal::LTPExpertConfig::BC_lemoout_from::Linkin) status |= m_ltp->BC_lemoout_from_linkin();
  if (a_BC_linkout_from == RCDLTPdal::LTPExpertConfig::BC_linkout_from::Local) status |= m_ltp->BC_linkout_from_local();
  else if (a_BC_linkout_from == RCDLTPdal::LTPExpertConfig::BC_linkout_from::Linkin) status |= m_ltp->BC_linkout_from_linkin();

  // Orbit
  std::string a_Orbit_local = ltpConfig->get_Orbit_local();
  ERS_LOG(sout << "Orbit_local = " << a_Orbit_local);
  std::string a_Orbit_lemoout_from = ltpConfig->get_Orbit_lemoout_from();
  ERS_LOG(sout << "Orbit_lemoout_from = " << a_Orbit_lemoout_from);
  std::string a_Orbit_linkout_from = ltpConfig->get_Orbit_linkout_from();
  ERS_LOG(sout << "Orbit_linkout_from = " << a_Orbit_linkout_from);

  if (a_Orbit_local == RCDLTPdal::LTPExpertConfig::Orbit_local::None) status |= m_ltp->ORB_local_no();
  else if (a_Orbit_local == RCDLTPdal::LTPExpertConfig::Orbit_local::Internal) status |= m_ltp->ORB_local_intern();
  else if (a_Orbit_local == RCDLTPdal::LTPExpertConfig::Orbit_local::Lemo) status |= m_ltp->ORB_local_lemo();
  else if (a_Orbit_local == RCDLTPdal::LTPExpertConfig::Orbit_local::Patgen) status |= m_ltp->ORB_local_pg();

  if (a_Orbit_lemoout_from == RCDLTPdal::LTPExpertConfig::Orbit_lemoout_from::Local) status |= m_ltp->ORB_lemoout_from_local();
  else if (a_Orbit_lemoout_from == RCDLTPdal::LTPExpertConfig::Orbit_lemoout_from::Linkin) status |= m_ltp->ORB_lemoout_from_linkin();

  if (a_Orbit_linkout_from == RCDLTPdal::LTPExpertConfig::Orbit_linkout_from::Local) status |= m_ltp->ORB_linkout_from_local();
  else if (a_Orbit_linkout_from == RCDLTPdal::LTPExpertConfig::Orbit_linkout_from::Linkin) status |= m_ltp->ORB_linkout_from_linkin();

  // triggers and bgo

  std::string a_L1A_local = ltpConfig->get_L1A_local();
  ERS_LOG(sout << "L1A_local = " << a_L1A_local);
  std::string a_L1A_lemoout_from = ltpConfig->get_L1A_lemoout_from();
  ERS_LOG(sout << "L1A_lemoout_from = " << a_L1A_lemoout_from);
  std::string a_L1A_linkout_from = ltpConfig->get_L1A_linkout_from();
  ERS_LOG(sout << "L1A_linkout_from = " << a_L1A_linkout_from);
  std::string a_L1A_gating = ltpConfig->get_L1A_gating();
  ERS_LOG(sout << "L1A_gating = " << a_L1A_gating);
  std::string a_L1A_pfn_width = ltpConfig->get_L1A_pfn_width();
  ERS_LOG(sout << "L1A_pfn_width = " << a_L1A_pfn_width);
  std::string a_TestTrigger1_local = ltpConfig->get_TestTrigger1_local();
  ERS_LOG(sout << "TestTrigger1_local = " << a_TestTrigger1_local);
  std::string a_TestTrigger1_lemoout_from = ltpConfig->get_TestTrigger1_lemoout_from();
  ERS_LOG(sout << "TestTrigger1_lemoout_from = " << a_TestTrigger1_lemoout_from);
  std::string a_TestTrigger1_linkout_from = ltpConfig->get_TestTrigger1_linkout_from();
  ERS_LOG(sout << "TestTrigger1_linkout_from = " << a_TestTrigger1_linkout_from);
  std::string a_TestTrigger1_gating = ltpConfig->get_TestTrigger1_gating();
  ERS_LOG(sout << "TestTrigger1_gating = " << a_TestTrigger1_gating);
  std::string a_TestTrigger1_pfn_width = ltpConfig->get_TestTrigger1_pfn_width();
  ERS_LOG(sout << "TestTrigger1_pfn_width = " << a_TestTrigger1_pfn_width);
  std::string a_TestTrigger2_local = ltpConfig->get_TestTrigger2_local();
  ERS_LOG(sout << "TestTrigger2_local = " << a_TestTrigger2_local);
  std::string a_TestTrigger2_lemoout_from = ltpConfig->get_TestTrigger2_lemoout_from();
  ERS_LOG(sout << "TestTrigger2_lemoout_from = " << a_TestTrigger2_lemoout_from);
  std::string a_TestTrigger2_linkout_from = ltpConfig->get_TestTrigger2_linkout_from();
  ERS_LOG(sout << "TestTrigger2_linkout_from = " << a_TestTrigger2_linkout_from);
  std::string a_TestTrigger2_gating = ltpConfig->get_TestTrigger2_gating();
  ERS_LOG(sout << "TestTrigger2_gating = " << a_TestTrigger2_gating);
  std::string a_TestTrigger2_pfn_width = ltpConfig->get_TestTrigger2_pfn_width();
  ERS_LOG(sout << "TestTrigger2_pfn_width = " << a_TestTrigger2_pfn_width);
  std::string a_TestTrigger3_local = ltpConfig->get_TestTrigger3_local();
  ERS_LOG(sout << "TestTrigger3_local = " << a_TestTrigger3_local);
  std::string a_TestTrigger3_lemoout_from = ltpConfig->get_TestTrigger3_lemoout_from();
  ERS_LOG(sout << "TestTrigger3_lemoout_from = " << a_TestTrigger3_lemoout_from);
  std::string a_TestTrigger3_linkout_from = ltpConfig->get_TestTrigger3_linkout_from();
  ERS_LOG(sout << "TestTrigger3_linkout_from = " << a_TestTrigger3_linkout_from);
  std::string a_TestTrigger3_gating = ltpConfig->get_TestTrigger3_gating();
  ERS_LOG(sout << "TestTrigger3_gating = " << a_TestTrigger3_gating);
  std::string a_TestTrigger3_pfn_width = ltpConfig->get_TestTrigger3_pfn_width();
  ERS_LOG(sout << "TestTrigger3_pfn_width = " << a_TestTrigger3_pfn_width);

  std::string a_BGo0_local = ltpConfig->get_BGo0_local();
  ERS_LOG(sout << "BGo0_local = " << a_BGo0_local);
  std::string a_BGo0_lemoout_from = ltpConfig->get_BGo0_lemoout_from();
  ERS_LOG(sout << "BGo0_lemoout_from = " << a_BGo0_lemoout_from);
  std::string a_BGo0_linkout_from = ltpConfig->get_BGo0_linkout_from();
  ERS_LOG(sout << "BGo0_linkout_from = " << a_BGo0_linkout_from);
  std::string a_BGo0_gating = ltpConfig->get_BGo0_gating();
  ERS_LOG(sout << "BGo0_gating = " << a_BGo0_gating);
  std::string a_BGo0_pfn_width = ltpConfig->get_BGo0_pfn_width();
  ERS_LOG(sout << "BGo0_pfn_width = " << a_BGo0_pfn_width);
  std::string a_BGo1_local = ltpConfig->get_BGo1_local();
  ERS_LOG(sout << "BGo1_local = " << a_BGo1_local);
  std::string a_BGo1_lemoout_from = ltpConfig->get_BGo1_lemoout_from();
  ERS_LOG(sout << "BGo1_lemoout_from = " << a_BGo1_lemoout_from);
  std::string a_BGo1_linkout_from = ltpConfig->get_BGo1_linkout_from();
  ERS_LOG(sout << "BGo1_linkout_from = " << a_BGo1_linkout_from);
  std::string a_BGo1_gating = ltpConfig->get_BGo1_gating();
  ERS_LOG(sout << "BGo1_gating = " << a_BGo1_gating);
  std::string a_BGo1_pfn_width = ltpConfig->get_BGo1_pfn_width();
  ERS_LOG(sout << "BGo1_pfn_width = " << a_BGo1_pfn_width);
  std::string a_BGo2_local = ltpConfig->get_BGo2_local();
  ERS_LOG(sout << "BGo2_local = " << a_BGo2_local);
  std::string a_BGo2_lemoout_from = ltpConfig->get_BGo2_lemoout_from();
  ERS_LOG(sout << "BGo2_lemoout_from = " << a_BGo2_lemoout_from);
  std::string a_BGo2_linkout_from = ltpConfig->get_BGo2_linkout_from();
  ERS_LOG(sout << "BGo2_linkout_from = " << a_BGo2_linkout_from);
  std::string a_BGo2_gating = ltpConfig->get_BGo2_gating();
  ERS_LOG(sout << "BGo2_gating = " << a_BGo2_gating);
  std::string a_BGo2_pfn_width = ltpConfig->get_BGo2_pfn_width();
  ERS_LOG(sout << "BGo2_pfn_width = " << a_BGo2_pfn_width);
  std::string a_BGo3_local = ltpConfig->get_BGo3_local();
  ERS_LOG(sout << "BGo3_local = " << a_BGo3_local);
  std::string a_BGo3_lemoout_from = ltpConfig->get_BGo3_lemoout_from();
  ERS_LOG(sout << "BGo3_lemoout_from = " << a_BGo3_lemoout_from);
  std::string a_BGo3_linkout_from = ltpConfig->get_BGo3_linkout_from();
  ERS_LOG(sout << "BGo3_linkout_from = " << a_BGo3_linkout_from);
  std::string a_BGo3_gating = ltpConfig->get_BGo3_gating();
  ERS_LOG(sout << "BGo3_gating = " << a_BGo3_gating);
  std::string a_BGo3_pfn_width = ltpConfig->get_BGo3_pfn_width();
  ERS_LOG(sout << "BGo3_pfn_width = " << a_BGo3_pfn_width);

  if (a_L1A_local == RCDLTPdal::LTPExpertConfig::L1A_local::None) status |= m_ltp->IO_local_no_source(LTP::L1A);
  else if (a_L1A_local == RCDLTPdal::LTPExpertConfig::L1A_local::Lemo) status |= m_ltp->IO_local_lemo_direct(LTP::L1A);
  else if (a_L1A_local == RCDLTPdal::LTPExpertConfig::L1A_local::Lemo_via_pfn) status |= m_ltp->IO_local_lemo_pfn(LTP::L1A);
  else if (a_L1A_local == RCDLTPdal::LTPExpertConfig::L1A_local::Patgen) status |= m_ltp->IO_local_pg(LTP::L1A);

  if (a_L1A_lemoout_from == RCDLTPdal::LTPExpertConfig::L1A_lemoout_from::Local) status |= m_ltp->IO_lemoout_from_local(LTP::L1A);
  else if (a_L1A_lemoout_from == RCDLTPdal::LTPExpertConfig::L1A_lemoout_from::Linkin) status |= m_ltp->IO_lemoout_from_linkin(LTP::L1A);

  if (a_L1A_linkout_from == RCDLTPdal::LTPExpertConfig::L1A_linkout_from::Local) status |= m_ltp->IO_linkout_from_local(LTP::L1A);
  else if (a_L1A_linkout_from == RCDLTPdal::LTPExpertConfig::L1A_linkout_from::Linkin) status |= m_ltp->IO_linkout_from_linkin(LTP::L1A);

  if (a_L1A_gating == RCDLTPdal::LTPExpertConfig::L1A_gating::None) {
    status |= m_ltp->IO_disable_busy_gating(LTP::L1A);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::L1A);
    status |= m_ltp->IO_disable_pg_gating(LTP::L1A);
  }
  else if (a_L1A_gating == RCDLTPdal::LTPExpertConfig::L1A_gating::Busy) {
    status |= m_ltp->IO_enable_busy_gating(LTP::L1A);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::L1A);
    status |= m_ltp->IO_disable_pg_gating(LTP::L1A);
  }
  else if (a_L1A_gating == RCDLTPdal::LTPExpertConfig::L1A_gating::Inhibit) {
    status |= m_ltp->IO_disable_busy_gating(LTP::L1A);
    status |= m_ltp->IO_enable_inhibit_gating(LTP::L1A);
    status |= m_ltp->IO_disable_pg_gating(LTP::L1A);
  }
  else if (a_L1A_gating == RCDLTPdal::LTPExpertConfig::L1A_gating::Patgen) {
    status |= m_ltp->IO_disable_busy_gating(LTP::L1A);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::L1A);
    status |= m_ltp->IO_enable_pg_gating(LTP::L1A);
  }

  if (a_L1A_pfn_width == RCDLTPdal::LTPExpertConfig::L1A_pfn_width::_1BC) status |= m_ltp->IO_pfn_width_1bc(LTP::L1A);
  else if (a_L1A_pfn_width == RCDLTPdal::LTPExpertConfig::L1A_pfn_width::_2BC) status |= m_ltp->IO_pfn_width_2bc(LTP::L1A);

  // -- Test Trigger 1
  if (a_TestTrigger1_local == RCDLTPdal::LTPExpertConfig::TestTrigger1_local::None) status |= m_ltp->IO_local_no_source(LTP::TR1);
  else if (a_TestTrigger1_local == RCDLTPdal::LTPExpertConfig::TestTrigger1_local::Lemo) status |= m_ltp->IO_local_lemo_direct(LTP::TR1);
  else if (a_TestTrigger1_local == RCDLTPdal::LTPExpertConfig::TestTrigger1_local::Lemo_via_pfn) status |= m_ltp->IO_local_lemo_pfn(LTP::TR1);
  else if (a_TestTrigger1_local == RCDLTPdal::LTPExpertConfig::TestTrigger1_local::Patgen) status |= m_ltp->IO_local_pg(LTP::TR1);

  if (a_TestTrigger1_lemoout_from == RCDLTPdal::LTPExpertConfig::TestTrigger1_lemoout_from::Local) status |= m_ltp->IO_lemoout_from_local(LTP::TR1);
  else if (a_TestTrigger1_lemoout_from == RCDLTPdal::LTPExpertConfig::TestTrigger1_lemoout_from::Linkin) status |= m_ltp->IO_lemoout_from_linkin(LTP::TR1);

  if (a_TestTrigger1_linkout_from == RCDLTPdal::LTPExpertConfig::TestTrigger1_linkout_from::Local) status |= m_ltp->IO_linkout_from_local(LTP::TR1);
  else if (a_TestTrigger1_linkout_from == RCDLTPdal::LTPExpertConfig::TestTrigger1_linkout_from::Linkin) status |= m_ltp->IO_linkout_from_linkin(LTP::TR1);

  if (a_TestTrigger1_gating == RCDLTPdal::LTPExpertConfig::TestTrigger1_gating::None) {
    status |= m_ltp->IO_disable_busy_gating(LTP::TR1);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::TR1);
    status |= m_ltp->IO_disable_pg_gating(LTP::TR1);
  }
  else if (a_TestTrigger1_gating == RCDLTPdal::LTPExpertConfig::TestTrigger1_gating::Busy) {
    status |= m_ltp->IO_enable_busy_gating(LTP::TR1);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::TR1);
    status |= m_ltp->IO_disable_pg_gating(LTP::TR1);
  }
  else if (a_TestTrigger1_gating == RCDLTPdal::LTPExpertConfig::TestTrigger1_gating::Inhibit) {
    status |= m_ltp->IO_disable_busy_gating(LTP::TR1);
    status |= m_ltp->IO_enable_inhibit_gating(LTP::TR1);
    status |= m_ltp->IO_disable_pg_gating(LTP::TR1);
  }
  else if (a_TestTrigger1_gating == RCDLTPdal::LTPExpertConfig::TestTrigger1_gating::Patgen) {
    status |= m_ltp->IO_disable_busy_gating(LTP::TR1);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::TR1);
    status |= m_ltp->IO_enable_pg_gating(LTP::TR1);
  }

  if (a_TestTrigger1_pfn_width == RCDLTPdal::LTPExpertConfig::TestTrigger1_pfn_width::_1BC) status |= m_ltp->IO_pfn_width_1bc(LTP::TR1);
  else if (a_TestTrigger1_pfn_width == RCDLTPdal::LTPExpertConfig::TestTrigger1_pfn_width::_2BC) status |= m_ltp->IO_pfn_width_2bc(LTP::TR1);

  // -- Test Trigger 2
  if (a_TestTrigger2_local == RCDLTPdal::LTPExpertConfig::TestTrigger2_local::None) status |= m_ltp->IO_local_no_source(LTP::TR2);
  else if (a_TestTrigger2_local == RCDLTPdal::LTPExpertConfig::TestTrigger2_local::Lemo) status |= m_ltp->IO_local_lemo_direct(LTP::TR2);
  else if (a_TestTrigger2_local == RCDLTPdal::LTPExpertConfig::TestTrigger2_local::Lemo_via_pfn) status |= m_ltp->IO_local_lemo_pfn(LTP::TR2);
  else if (a_TestTrigger2_local == RCDLTPdal::LTPExpertConfig::TestTrigger2_local::Patgen) status |= m_ltp->IO_local_pg(LTP::TR2);

  if (a_TestTrigger2_lemoout_from == RCDLTPdal::LTPExpertConfig::TestTrigger2_lemoout_from::Local) status |= m_ltp->IO_lemoout_from_local(LTP::TR2);
  else if (a_TestTrigger2_lemoout_from == RCDLTPdal::LTPExpertConfig::TestTrigger2_lemoout_from::Linkin) status |= m_ltp->IO_lemoout_from_linkin(LTP::TR2);

  if (a_TestTrigger2_linkout_from == RCDLTPdal::LTPExpertConfig::TestTrigger2_linkout_from::Local) status |= m_ltp->IO_linkout_from_local(LTP::TR2);
  else if (a_TestTrigger2_linkout_from == RCDLTPdal::LTPExpertConfig::TestTrigger2_linkout_from::Linkin) status |= m_ltp->IO_linkout_from_linkin(LTP::TR2);

  if (a_TestTrigger2_gating == RCDLTPdal::LTPExpertConfig::TestTrigger2_gating::None) {
    status |= m_ltp->IO_disable_busy_gating(LTP::TR2);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::TR2);
    status |= m_ltp->IO_disable_pg_gating(LTP::TR2);
  }
  else if (a_TestTrigger2_gating == RCDLTPdal::LTPExpertConfig::TestTrigger2_gating::Busy) {
    status |= m_ltp->IO_enable_busy_gating(LTP::TR2);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::TR2);
    status |= m_ltp->IO_disable_pg_gating(LTP::TR2);
  }
  else if (a_TestTrigger2_gating == RCDLTPdal::LTPExpertConfig::TestTrigger2_gating::Inhibit) {
    status |= m_ltp->IO_disable_busy_gating(LTP::TR2);
    status |= m_ltp->IO_enable_inhibit_gating(LTP::TR2);
    status |= m_ltp->IO_disable_pg_gating(LTP::TR2);
  }
  else if (a_TestTrigger2_gating == RCDLTPdal::LTPExpertConfig::TestTrigger2_gating::Patgen) {
    status |= m_ltp->IO_disable_busy_gating(LTP::TR2);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::TR2);
    status |= m_ltp->IO_enable_pg_gating(LTP::TR2);
  }

  if (a_TestTrigger2_pfn_width == RCDLTPdal::LTPExpertConfig::TestTrigger2_pfn_width::_1BC) status |= m_ltp->IO_pfn_width_1bc(LTP::TR2);
  else if (a_TestTrigger2_pfn_width == RCDLTPdal::LTPExpertConfig::TestTrigger2_pfn_width::_2BC) status |= m_ltp->IO_pfn_width_2bc(LTP::TR2);

  // -- Test Trigger 3
  if (a_TestTrigger3_local == RCDLTPdal::LTPExpertConfig::TestTrigger3_local::None) status |= m_ltp->IO_local_no_source(LTP::TR3);
  else if (a_TestTrigger3_local == RCDLTPdal::LTPExpertConfig::TestTrigger3_local::Lemo) status |= m_ltp->IO_local_lemo_direct(LTP::TR3);
  else if (a_TestTrigger3_local == RCDLTPdal::LTPExpertConfig::TestTrigger3_local::Lemo_via_pfn) status |= m_ltp->IO_local_lemo_pfn(LTP::TR3);
  else if (a_TestTrigger3_local == RCDLTPdal::LTPExpertConfig::TestTrigger3_local::Patgen) status |= m_ltp->IO_local_pg(LTP::TR3);

  if (a_TestTrigger3_lemoout_from == RCDLTPdal::LTPExpertConfig::TestTrigger3_lemoout_from::Local) status |= m_ltp->IO_lemoout_from_local(LTP::TR3);
  else if (a_TestTrigger3_lemoout_from == RCDLTPdal::LTPExpertConfig::TestTrigger3_lemoout_from::Linkin) status |= m_ltp->IO_lemoout_from_linkin(LTP::TR3);

  if (a_TestTrigger3_linkout_from == RCDLTPdal::LTPExpertConfig::TestTrigger3_linkout_from::Local) status |= m_ltp->IO_linkout_from_local(LTP::TR3);
  else if (a_TestTrigger3_linkout_from == RCDLTPdal::LTPExpertConfig::TestTrigger3_linkout_from::Linkin) status |= m_ltp->IO_linkout_from_linkin(LTP::TR3);

  if (a_TestTrigger3_gating == RCDLTPdal::LTPExpertConfig::TestTrigger3_gating::None) {
    status |= m_ltp->IO_disable_busy_gating(LTP::TR3);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::TR3);
    status |= m_ltp->IO_disable_pg_gating(LTP::TR3);
  }
  else if (a_TestTrigger3_gating == RCDLTPdal::LTPExpertConfig::TestTrigger3_gating::Busy) {
    status |= m_ltp->IO_enable_busy_gating(LTP::TR3);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::TR3);
    status |= m_ltp->IO_disable_pg_gating(LTP::TR3);
  }
  else if (a_TestTrigger3_gating == RCDLTPdal::LTPExpertConfig::TestTrigger3_gating::Inhibit) {
    status |= m_ltp->IO_disable_busy_gating(LTP::TR3);
    status |= m_ltp->IO_enable_inhibit_gating(LTP::TR3);
    status |= m_ltp->IO_disable_pg_gating(LTP::TR3);
  }
  else if (a_TestTrigger3_gating == RCDLTPdal::LTPExpertConfig::TestTrigger3_gating::Patgen) {
    status |= m_ltp->IO_disable_busy_gating(LTP::TR3);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::TR3);
    status |= m_ltp->IO_enable_pg_gating(LTP::TR3);
  }

  if (a_TestTrigger3_pfn_width == RCDLTPdal::LTPExpertConfig::TestTrigger3_pfn_width::_1BC) status |= m_ltp->IO_pfn_width_1bc(LTP::TR3);
  else if (a_TestTrigger3_pfn_width == RCDLTPdal::LTPExpertConfig::TestTrigger3_pfn_width::_2BC) status |= m_ltp->IO_pfn_width_2bc(LTP::TR3);


  if (a_BGo0_local == RCDLTPdal::LTPExpertConfig::BGo0_local::None) status |= m_ltp->IO_local_no_source(LTP::BG0);
  else if (a_BGo0_local == RCDLTPdal::LTPExpertConfig::BGo0_local::Lemo) status |= m_ltp->IO_local_lemo_direct(LTP::BG0);
  else if (a_BGo0_local == RCDLTPdal::LTPExpertConfig::BGo0_local::Lemo_via_pfn) status |= m_ltp->IO_local_lemo_pfn(LTP::BG0);
  else if (a_BGo0_local == RCDLTPdal::LTPExpertConfig::BGo0_local::Patgen) status |= m_ltp->IO_local_pg(LTP::BG0);

  if (a_BGo0_lemoout_from == RCDLTPdal::LTPExpertConfig::BGo0_lemoout_from::Local) status |= m_ltp->IO_lemoout_from_local(LTP::BG0);
  else if (a_BGo0_lemoout_from == RCDLTPdal::LTPExpertConfig::BGo0_lemoout_from::Linkin) status |= m_ltp->IO_lemoout_from_linkin(LTP::BG0);

  if (a_BGo0_linkout_from == RCDLTPdal::LTPExpertConfig::BGo0_linkout_from::Local) status |= m_ltp->IO_linkout_from_local(LTP::BG0);
  else if (a_BGo0_linkout_from == RCDLTPdal::LTPExpertConfig::BGo0_linkout_from::Linkin) status |= m_ltp->IO_linkout_from_linkin(LTP::BG0);

  if (a_BGo0_gating == RCDLTPdal::LTPExpertConfig::BGo0_gating::None) {
    status |= m_ltp->IO_disable_busy_gating(LTP::BG0);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::BG0);
    status |= m_ltp->IO_disable_pg_gating(LTP::BG0);
  }
  else if (a_BGo0_gating == RCDLTPdal::LTPExpertConfig::BGo0_gating::Busy) {
    status |= m_ltp->IO_enable_busy_gating(LTP::BG0);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::BG0);
    status |= m_ltp->IO_disable_pg_gating(LTP::BG0);
  }
  else if (a_BGo0_gating == RCDLTPdal::LTPExpertConfig::BGo0_gating::Inhibit) {
    status |= m_ltp->IO_disable_busy_gating(LTP::BG0);
    status |= m_ltp->IO_enable_inhibit_gating(LTP::BG0);
    status |= m_ltp->IO_disable_pg_gating(LTP::BG0);
  }
  else if (a_BGo0_gating == RCDLTPdal::LTPExpertConfig::BGo0_gating::Patgen) {
    status |= m_ltp->IO_disable_busy_gating(LTP::BG0);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::BG0);
    status |= m_ltp->IO_enable_pg_gating(LTP::BG0);
  }

  if (a_BGo0_pfn_width == RCDLTPdal::LTPExpertConfig::BGo0_pfn_width::_1BC) status |= m_ltp->IO_pfn_width_1bc(LTP::BG0);
  else if (a_BGo0_pfn_width == RCDLTPdal::LTPExpertConfig::BGo0_pfn_width::_2BC) status |= m_ltp->IO_pfn_width_2bc(LTP::BG0);

  if (a_BGo1_local == RCDLTPdal::LTPExpertConfig::BGo1_local::None) status |= m_ltp->IO_local_no_source(LTP::BG1);
  else if (a_BGo1_local == RCDLTPdal::LTPExpertConfig::BGo1_local::Lemo) status |= m_ltp->IO_local_lemo_direct(LTP::BG1);
  else if (a_BGo1_local == RCDLTPdal::LTPExpertConfig::BGo1_local::Lemo_via_pfn) status |= m_ltp->IO_local_lemo_pfn(LTP::BG1);
  else if (a_BGo1_local == RCDLTPdal::LTPExpertConfig::BGo1_local::Patgen) status |= m_ltp->IO_local_pg(LTP::BG1);
  
  if (a_BGo1_lemoout_from == RCDLTPdal::LTPExpertConfig::BGo1_lemoout_from::Local) status |= m_ltp->IO_lemoout_from_local(LTP::BG1);
  else if (a_BGo1_lemoout_from == RCDLTPdal::LTPExpertConfig::BGo1_lemoout_from::Linkin) status |= m_ltp->IO_lemoout_from_linkin(LTP::BG1);
      
  if (a_BGo1_linkout_from == RCDLTPdal::LTPExpertConfig::BGo1_linkout_from::Local) status |= m_ltp->IO_linkout_from_local(LTP::BG1);
  else if (a_BGo1_linkout_from == RCDLTPdal::LTPExpertConfig::BGo1_linkout_from::Linkin) status |= m_ltp->IO_linkout_from_linkin(LTP::BG1);

  if (a_BGo1_gating == RCDLTPdal::LTPExpertConfig::BGo1_gating::None) {
    status |= m_ltp->IO_disable_busy_gating(LTP::BG1);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::BG1);
    status |= m_ltp->IO_disable_pg_gating(LTP::BG1);
  }
  else if (a_BGo1_gating == RCDLTPdal::LTPExpertConfig::BGo1_gating::Busy) {
    status |= m_ltp->IO_enable_busy_gating(LTP::BG1);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::BG1);
    status |= m_ltp->IO_disable_pg_gating(LTP::BG1);
  }
  else if (a_BGo1_gating == RCDLTPdal::LTPExpertConfig::BGo1_gating::Inhibit) {
    status |= m_ltp->IO_disable_busy_gating(LTP::BG1);
    status |= m_ltp->IO_enable_inhibit_gating(LTP::BG1);
    status |= m_ltp->IO_disable_pg_gating(LTP::BG1);
  }
  else if (a_BGo1_gating == RCDLTPdal::LTPExpertConfig::BGo1_gating::Patgen) {
    status |= m_ltp->IO_disable_busy_gating(LTP::BG1);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::BG1);
    status |= m_ltp->IO_enable_pg_gating(LTP::BG1);
  }

  if (a_BGo1_pfn_width == RCDLTPdal::LTPExpertConfig::BGo1_pfn_width::_1BC) status |= m_ltp->IO_pfn_width_1bc(LTP::BG1);
  else if (a_BGo1_pfn_width == RCDLTPdal::LTPExpertConfig::BGo1_pfn_width::_2BC) status |= m_ltp->IO_pfn_width_2bc(LTP::BG1);

  if (a_BGo2_local == RCDLTPdal::LTPExpertConfig::BGo2_local::None) status |= m_ltp->IO_local_no_source(LTP::BG2);
  else if (a_BGo2_local == RCDLTPdal::LTPExpertConfig::BGo2_local::Lemo) status |= m_ltp->IO_local_lemo_direct(LTP::BG2);
  else if (a_BGo2_local == RCDLTPdal::LTPExpertConfig::BGo2_local::Lemo_via_pfn) status |= m_ltp->IO_local_lemo_pfn(LTP::BG2);
  else if (a_BGo2_local == RCDLTPdal::LTPExpertConfig::BGo2_local::Patgen) status |= m_ltp->IO_local_pg(LTP::BG2);

  if (a_BGo2_lemoout_from == RCDLTPdal::LTPExpertConfig::BGo2_lemoout_from::Local) status |= m_ltp->IO_lemoout_from_local(LTP::BG2);
  else if (a_BGo2_lemoout_from == RCDLTPdal::LTPExpertConfig::BGo2_lemoout_from::Linkin) status |= m_ltp->IO_lemoout_from_linkin(LTP::BG2);

  if (a_BGo2_linkout_from == RCDLTPdal::LTPExpertConfig::BGo2_linkout_from::Local) status |= m_ltp->IO_linkout_from_local(LTP::BG2);
  else if (a_BGo2_linkout_from == RCDLTPdal::LTPExpertConfig::BGo2_linkout_from::Linkin) status |= m_ltp->IO_linkout_from_linkin(LTP::BG2);

  if (a_BGo2_gating == RCDLTPdal::LTPExpertConfig::BGo2_gating::None) {
    status |= m_ltp->IO_disable_busy_gating(LTP::BG2);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::BG2);
    status |= m_ltp->IO_disable_pg_gating(LTP::BG2);
  }
  else if (a_BGo2_gating == RCDLTPdal::LTPExpertConfig::BGo2_gating::Busy) {
    status |= m_ltp->IO_enable_busy_gating(LTP::BG2);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::BG2);
    status |= m_ltp->IO_disable_pg_gating(LTP::BG2);
  }
  else if (a_BGo2_gating == RCDLTPdal::LTPExpertConfig::BGo2_gating::Inhibit) {
    status |= m_ltp->IO_disable_busy_gating(LTP::BG2);
    status |= m_ltp->IO_enable_inhibit_gating(LTP::BG2);
    status |= m_ltp->IO_disable_pg_gating(LTP::BG2);
  }
  else if (a_BGo2_gating == RCDLTPdal::LTPExpertConfig::BGo2_gating::Patgen) {
    status |= m_ltp->IO_disable_busy_gating(LTP::BG2);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::BG2);
    status |= m_ltp->IO_enable_pg_gating(LTP::BG2);
  }

  if (a_BGo2_pfn_width == RCDLTPdal::LTPExpertConfig::BGo2_pfn_width::_1BC) status |= m_ltp->IO_pfn_width_1bc(LTP::BG2);
  else if (a_BGo2_pfn_width == RCDLTPdal::LTPExpertConfig::BGo2_pfn_width::_2BC) status |= m_ltp->IO_pfn_width_2bc(LTP::BG2);

  if (a_BGo3_local == RCDLTPdal::LTPExpertConfig::BGo3_local::None) status |= m_ltp->IO_local_no_source(LTP::BG3);
  else if (a_BGo3_local == RCDLTPdal::LTPExpertConfig::BGo3_local::Lemo) status |= m_ltp->IO_local_lemo_direct(LTP::BG3);
  else if (a_BGo3_local == RCDLTPdal::LTPExpertConfig::BGo3_local::Lemo_via_pfn) status |= m_ltp->IO_local_lemo_pfn(LTP::BG3);
  else if (a_BGo3_local == RCDLTPdal::LTPExpertConfig::BGo3_local::Patgen) status |= m_ltp->IO_local_pg(LTP::BG3);

  if (a_BGo3_lemoout_from == RCDLTPdal::LTPExpertConfig::BGo3_lemoout_from::Local) status |= m_ltp->IO_lemoout_from_local(LTP::BG3);
  else if (a_BGo3_lemoout_from == RCDLTPdal::LTPExpertConfig::BGo3_lemoout_from::Linkin) status |= m_ltp->IO_lemoout_from_linkin(LTP::BG3);

  if (a_BGo3_linkout_from == RCDLTPdal::LTPExpertConfig::BGo3_linkout_from::Local) status |= m_ltp->IO_linkout_from_local(LTP::BG3);
  else if (a_BGo3_linkout_from == RCDLTPdal::LTPExpertConfig::BGo3_linkout_from::Linkin) status |= m_ltp->IO_linkout_from_linkin(LTP::BG3);

  if (a_BGo3_gating == RCDLTPdal::LTPExpertConfig::BGo3_gating::None) {
    status |= m_ltp->IO_disable_busy_gating(LTP::BG3);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::BG3);
    status |= m_ltp->IO_disable_pg_gating(LTP::BG3);
  }
  else if (a_BGo3_gating == RCDLTPdal::LTPExpertConfig::BGo3_gating::Busy) {
    status |= m_ltp->IO_enable_busy_gating(LTP::BG3);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::BG3);
    status |= m_ltp->IO_disable_pg_gating(LTP::BG3);
  }
  else if (a_BGo3_gating == RCDLTPdal::LTPExpertConfig::BGo3_gating::Inhibit) {
    status |= m_ltp->IO_disable_busy_gating(LTP::BG3);
    status |= m_ltp->IO_enable_inhibit_gating(LTP::BG3);
    status |= m_ltp->IO_disable_pg_gating(LTP::BG3);
  }
  else if (a_BGo3_gating == RCDLTPdal::LTPExpertConfig::BGo3_gating::Patgen) {
    status |= m_ltp->IO_disable_busy_gating(LTP::BG3);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::BG3);
    status |= m_ltp->IO_enable_pg_gating(LTP::BG3);
  }

  if (a_BGo3_pfn_width == RCDLTPdal::LTPExpertConfig::BGo3_pfn_width::_1BC) status |= m_ltp->IO_pfn_width_1bc(LTP::BG3);
  else if (a_BGo3_pfn_width == RCDLTPdal::LTPExpertConfig::BGo3_pfn_width::_2BC) status |= m_ltp->IO_pfn_width_2bc(LTP::BG3);


  // busy + calreq + triggertype
  std::string a_Busy_deadtime_source = ltpConfig->get_Busy_deadtime_source();
  ERS_LOG(sout << "Busy_deadtime_source = " << a_Busy_deadtime_source);
  bool a_Busy_local_with_deadtime = ltpConfig->get_Busy_local_with_deadtime();
  ERS_LOG(sout << "Busy_local_with_deadtime = " << a_Busy_local_with_deadtime);
  std::string a_Busy_lemoout = ltpConfig->get_Busy_lemoout();
  ERS_LOG(sout << "Busy_lemoout = " << a_Busy_lemoout);
  std::string a_Busy_linkout_from = ltpConfig->get_Busy_linkout_from();
  ERS_LOG(sout << "Busy_linkout_from = " << a_Busy_linkout_from);
  bool a_Busy_from_patgen = ltpConfig->get_Busy_from_patgen();
  ERS_LOG(sout << "Busy_from_patgen = " << a_Busy_from_patgen);
  std::string a_Busy_deadtime_duration = ltpConfig->get_Busy_deadtime_duration();
  ERS_LOG(sout << "Busy_deadtime_duration = " << a_Busy_deadtime_duration);
  std::string a_CalReq_source = ltpConfig->get_CalReq_source();
  ERS_LOG(sout << "CalReq_source = " << a_CalReq_source);
  bool a_CalReq_enable_testpath = ltpConfig->get_CalReq_enable_testpath();
  ERS_LOG(sout << "CalReq_enable_testpath = " << a_CalReq_enable_testpath);
  std::string a_CalReq_preset_value = ltpConfig->get_CalReq_preset_value();
  ERS_LOG(sout << "CalReq_preset_value = " << a_CalReq_preset_value);
  std::string a_TriggerType_fifotrigger = ltpConfig->get_TriggerType_fifotrigger();
  ERS_LOG(sout << "TriggerType_fifotrigger = " << a_TriggerType_fifotrigger);
  std::string a_TriggerType_fifodelay = ltpConfig->get_TriggerType_fifodelay();
  ERS_LOG(sout << "TriggerType_fifodelay = " << a_TriggerType_fifodelay);
  std::string a_TriggerType_regfile_address = ltpConfig->get_TriggerType_regfile_address();
  ERS_LOG(sout << "TriggerType_regfile_address = " << a_TriggerType_regfile_address);
  bool a_TriggerType_eclout_enabled = ltpConfig->get_TriggerType_eclout_enabled();
  ERS_LOG(sout << "TriggerType_eclout_enabled = " << a_TriggerType_eclout_enabled);
  std::string a_TriggerType_local = ltpConfig->get_TriggerType_local();
  ERS_LOG(sout << "TriggerType_local = " << a_TriggerType_local);
  std::string a_TriggerType_eclout_from = ltpConfig->get_TriggerType_eclout_from();
  ERS_LOG(sout << "TriggerType_eclout_from = " << a_TriggerType_eclout_from);
  std::string a_TriggerType_linkout_from = ltpConfig->get_TriggerType_linkout_from();
  ERS_LOG(sout << "TriggerType_linkout_from = " << a_TriggerType_linkout_from);
  unsigned int a_TriggerType_regfile0_data = ltpConfig->get_TriggerType_regfile0_data();
  ERS_LOG(sout << "TriggerType_regfile0_data = " << a_TriggerType_regfile0_data);
  unsigned int a_TriggerType_regfile1_data = ltpConfig->get_TriggerType_regfile1_data();
  ERS_LOG(sout << "TriggerType_regfile1_data = " << a_TriggerType_regfile1_data);
  unsigned int a_TriggerType_regfile2_data = ltpConfig->get_TriggerType_regfile2_data();
  ERS_LOG(sout << "TriggerType_regfile2_data = " << a_TriggerType_regfile2_data);
  unsigned int a_TriggerType_regfile3_data = ltpConfig->get_TriggerType_regfile3_data();
  ERS_LOG(sout << "TriggerType_regfile3_data = " << a_TriggerType_regfile3_data);
  std::string a_CalReq_turn_orbit_lemoout = ltpConfig->get_CalReq_turn_orbit_lemoout();
  ERS_LOG(sout << "CalReq_turn_orbit_lemoout = " << a_CalReq_turn_orbit_lemoout);
  unsigned int a_CalReq_turn_selector_code = ltpConfig->get_CalReq_turn_selector_code();
  ERS_LOG(sout << "CalReq_turn_selector_code = " << a_CalReq_turn_selector_code);
  unsigned int a_Inhibit_delay = ltpConfig->get_Inhibit_delay();
  ERS_LOG(sout << "Inhibit_delay = " << a_Inhibit_delay);

  // Busy
  if (a_Busy_deadtime_source == RCDLTPdal::LTPExpertConfig::Busy_deadtime_source::L1A) status |= m_ltp->BUSY_deadtime_source_L1A();
  else if (a_Busy_deadtime_source == RCDLTPdal::LTPExpertConfig::Busy_deadtime_source::TestTrigger1) status |= m_ltp->BUSY_deadtime_source_TR1();
  else if (a_Busy_deadtime_source == RCDLTPdal::LTPExpertConfig::Busy_deadtime_source::TestTrigger2) status |= m_ltp->BUSY_deadtime_source_TR2();
  else if (a_Busy_deadtime_source == RCDLTPdal::LTPExpertConfig::Busy_deadtime_source::TestTrigger3) status |= m_ltp->BUSY_deadtime_source_TR3();
  
  if (a_Busy_lemoout == RCDLTPdal::LTPExpertConfig::Busy_lemoout::Busy_only) status |= m_ltp->BUSY_lemoout_busy_only();
  else if (a_Busy_lemoout == RCDLTPdal::LTPExpertConfig::Busy_lemoout::Busy_or_deadtime) status |= m_ltp->BUSY_lemoout_busy_or_deadtime();
  else if (a_Busy_lemoout == RCDLTPdal::LTPExpertConfig::Busy_lemoout::Timer_inhibit) status |= m_ltp->BUSY_lemoout_inhibit();
  else if (a_Busy_lemoout == RCDLTPdal::LTPExpertConfig::Busy_lemoout::Patgen_inhibit) status |= m_ltp->BUSY_lemoout_pg_inhibit();
  
  if (a_Busy_linkout_from == RCDLTPdal::LTPExpertConfig::Busy_linkout_from::Local) status |= m_ltp->BUSY_linkout_from_local();
  else if (a_Busy_linkout_from == RCDLTPdal::LTPExpertConfig::Busy_linkout_from::Linkin) status |= m_ltp->BUSY_linkout_from_linkin();
  
  m_Busy_current[0] = m_Busy_local_with_linkin;
  m_Busy_current[1] = m_Busy_local_with_ttlin;
  m_Busy_current[2] = m_Busy_local_with_nimin;
  this->updateBusy();
    
  if (a_Busy_local_with_deadtime) status |= m_ltp->BUSY_local_with_deadtime();
  else status |= m_ltp->BUSY_local_without_deadtime();
    
  if (a_Busy_from_patgen) status |= m_ltp->BUSY_enable_from_pg();
  else status |= m_ltp->BUSY_disable_from_pg();

  if      (a_Busy_deadtime_duration == RCDLTPdal::LTPExpertConfig::Busy_deadtime_duration::_4BC) status |= m_ltp->BUSY_deadtime_duration(0);
  else if (a_Busy_deadtime_duration == RCDLTPdal::LTPExpertConfig::Busy_deadtime_duration::_5BC) status |= m_ltp->BUSY_deadtime_duration(1);
  else if (a_Busy_deadtime_duration == RCDLTPdal::LTPExpertConfig::Busy_deadtime_duration::_6BC) status |= m_ltp->BUSY_deadtime_duration(2);
  else if (a_Busy_deadtime_duration == RCDLTPdal::LTPExpertConfig::Busy_deadtime_duration::_7BC) status |= m_ltp->BUSY_deadtime_duration(3);
  else if (a_Busy_deadtime_duration == RCDLTPdal::LTPExpertConfig::Busy_deadtime_duration::_8BC) status |= m_ltp->BUSY_deadtime_duration(4);
  else if (a_Busy_deadtime_duration == RCDLTPdal::LTPExpertConfig::Busy_deadtime_duration::_9BC) status |= m_ltp->BUSY_deadtime_duration(5);
  else if (a_Busy_deadtime_duration == RCDLTPdal::LTPExpertConfig::Busy_deadtime_duration::_10BC) status |= m_ltp->BUSY_deadtime_duration(6);
  else if (a_Busy_deadtime_duration == RCDLTPdal::LTPExpertConfig::Busy_deadtime_duration::_11BC) status |= m_ltp->BUSY_deadtime_duration(7);
    
  // Calibration Request
  if      (a_CalReq_source == RCDLTPdal::LTPExpertConfig::CalReq_source::Linkin) status |= m_ltp->CAL_source_link();
  else if (a_CalReq_source == RCDLTPdal::LTPExpertConfig::CalReq_source::Preset) status |= m_ltp->CAL_source_preset();
  else if (a_CalReq_source == RCDLTPdal::LTPExpertConfig::CalReq_source::FrontPanel) status |= m_ltp->CAL_source_fp();
  else if (a_CalReq_source == RCDLTPdal::LTPExpertConfig::CalReq_source::PatGen) status |= m_ltp->CAL_source_pg();
  
  if (a_CalReq_enable_testpath) status |= m_ltp->CAL_enable_testpath();
  else status |= m_ltp->CAL_disable_testpath();

  if      (a_CalReq_preset_value == RCDLTPdal::LTPExpertConfig::CalReq_preset_value::_0) status |= m_ltp->CAL_set_preset_value(0);
  else if (a_CalReq_preset_value == RCDLTPdal::LTPExpertConfig::CalReq_preset_value::_1) status |= m_ltp->CAL_set_preset_value(1);
  else if (a_CalReq_preset_value == RCDLTPdal::LTPExpertConfig::CalReq_preset_value::_2) status |= m_ltp->CAL_set_preset_value(2);
  else if (a_CalReq_preset_value == RCDLTPdal::LTPExpertConfig::CalReq_preset_value::_3) status |= m_ltp->CAL_set_preset_value(3);
  else if (a_CalReq_preset_value == RCDLTPdal::LTPExpertConfig::CalReq_preset_value::_4) status |= m_ltp->CAL_set_preset_value(4);
  else if (a_CalReq_preset_value == RCDLTPdal::LTPExpertConfig::CalReq_preset_value::_5) status |= m_ltp->CAL_set_preset_value(5);
  else if (a_CalReq_preset_value == RCDLTPdal::LTPExpertConfig::CalReq_preset_value::_6) status |= m_ltp->CAL_set_preset_value(6);
  else if (a_CalReq_preset_value == RCDLTPdal::LTPExpertConfig::CalReq_preset_value::_7) status |= m_ltp->CAL_set_preset_value(7);

  // Trigger Type
  if (a_TriggerType_fifotrigger == RCDLTPdal::LTPExpertConfig::TriggerType_fifotrigger::L1A) status |= m_ltp->TTYPE_fifotrigger_L1A();
  else if (a_TriggerType_fifotrigger == RCDLTPdal::LTPExpertConfig::TriggerType_fifotrigger::TestTrigger1) status |= m_ltp->TTYPE_fifotrigger_TR1();
  else if (a_TriggerType_fifotrigger == RCDLTPdal::LTPExpertConfig::TriggerType_fifotrigger::TestTrigger2) status |= m_ltp->TTYPE_fifotrigger_TR2();
  else if (a_TriggerType_fifotrigger == RCDLTPdal::LTPExpertConfig::TriggerType_fifotrigger::TestTrigger3) status |= m_ltp->TTYPE_fifotrigger_TR3();

  if (a_TriggerType_fifodelay == RCDLTPdal::LTPExpertConfig::TriggerType_fifodelay::_2BC) status |= m_ltp->TTYPE_fifodelay_2bc();
  else if (a_TriggerType_fifodelay == RCDLTPdal::LTPExpertConfig::TriggerType_fifodelay::_3BC) status |= m_ltp->TTYPE_fifodelay_3bc();
  else if (a_TriggerType_fifodelay == RCDLTPdal::LTPExpertConfig::TriggerType_fifodelay::_4BC) status |= m_ltp->TTYPE_fifodelay_4bc();
  else if (a_TriggerType_fifodelay == RCDLTPdal::LTPExpertConfig::TriggerType_fifodelay::_5BC) status |= m_ltp->TTYPE_fifodelay_5bc();

  if (a_TriggerType_regfile_address == RCDLTPdal::LTPExpertConfig::TriggerType_regfile_address::File0) status |= m_ltp->TTYPE_regfile_address_0();
  else if (a_TriggerType_regfile_address == RCDLTPdal::LTPExpertConfig::TriggerType_regfile_address::File1) status |= m_ltp->TTYPE_regfile_address_1();
  else if (a_TriggerType_regfile_address == RCDLTPdal::LTPExpertConfig::TriggerType_regfile_address::File2) status |= m_ltp->TTYPE_regfile_address_2();
  else if (a_TriggerType_regfile_address == RCDLTPdal::LTPExpertConfig::TriggerType_regfile_address::File3) status |= m_ltp->TTYPE_regfile_address_3();
 
  if (a_TriggerType_eclout_enabled) status |= m_ltp->TTYPE_enable_eclout();
  else status |= m_ltp->TTYPE_disable_eclout();
 
  if (a_TriggerType_local == RCDLTPdal::LTPExpertConfig::TriggerType_local::Patgen) status |= m_ltp->TTYPE_local_from_pg();
  else if (a_TriggerType_local == RCDLTPdal::LTPExpertConfig::TriggerType_local::Vme) status |= m_ltp->TTYPE_local_from_vme();

  if (a_TriggerType_eclout_from == RCDLTPdal::LTPExpertConfig::TriggerType_eclout_from::Local) status |= m_ltp->TTYPE_eclout_from_local();
  else if (a_TriggerType_eclout_from == RCDLTPdal::LTPExpertConfig::TriggerType_eclout_from::Linkin) status |= m_ltp->TTYPE_eclout_from_linkin();

  if (a_TriggerType_linkout_from == RCDLTPdal::LTPExpertConfig::TriggerType_linkout_from::Local) status |= m_ltp->TTYPE_linkout_from_local();
  else if (a_TriggerType_linkout_from == RCDLTPdal::LTPExpertConfig::TriggerType_linkout_from::Linkin) status |= m_ltp->TTYPE_linkout_from_linkin();

  status |= m_ltp->TTYPE_write_regfile0(a_TriggerType_regfile0_data);
  status |= m_ltp->TTYPE_write_regfile1(a_TriggerType_regfile1_data);
  status |= m_ltp->TTYPE_write_regfile2(a_TriggerType_regfile2_data);
  status |= m_ltp->TTYPE_write_regfile3(a_TriggerType_regfile3_data);

  // Turn counter
  if (a_CalReq_turn_orbit_lemoout == RCDLTPdal::LTPExpertConfig::CalReq_turn_orbit_lemoout::Selected_orbit_source) status |= m_ltp->TURN_orbit_lemoout_from_orbit();
  else if (a_CalReq_turn_orbit_lemoout == RCDLTPdal::LTPExpertConfig::CalReq_turn_orbit_lemoout::Turn_selector) status |= m_ltp->TURN_orbit_lemoout_from_turn();

  status |= m_ltp->TURN_write_selector_code(a_CalReq_turn_selector_code);

  // Inhibit time value
  status |= m_ltp->INH_writeShadow(a_Inhibit_delay);

  // setup pattern generator
  m_use_patgen = false;

  std::string a_PatGen_runmode = ltpConfig->get_PatGen_runmode();
  m_PatGen_runmode = a_PatGen_runmode;
  ERS_LOG(sout << "PatGen_runmode = \"" << a_PatGen_runmode << "\"");
  std::string a_PatGen_filename = ltpConfig->get_PatGen_filename();
  ERS_LOG(sout << "PatGen_filename = \"" << a_PatGen_filename << "\"");
  
  u_int a_PatGen_fixed_value = ltpConfig->get_PatGen_fixed_value();
  ERS_LOG(sout << "PatGen_fixed_value = 0x" << std::hex << a_PatGen_fixed_value << std::dec);

  if (a_PatGen_runmode == RCDLTPdal::LTPExpertConfig::PatGen_runmode::Off) {
    status |= m_ltp->PG_runmode_notused();
    m_use_patgen = false;
  }
  else {
    ERS_LOG(sout << "Pattern generator mode is not OFF, therefore it is being used");
    m_use_patgen = true;
    ERS_LOG(sout << "m_use_patgen = " << m_use_patgen);

    // load patterns
    ERS_LOG(sout << "Reset pattern generator");
    status |= m_ltp->PG_reset();

    if (a_PatGen_runmode != RCDLTPdal::LTPExpertConfig::PatGen_runmode::Fixed_Value) {
      // first try if there is an LTPPatternBase linked
      const RCDLTPdal::LTPPatternBase* pattern = ltpConfig->get_PatGen_pattern();
      if (pattern != nullptr) {
	this->loadPattern(pattern);
      } else { // this else clause should become obsolete in the next tdaq version

	ERS_LOG(sout << ">>> OBSOLETE: loading a pattern via the PatGen_pattern attribute is obsolete and will be dropped in the next release! Please use an object of class type LTPPatternBase instead");
	ERS_LOG(sout << "Load pattern from file: \"" << a_PatGen_filename << "\"");
	status |= m_ltp->PG_runmode_load();
    
	int lstatus = m_ltp->PG_load_memory_from_file(&a_PatGen_filename);
	if ( lstatus != 0 ) {
	  ostringstream text;
	  text << sout << "Could not load pattern to pattern generator memory. PatGen_filename = " << a_PatGen_filename;
	  ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
	}
      }
    }

    // copied runmode into member variable, set run mode in connect
    status |= m_ltp->PG_reset();
    status |= m_ltp->PG_set_fixed_value(a_PatGen_fixed_value);
  }

  if (status != 0) {
    ostringstream text;
    text << sout << "VME error, status="<< status << "(0x" << std::hex << status << std::dec << ")";
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
  }

  ERS_LOG(sout << "Done");
}  

// ----------------------------------------------------------------------------------
void  LTPModule::configureExpertBypassed(const RCDLTPdal::LTPExpertBypassedConfig* ltpConfig) {
  std::string sout = m_UID + "@LTPModule::configureExpertBypassed() ";
  ERS_LOG(sout << "Entered");

  // dump configuration object
  ERS_LOG(sout << "called with LTPExpertBypassedConfig = " << ltpConfig);
  ltpConfig->print(1,true,std::cout);

  // configure according to expert being bypassed
  
  int status = 0;

  // BC
  std::string a_BC_local = ltpConfig->get_BC_local();
  ERS_LOG(sout << "BC_local = " << a_BC_local);
  std::string a_BC_lemoout_from = ltpConfig->get_BC_lemoout_from();
  ERS_LOG(sout << "BC_lemoout_from = " << a_BC_lemoout_from);
  if (a_BC_local == RCDLTPdal::LTPExpertBypassedConfig::BC_local::Local_oscillator) status |= m_ltp->BC_local_intern();
  else if (a_BC_local == RCDLTPdal::LTPExpertBypassedConfig::BC_local::Lemo) status |= m_ltp->BC_local_lemo();
  if (a_BC_lemoout_from == RCDLTPdal::LTPExpertBypassedConfig::BC_lemoout_from::Local) status |= m_ltp->BC_lemoout_from_local();
  else if (a_BC_lemoout_from == RCDLTPdal::LTPExpertBypassedConfig::BC_lemoout_from::Linkin) status |= m_ltp->BC_lemoout_from_linkin();

  // Orbit
  std::string a_Orbit_local = ltpConfig->get_Orbit_local();
  ERS_LOG(sout << "Orbit_local = " << a_Orbit_local);
  std::string a_Orbit_lemoout_from = ltpConfig->get_Orbit_lemoout_from();
  ERS_LOG(sout << "Orbit_lemoout_from = " << a_Orbit_lemoout_from);

  if (a_Orbit_local == RCDLTPdal::LTPExpertBypassedConfig::Orbit_local::None) status |= m_ltp->ORB_local_no();
  else if (a_Orbit_local == RCDLTPdal::LTPExpertBypassedConfig::Orbit_local::Internal) status |= m_ltp->ORB_local_intern();
  else if (a_Orbit_local == RCDLTPdal::LTPExpertBypassedConfig::Orbit_local::Lemo) status |= m_ltp->ORB_local_lemo();
  else if (a_Orbit_local == RCDLTPdal::LTPExpertBypassedConfig::Orbit_local::Patgen) status |= m_ltp->ORB_local_pg();

  if (a_Orbit_lemoout_from == RCDLTPdal::LTPExpertBypassedConfig::Orbit_lemoout_from::Local) status |= m_ltp->ORB_lemoout_from_local();
  else if (a_Orbit_lemoout_from == RCDLTPdal::LTPExpertBypassedConfig::Orbit_lemoout_from::Linkin) status |= m_ltp->ORB_lemoout_from_linkin();

  // triggers and bgo

  std::string a_L1A_local = ltpConfig->get_L1A_local();
  ERS_LOG(sout << "L1A_local = " << a_L1A_local);
  std::string a_L1A_lemoout_from = ltpConfig->get_L1A_lemoout_from();
  ERS_LOG(sout << "L1A_lemoout_from = " << a_L1A_lemoout_from);
  std::string a_L1A_gating = ltpConfig->get_L1A_gating();
  ERS_LOG(sout << "L1A_gating = " << a_L1A_gating);
  std::string a_L1A_pfn_width = ltpConfig->get_L1A_pfn_width();
  ERS_LOG(sout << "L1A_pfn_width = " << a_L1A_pfn_width);
  std::string a_TestTrigger1_local = ltpConfig->get_TestTrigger1_local();
  ERS_LOG(sout << "TestTrigger1_local = " << a_TestTrigger1_local);
  std::string a_TestTrigger1_lemoout_from = ltpConfig->get_TestTrigger1_lemoout_from();
  ERS_LOG(sout << "TestTrigger1_lemoout_from = " << a_TestTrigger1_lemoout_from);
  std::string a_TestTrigger1_gating = ltpConfig->get_TestTrigger1_gating();
  ERS_LOG(sout << "TestTrigger1_gating = " << a_TestTrigger1_gating);
  std::string a_TestTrigger1_pfn_width = ltpConfig->get_TestTrigger1_pfn_width();
  ERS_LOG(sout << "TestTrigger1_pfn_width = " << a_TestTrigger1_pfn_width);
  std::string a_TestTrigger2_local = ltpConfig->get_TestTrigger2_local();
  ERS_LOG(sout << "TestTrigger2_local = " << a_TestTrigger2_local);
  std::string a_TestTrigger2_lemoout_from = ltpConfig->get_TestTrigger2_lemoout_from();
  ERS_LOG(sout << "TestTrigger2_lemoout_from = " << a_TestTrigger2_lemoout_from);
  std::string a_TestTrigger2_gating = ltpConfig->get_TestTrigger2_gating();
  ERS_LOG(sout << "TestTrigger2_gating = " << a_TestTrigger2_gating);
  std::string a_TestTrigger2_pfn_width = ltpConfig->get_TestTrigger2_pfn_width();
  ERS_LOG(sout << "TestTrigger2_pfn_width = " << a_TestTrigger2_pfn_width);
  std::string a_TestTrigger3_local = ltpConfig->get_TestTrigger3_local();
  ERS_LOG(sout << "TestTrigger3_local = " << a_TestTrigger3_local);
  std::string a_TestTrigger3_lemoout_from = ltpConfig->get_TestTrigger3_lemoout_from();
  ERS_LOG(sout << "TestTrigger3_lemoout_from = " << a_TestTrigger3_lemoout_from);
  std::string a_TestTrigger3_gating = ltpConfig->get_TestTrigger3_gating();
  ERS_LOG(sout << "TestTrigger3_gating = " << a_TestTrigger3_gating);
  std::string a_TestTrigger3_pfn_width = ltpConfig->get_TestTrigger3_pfn_width();
  ERS_LOG(sout << "TestTrigger3_pfn_width = " << a_TestTrigger3_pfn_width);

  std::string a_BGo0_local = ltpConfig->get_BGo0_local();
  ERS_LOG(sout << "BGo0_local = " << a_BGo0_local);
  std::string a_BGo0_lemoout_from = ltpConfig->get_BGo0_lemoout_from();
  ERS_LOG(sout << "BGo0_lemoout_from = " << a_BGo0_lemoout_from);
  std::string a_BGo0_gating = ltpConfig->get_BGo0_gating();
  ERS_LOG(sout << "BGo0_gating = " << a_BGo0_gating);
  std::string a_BGo0_pfn_width = ltpConfig->get_BGo0_pfn_width();
  ERS_LOG(sout << "BGo0_pfn_width = " << a_BGo0_pfn_width);
  std::string a_BGo1_local = ltpConfig->get_BGo1_local();
  ERS_LOG(sout << "BGo1_local = " << a_BGo1_local);
  std::string a_BGo1_lemoout_from = ltpConfig->get_BGo1_lemoout_from();
  ERS_LOG(sout << "BGo1_lemoout_from = " << a_BGo1_lemoout_from);
  std::string a_BGo1_gating = ltpConfig->get_BGo1_gating();
  ERS_LOG(sout << "BGo1_gating = " << a_BGo1_gating);
  std::string a_BGo1_pfn_width = ltpConfig->get_BGo1_pfn_width();
  ERS_LOG(sout << "BGo1_pfn_width = " << a_BGo1_pfn_width);
  std::string a_BGo2_local = ltpConfig->get_BGo2_local();
  ERS_LOG(sout << "BGo2_local = " << a_BGo2_local);
  std::string a_BGo2_lemoout_from = ltpConfig->get_BGo2_lemoout_from();
  ERS_LOG(sout << "BGo2_lemoout_from = " << a_BGo2_lemoout_from);
  std::string a_BGo2_gating = ltpConfig->get_BGo2_gating();
  ERS_LOG(sout << "BGo2_gating = " << a_BGo2_gating);
  std::string a_BGo2_pfn_width = ltpConfig->get_BGo2_pfn_width();
  ERS_LOG(sout << "BGo2_pfn_width = " << a_BGo2_pfn_width);
  std::string a_BGo3_local = ltpConfig->get_BGo3_local();
  ERS_LOG(sout << "BGo3_local = " << a_BGo3_local);
  std::string a_BGo3_lemoout_from = ltpConfig->get_BGo3_lemoout_from();
  ERS_LOG(sout << "BGo3_lemoout_from = " << a_BGo3_lemoout_from);
  std::string a_BGo3_gating = ltpConfig->get_BGo3_gating();
  ERS_LOG(sout << "BGo3_gating = " << a_BGo3_gating);
  std::string a_BGo3_pfn_width = ltpConfig->get_BGo3_pfn_width();
  ERS_LOG(sout << "BGo3_pfn_width = " << a_BGo3_pfn_width);

  if (a_L1A_local == RCDLTPdal::LTPExpertBypassedConfig::L1A_local::None) status |= m_ltp->IO_local_no_source(LTP::L1A);
  else if (a_L1A_local == RCDLTPdal::LTPExpertBypassedConfig::L1A_local::Lemo) status |= m_ltp->IO_local_lemo_direct(LTP::L1A);
  else if (a_L1A_local == RCDLTPdal::LTPExpertBypassedConfig::L1A_local::Lemo_via_pfn) status |= m_ltp->IO_local_lemo_pfn(LTP::L1A);
  else if (a_L1A_local == RCDLTPdal::LTPExpertBypassedConfig::L1A_local::Patgen) status |= m_ltp->IO_local_pg(LTP::L1A);

  if (a_L1A_lemoout_from == RCDLTPdal::LTPExpertBypassedConfig::L1A_lemoout_from::Local) status |= m_ltp->IO_lemoout_from_local(LTP::L1A);
  else if (a_L1A_lemoout_from == RCDLTPdal::LTPExpertBypassedConfig::L1A_lemoout_from::Linkin) status |= m_ltp->IO_lemoout_from_linkin(LTP::L1A);

  if (a_L1A_gating == RCDLTPdal::LTPExpertBypassedConfig::L1A_gating::None) {
    status |= m_ltp->IO_disable_busy_gating(LTP::L1A);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::L1A);
    status |= m_ltp->IO_disable_pg_gating(LTP::L1A);
  }
  else if (a_L1A_gating == RCDLTPdal::LTPExpertBypassedConfig::L1A_gating::Busy) {
    status |= m_ltp->IO_enable_busy_gating(LTP::L1A);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::L1A);
    status |= m_ltp->IO_disable_pg_gating(LTP::L1A);
  }
  else if (a_L1A_gating == RCDLTPdal::LTPExpertBypassedConfig::L1A_gating::Inhibit) {
    status |= m_ltp->IO_disable_busy_gating(LTP::L1A);
    status |= m_ltp->IO_enable_inhibit_gating(LTP::L1A);
    status |= m_ltp->IO_disable_pg_gating(LTP::L1A);
  }
  else if (a_L1A_gating == RCDLTPdal::LTPExpertBypassedConfig::L1A_gating::Patgen) {
    status |= m_ltp->IO_disable_busy_gating(LTP::L1A);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::L1A);
    status |= m_ltp->IO_enable_pg_gating(LTP::L1A);
  }

  if (a_L1A_pfn_width == RCDLTPdal::LTPExpertBypassedConfig::L1A_pfn_width::_1BC) status |= m_ltp->IO_pfn_width_1bc(LTP::L1A);
  else if (a_L1A_pfn_width == RCDLTPdal::LTPExpertBypassedConfig::L1A_pfn_width::_2BC) status |= m_ltp->IO_pfn_width_2bc(LTP::L1A);

  // -- Test Trigger 1
  if (a_TestTrigger1_local == RCDLTPdal::LTPExpertBypassedConfig::TestTrigger1_local::None) status |= m_ltp->IO_local_no_source(LTP::TR1);
  else if (a_TestTrigger1_local == RCDLTPdal::LTPExpertBypassedConfig::TestTrigger1_local::Lemo) status |= m_ltp->IO_local_lemo_direct(LTP::TR1);
  else if (a_TestTrigger1_local == RCDLTPdal::LTPExpertBypassedConfig::TestTrigger1_local::Lemo_via_pfn) status |= m_ltp->IO_local_lemo_pfn(LTP::TR1);
  else if (a_TestTrigger1_local == RCDLTPdal::LTPExpertBypassedConfig::TestTrigger1_local::Patgen) status |= m_ltp->IO_local_pg(LTP::TR1);

  if (a_TestTrigger1_lemoout_from == RCDLTPdal::LTPExpertBypassedConfig::TestTrigger1_lemoout_from::Local) status |= m_ltp->IO_lemoout_from_local(LTP::TR1);
  else if (a_TestTrigger1_lemoout_from == RCDLTPdal::LTPExpertBypassedConfig::TestTrigger1_lemoout_from::Linkin) status |= m_ltp->IO_lemoout_from_linkin(LTP::TR1);

  if (a_TestTrigger1_gating == RCDLTPdal::LTPExpertBypassedConfig::TestTrigger1_gating::None) {
    status |= m_ltp->IO_disable_busy_gating(LTP::TR1);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::TR1);
    status |= m_ltp->IO_disable_pg_gating(LTP::TR1);
  }
  else if (a_TestTrigger1_gating == RCDLTPdal::LTPExpertBypassedConfig::TestTrigger1_gating::Busy) {
    status |= m_ltp->IO_enable_busy_gating(LTP::TR1);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::TR1);
    status |= m_ltp->IO_disable_pg_gating(LTP::TR1);
  }
  else if (a_TestTrigger1_gating == RCDLTPdal::LTPExpertBypassedConfig::TestTrigger1_gating::Inhibit) {
    status |= m_ltp->IO_disable_busy_gating(LTP::TR1);
    status |= m_ltp->IO_enable_inhibit_gating(LTP::TR1);
    status |= m_ltp->IO_disable_pg_gating(LTP::TR1);
  }
  else if (a_TestTrigger1_gating == RCDLTPdal::LTPExpertBypassedConfig::TestTrigger1_gating::Patgen) {
    status |= m_ltp->IO_disable_busy_gating(LTP::TR1);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::TR1);
    status |= m_ltp->IO_enable_pg_gating(LTP::TR1);
  }

  if (a_TestTrigger1_pfn_width == RCDLTPdal::LTPExpertBypassedConfig::TestTrigger1_pfn_width::_1BC) status |= m_ltp->IO_pfn_width_1bc(LTP::TR1);
  else if (a_TestTrigger1_pfn_width == RCDLTPdal::LTPExpertBypassedConfig::TestTrigger1_pfn_width::_2BC) status |= m_ltp->IO_pfn_width_2bc(LTP::TR1);

  // -- Test Trigger 2
  if (a_TestTrigger2_local == RCDLTPdal::LTPExpertBypassedConfig::TestTrigger2_local::None) status |= m_ltp->IO_local_no_source(LTP::TR2);
  else if (a_TestTrigger2_local == RCDLTPdal::LTPExpertBypassedConfig::TestTrigger2_local::Lemo) status |= m_ltp->IO_local_lemo_direct(LTP::TR2);
  else if (a_TestTrigger2_local == RCDLTPdal::LTPExpertBypassedConfig::TestTrigger2_local::Lemo_via_pfn) status |= m_ltp->IO_local_lemo_pfn(LTP::TR2);
  else if (a_TestTrigger2_local == RCDLTPdal::LTPExpertBypassedConfig::TestTrigger2_local::Patgen) status |= m_ltp->IO_local_pg(LTP::TR2);

  if (a_TestTrigger2_lemoout_from == RCDLTPdal::LTPExpertBypassedConfig::TestTrigger2_lemoout_from::Local) status |= m_ltp->IO_lemoout_from_local(LTP::TR2);
  else if (a_TestTrigger2_lemoout_from == RCDLTPdal::LTPExpertBypassedConfig::TestTrigger2_lemoout_from::Linkin) status |= m_ltp->IO_lemoout_from_linkin(LTP::TR2);

  if (a_TestTrigger2_gating == RCDLTPdal::LTPExpertBypassedConfig::TestTrigger2_gating::None) {
    status |= m_ltp->IO_disable_busy_gating(LTP::TR2);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::TR2);
    status |= m_ltp->IO_disable_pg_gating(LTP::TR2);
  }
  else if (a_TestTrigger2_gating == RCDLTPdal::LTPExpertBypassedConfig::TestTrigger2_gating::Busy) {
    status |= m_ltp->IO_enable_busy_gating(LTP::TR2);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::TR2);
    status |= m_ltp->IO_disable_pg_gating(LTP::TR2);
  }
  else if (a_TestTrigger2_gating == RCDLTPdal::LTPExpertBypassedConfig::TestTrigger2_gating::Inhibit) {
    status |= m_ltp->IO_disable_busy_gating(LTP::TR2);
    status |= m_ltp->IO_enable_inhibit_gating(LTP::TR2);
    status |= m_ltp->IO_disable_pg_gating(LTP::TR2);
  }
  else if (a_TestTrigger2_gating == RCDLTPdal::LTPExpertBypassedConfig::TestTrigger2_gating::Patgen) {
    status |= m_ltp->IO_disable_busy_gating(LTP::TR2);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::TR2);
    status |= m_ltp->IO_enable_pg_gating(LTP::TR2);
  }

  if (a_TestTrigger2_pfn_width == RCDLTPdal::LTPExpertBypassedConfig::TestTrigger2_pfn_width::_1BC) status |= m_ltp->IO_pfn_width_1bc(LTP::TR2);
  else if (a_TestTrigger2_pfn_width == RCDLTPdal::LTPExpertBypassedConfig::TestTrigger2_pfn_width::_2BC) status |= m_ltp->IO_pfn_width_2bc(LTP::TR2);

  // -- Test Trigger 3
  if (a_TestTrigger3_local == RCDLTPdal::LTPExpertBypassedConfig::TestTrigger3_local::None) status |= m_ltp->IO_local_no_source(LTP::TR3);
  else if (a_TestTrigger3_local == RCDLTPdal::LTPExpertBypassedConfig::TestTrigger3_local::Lemo) status |= m_ltp->IO_local_lemo_direct(LTP::TR3);
  else if (a_TestTrigger3_local == RCDLTPdal::LTPExpertBypassedConfig::TestTrigger3_local::Lemo_via_pfn) status |= m_ltp->IO_local_lemo_pfn(LTP::TR3);
  else if (a_TestTrigger3_local == RCDLTPdal::LTPExpertBypassedConfig::TestTrigger3_local::Patgen) status |= m_ltp->IO_local_pg(LTP::TR3);

  if (a_TestTrigger3_lemoout_from == RCDLTPdal::LTPExpertBypassedConfig::TestTrigger3_lemoout_from::Local) status |= m_ltp->IO_lemoout_from_local(LTP::TR3);
  else if (a_TestTrigger3_lemoout_from == RCDLTPdal::LTPExpertBypassedConfig::TestTrigger3_lemoout_from::Linkin) status |= m_ltp->IO_lemoout_from_linkin(LTP::TR3);

  if (a_TestTrigger3_gating == RCDLTPdal::LTPExpertBypassedConfig::TestTrigger3_gating::None) {
    status |= m_ltp->IO_disable_busy_gating(LTP::TR3);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::TR3);
    status |= m_ltp->IO_disable_pg_gating(LTP::TR3);
  }
  else if (a_TestTrigger3_gating == RCDLTPdal::LTPExpertBypassedConfig::TestTrigger3_gating::Busy) {
    status |= m_ltp->IO_enable_busy_gating(LTP::TR3);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::TR3);
    status |= m_ltp->IO_disable_pg_gating(LTP::TR3);
  }
  else if (a_TestTrigger3_gating == RCDLTPdal::LTPExpertBypassedConfig::TestTrigger3_gating::Inhibit) {
    status |= m_ltp->IO_disable_busy_gating(LTP::TR3);
    status |= m_ltp->IO_enable_inhibit_gating(LTP::TR3);
    status |= m_ltp->IO_disable_pg_gating(LTP::TR3);
  }
  else if (a_TestTrigger3_gating == RCDLTPdal::LTPExpertBypassedConfig::TestTrigger3_gating::Patgen) {
    status |= m_ltp->IO_disable_busy_gating(LTP::TR3);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::TR3);
    status |= m_ltp->IO_enable_pg_gating(LTP::TR3);
  }

  if (a_TestTrigger3_pfn_width == RCDLTPdal::LTPExpertBypassedConfig::TestTrigger3_pfn_width::_1BC) status |= m_ltp->IO_pfn_width_1bc(LTP::TR3);
  else if (a_TestTrigger3_pfn_width == RCDLTPdal::LTPExpertBypassedConfig::TestTrigger3_pfn_width::_2BC) status |= m_ltp->IO_pfn_width_2bc(LTP::TR3);


  if (a_BGo0_local == RCDLTPdal::LTPExpertBypassedConfig::BGo0_local::None) status |= m_ltp->IO_local_no_source(LTP::BG0);
  else if (a_BGo0_local == RCDLTPdal::LTPExpertBypassedConfig::BGo0_local::Lemo) status |= m_ltp->IO_local_lemo_direct(LTP::BG0);
  else if (a_BGo0_local == RCDLTPdal::LTPExpertBypassedConfig::BGo0_local::Lemo_via_pfn) status |= m_ltp->IO_local_lemo_pfn(LTP::BG0);
  else if (a_BGo0_local == RCDLTPdal::LTPExpertBypassedConfig::BGo0_local::Patgen) status |= m_ltp->IO_local_pg(LTP::BG0);

  if (a_BGo0_lemoout_from == RCDLTPdal::LTPExpertBypassedConfig::BGo0_lemoout_from::Local) status |= m_ltp->IO_lemoout_from_local(LTP::BG0);
  else if (a_BGo0_lemoout_from == RCDLTPdal::LTPExpertBypassedConfig::BGo0_lemoout_from::Linkin) status |= m_ltp->IO_lemoout_from_linkin(LTP::BG0);

  if (a_BGo0_gating == RCDLTPdal::LTPExpertBypassedConfig::BGo0_gating::None) {
    status |= m_ltp->IO_disable_busy_gating(LTP::BG0);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::BG0);
    status |= m_ltp->IO_disable_pg_gating(LTP::BG0);
  }
  else if (a_BGo0_gating == RCDLTPdal::LTPExpertBypassedConfig::BGo0_gating::Busy) {
    status |= m_ltp->IO_enable_busy_gating(LTP::BG0);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::BG0);
    status |= m_ltp->IO_disable_pg_gating(LTP::BG0);
  }
  else if (a_BGo0_gating == RCDLTPdal::LTPExpertBypassedConfig::BGo0_gating::Inhibit) {
    status |= m_ltp->IO_disable_busy_gating(LTP::BG0);
    status |= m_ltp->IO_enable_inhibit_gating(LTP::BG0);
    status |= m_ltp->IO_disable_pg_gating(LTP::BG0);
  }
  else if (a_BGo0_gating == RCDLTPdal::LTPExpertBypassedConfig::BGo0_gating::Patgen) {
    status |= m_ltp->IO_disable_busy_gating(LTP::BG0);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::BG0);
    status |= m_ltp->IO_enable_pg_gating(LTP::BG0);
  }

  if (a_BGo0_pfn_width == RCDLTPdal::LTPExpertBypassedConfig::BGo0_pfn_width::_1BC) status |= m_ltp->IO_pfn_width_1bc(LTP::BG0);
  else if (a_BGo0_pfn_width == RCDLTPdal::LTPExpertBypassedConfig::BGo0_pfn_width::_2BC) status |= m_ltp->IO_pfn_width_2bc(LTP::BG0);

  if (a_BGo1_local == RCDLTPdal::LTPExpertBypassedConfig::BGo1_local::None) status |= m_ltp->IO_local_no_source(LTP::BG1);
  else if (a_BGo1_local == RCDLTPdal::LTPExpertBypassedConfig::BGo1_local::Lemo) status |= m_ltp->IO_local_lemo_direct(LTP::BG1);
  else if (a_BGo1_local == RCDLTPdal::LTPExpertBypassedConfig::BGo1_local::Lemo_via_pfn) status |= m_ltp->IO_local_lemo_pfn(LTP::BG1);
  else if (a_BGo1_local == RCDLTPdal::LTPExpertBypassedConfig::BGo1_local::Patgen) status |= m_ltp->IO_local_pg(LTP::BG1);
  
  if (a_BGo1_lemoout_from == RCDLTPdal::LTPExpertBypassedConfig::BGo1_lemoout_from::Local) status |= m_ltp->IO_lemoout_from_local(LTP::BG1);
  else if (a_BGo1_lemoout_from == RCDLTPdal::LTPExpertBypassedConfig::BGo1_lemoout_from::Linkin) status |= m_ltp->IO_lemoout_from_linkin(LTP::BG1);
      
  if (a_BGo1_gating == RCDLTPdal::LTPExpertBypassedConfig::BGo1_gating::None) {
    status |= m_ltp->IO_disable_busy_gating(LTP::BG1);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::BG1);
    status |= m_ltp->IO_disable_pg_gating(LTP::BG1);
  }
  else if (a_BGo1_gating == RCDLTPdal::LTPExpertBypassedConfig::BGo1_gating::Busy) {
    status |= m_ltp->IO_enable_busy_gating(LTP::BG1);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::BG1);
    status |= m_ltp->IO_disable_pg_gating(LTP::BG1);
  }
  else if (a_BGo1_gating == RCDLTPdal::LTPExpertBypassedConfig::BGo1_gating::Inhibit) {
    status |= m_ltp->IO_disable_busy_gating(LTP::BG1);
    status |= m_ltp->IO_enable_inhibit_gating(LTP::BG1);
    status |= m_ltp->IO_disable_pg_gating(LTP::BG1);
  }
  else if (a_BGo1_gating == RCDLTPdal::LTPExpertBypassedConfig::BGo1_gating::Patgen) {
    status |= m_ltp->IO_disable_busy_gating(LTP::BG1);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::BG1);
    status |= m_ltp->IO_enable_pg_gating(LTP::BG1);
  }

  if (a_BGo1_pfn_width == RCDLTPdal::LTPExpertBypassedConfig::BGo1_pfn_width::_1BC) status |= m_ltp->IO_pfn_width_1bc(LTP::BG1);
  else if (a_BGo1_pfn_width == RCDLTPdal::LTPExpertBypassedConfig::BGo1_pfn_width::_2BC) status |= m_ltp->IO_pfn_width_2bc(LTP::BG1);

  if (a_BGo2_local == RCDLTPdal::LTPExpertBypassedConfig::BGo2_local::None) status |= m_ltp->IO_local_no_source(LTP::BG2);
  else if (a_BGo2_local == RCDLTPdal::LTPExpertBypassedConfig::BGo2_local::Lemo) status |= m_ltp->IO_local_lemo_direct(LTP::BG2);
  else if (a_BGo2_local == RCDLTPdal::LTPExpertBypassedConfig::BGo2_local::Lemo_via_pfn) status |= m_ltp->IO_local_lemo_pfn(LTP::BG2);
  else if (a_BGo2_local == RCDLTPdal::LTPExpertBypassedConfig::BGo2_local::Patgen) status |= m_ltp->IO_local_pg(LTP::BG2);

  if (a_BGo2_lemoout_from == RCDLTPdal::LTPExpertBypassedConfig::BGo2_lemoout_from::Local) status |= m_ltp->IO_lemoout_from_local(LTP::BG2);
  else if (a_BGo2_lemoout_from == RCDLTPdal::LTPExpertBypassedConfig::BGo2_lemoout_from::Linkin) status |= m_ltp->IO_lemoout_from_linkin(LTP::BG2);

  if (a_BGo2_gating == RCDLTPdal::LTPExpertBypassedConfig::BGo2_gating::None) {
    status |= m_ltp->IO_disable_busy_gating(LTP::BG2);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::BG2);
    status |= m_ltp->IO_disable_pg_gating(LTP::BG2);
  }
  else if (a_BGo2_gating == RCDLTPdal::LTPExpertBypassedConfig::BGo2_gating::Busy) {
    status |= m_ltp->IO_enable_busy_gating(LTP::BG2);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::BG2);
    status |= m_ltp->IO_disable_pg_gating(LTP::BG2);
  }
  else if (a_BGo2_gating == RCDLTPdal::LTPExpertBypassedConfig::BGo2_gating::Inhibit) {
    status |= m_ltp->IO_disable_busy_gating(LTP::BG2);
    status |= m_ltp->IO_enable_inhibit_gating(LTP::BG2);
    status |= m_ltp->IO_disable_pg_gating(LTP::BG2);
  }
  else if (a_BGo2_gating == RCDLTPdal::LTPExpertBypassedConfig::BGo2_gating::Patgen) {
    status |= m_ltp->IO_disable_busy_gating(LTP::BG2);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::BG2);
    status |= m_ltp->IO_enable_pg_gating(LTP::BG2);
  }

  if (a_BGo2_pfn_width == RCDLTPdal::LTPExpertBypassedConfig::BGo2_pfn_width::_1BC) status |= m_ltp->IO_pfn_width_1bc(LTP::BG2);
  else if (a_BGo2_pfn_width == RCDLTPdal::LTPExpertBypassedConfig::BGo2_pfn_width::_2BC) status |= m_ltp->IO_pfn_width_2bc(LTP::BG2);

  if (a_BGo3_local == RCDLTPdal::LTPExpertBypassedConfig::BGo3_local::None) status |= m_ltp->IO_local_no_source(LTP::BG3);
  else if (a_BGo3_local == RCDLTPdal::LTPExpertBypassedConfig::BGo3_local::Lemo) status |= m_ltp->IO_local_lemo_direct(LTP::BG3);
  else if (a_BGo3_local == RCDLTPdal::LTPExpertBypassedConfig::BGo3_local::Lemo_via_pfn) status |= m_ltp->IO_local_lemo_pfn(LTP::BG3);
  else if (a_BGo3_local == RCDLTPdal::LTPExpertBypassedConfig::BGo3_local::Patgen) status |= m_ltp->IO_local_pg(LTP::BG3);

  if (a_BGo3_lemoout_from == RCDLTPdal::LTPExpertBypassedConfig::BGo3_lemoout_from::Local) status |= m_ltp->IO_lemoout_from_local(LTP::BG3);
  else if (a_BGo3_lemoout_from == RCDLTPdal::LTPExpertBypassedConfig::BGo3_lemoout_from::Linkin) status |= m_ltp->IO_lemoout_from_linkin(LTP::BG3);

  if (a_BGo3_gating == RCDLTPdal::LTPExpertBypassedConfig::BGo3_gating::None) {
    status |= m_ltp->IO_disable_busy_gating(LTP::BG3);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::BG3);
    status |= m_ltp->IO_disable_pg_gating(LTP::BG3);
  }
  else if (a_BGo3_gating == RCDLTPdal::LTPExpertBypassedConfig::BGo3_gating::Busy) {
    status |= m_ltp->IO_enable_busy_gating(LTP::BG3);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::BG3);
    status |= m_ltp->IO_disable_pg_gating(LTP::BG3);
  }
  else if (a_BGo3_gating == RCDLTPdal::LTPExpertBypassedConfig::BGo3_gating::Inhibit) {
    status |= m_ltp->IO_disable_busy_gating(LTP::BG3);
    status |= m_ltp->IO_enable_inhibit_gating(LTP::BG3);
    status |= m_ltp->IO_disable_pg_gating(LTP::BG3);
  }
  else if (a_BGo3_gating == RCDLTPdal::LTPExpertBypassedConfig::BGo3_gating::Patgen) {
    status |= m_ltp->IO_disable_busy_gating(LTP::BG3);
    status |= m_ltp->IO_disable_inhibit_gating(LTP::BG3);
    status |= m_ltp->IO_enable_pg_gating(LTP::BG3);
  }

  if (a_BGo3_pfn_width == RCDLTPdal::LTPExpertBypassedConfig::BGo3_pfn_width::_1BC) status |= m_ltp->IO_pfn_width_1bc(LTP::BG3);
  else if (a_BGo3_pfn_width == RCDLTPdal::LTPExpertBypassedConfig::BGo3_pfn_width::_2BC) status |= m_ltp->IO_pfn_width_2bc(LTP::BG3);


  // busy + calreq + triggertype
  std::string a_Busy_deadtime_source = ltpConfig->get_Busy_deadtime_source();
  ERS_LOG(sout << "Busy_deadtime_source = " << a_Busy_deadtime_source);
  bool a_Busy_local_with_deadtime = ltpConfig->get_Busy_local_with_deadtime();
  ERS_LOG(sout << "Busy_local_with_deadtime = " << a_Busy_local_with_deadtime);
  std::string a_Busy_lemoout = ltpConfig->get_Busy_lemoout();
  ERS_LOG(sout << "Busy_lemoout = " << a_Busy_lemoout);
  bool a_Busy_from_patgen = ltpConfig->get_Busy_from_patgen();
  ERS_LOG(sout << "Busy_from_patgen = " << a_Busy_from_patgen);
  std::string a_Busy_deadtime_duration = ltpConfig->get_Busy_deadtime_duration();
  ERS_LOG(sout << "Busy_deadtime_duration = " << a_Busy_deadtime_duration);
  std::string a_TriggerType_fifotrigger = ltpConfig->get_TriggerType_fifotrigger();
  ERS_LOG(sout << "TriggerType_fifotrigger = " << a_TriggerType_fifotrigger);
  std::string a_TriggerType_fifodelay = ltpConfig->get_TriggerType_fifodelay();
  ERS_LOG(sout << "TriggerType_fifodelay = " << a_TriggerType_fifodelay);
  std::string a_TriggerType_regfile_address = ltpConfig->get_TriggerType_regfile_address();
  ERS_LOG(sout << "TriggerType_regfile_address = " << a_TriggerType_regfile_address);
  bool a_TriggerType_eclout_enabled = ltpConfig->get_TriggerType_eclout_enabled();
  ERS_LOG(sout << "TriggerType_eclout_enabled = " << a_TriggerType_eclout_enabled);
  std::string a_TriggerType_local = ltpConfig->get_TriggerType_local();
  ERS_LOG(sout << "TriggerType_local = " << a_TriggerType_local);
  std::string a_TriggerType_eclout_from = ltpConfig->get_TriggerType_eclout_from();
  ERS_LOG(sout << "TriggerType_eclout_from = " << a_TriggerType_eclout_from);
  unsigned int a_TriggerType_regfile0_data = ltpConfig->get_TriggerType_regfile0_data();
  ERS_LOG(sout << "TriggerType_regfile0_data = " << a_TriggerType_regfile0_data);
  unsigned int a_TriggerType_regfile1_data = ltpConfig->get_TriggerType_regfile1_data();
  ERS_LOG(sout << "TriggerType_regfile1_data = " << a_TriggerType_regfile1_data);
  unsigned int a_TriggerType_regfile2_data = ltpConfig->get_TriggerType_regfile2_data();
  ERS_LOG(sout << "TriggerType_regfile2_data = " << a_TriggerType_regfile2_data);
  unsigned int a_TriggerType_regfile3_data = ltpConfig->get_TriggerType_regfile3_data();
  ERS_LOG(sout << "TriggerType_regfile3_data = " << a_TriggerType_regfile3_data);
  unsigned int a_Inhibit_delay = ltpConfig->get_Inhibit_delay();
  ERS_LOG(sout << "Inhibit_delay = " << a_Inhibit_delay);

  // Busy
  if (a_Busy_deadtime_source == RCDLTPdal::LTPExpertBypassedConfig::Busy_deadtime_source::L1A) status |= m_ltp->BUSY_deadtime_source_L1A();
  else if (a_Busy_deadtime_source == RCDLTPdal::LTPExpertBypassedConfig::Busy_deadtime_source::TestTrigger1) status |= m_ltp->BUSY_deadtime_source_TR1();
  else if (a_Busy_deadtime_source == RCDLTPdal::LTPExpertBypassedConfig::Busy_deadtime_source::TestTrigger2) status |= m_ltp->BUSY_deadtime_source_TR2();
  else if (a_Busy_deadtime_source == RCDLTPdal::LTPExpertBypassedConfig::Busy_deadtime_source::TestTrigger3) status |= m_ltp->BUSY_deadtime_source_TR3();
  
  if (a_Busy_lemoout == RCDLTPdal::LTPExpertBypassedConfig::Busy_lemoout::Busy_only) status |= m_ltp->BUSY_lemoout_busy_only();
  else if (a_Busy_lemoout == RCDLTPdal::LTPExpertBypassedConfig::Busy_lemoout::Busy_or_deadtime) status |= m_ltp->BUSY_lemoout_busy_or_deadtime();
  else if (a_Busy_lemoout == RCDLTPdal::LTPExpertBypassedConfig::Busy_lemoout::Timer_inhibit) status |= m_ltp->BUSY_lemoout_inhibit();
  else if (a_Busy_lemoout == RCDLTPdal::LTPExpertBypassedConfig::Busy_lemoout::Patgen_inhibit) status |= m_ltp->BUSY_lemoout_pg_inhibit();
  
  m_Busy_current[0] = m_Busy_local_with_linkin;
  m_Busy_current[1] = m_Busy_local_with_ttlin;
  m_Busy_current[2] = m_Busy_local_with_nimin;
  this->updateBusy();
    
  if (a_Busy_local_with_deadtime) status |= m_ltp->BUSY_local_with_deadtime();
  else status |= m_ltp->BUSY_local_without_deadtime();
    
  if (a_Busy_from_patgen) status |= m_ltp->BUSY_enable_from_pg();
  else status |= m_ltp->BUSY_disable_from_pg();

  if      (a_Busy_deadtime_duration == RCDLTPdal::LTPExpertBypassedConfig::Busy_deadtime_duration::_4BC) status |= m_ltp->BUSY_deadtime_duration(0);
  else if (a_Busy_deadtime_duration == RCDLTPdal::LTPExpertBypassedConfig::Busy_deadtime_duration::_5BC) status |= m_ltp->BUSY_deadtime_duration(1);
  else if (a_Busy_deadtime_duration == RCDLTPdal::LTPExpertBypassedConfig::Busy_deadtime_duration::_6BC) status |= m_ltp->BUSY_deadtime_duration(2);
  else if (a_Busy_deadtime_duration == RCDLTPdal::LTPExpertBypassedConfig::Busy_deadtime_duration::_7BC) status |= m_ltp->BUSY_deadtime_duration(3);
  else if (a_Busy_deadtime_duration == RCDLTPdal::LTPExpertBypassedConfig::Busy_deadtime_duration::_8BC) status |= m_ltp->BUSY_deadtime_duration(4);
  else if (a_Busy_deadtime_duration == RCDLTPdal::LTPExpertBypassedConfig::Busy_deadtime_duration::_9BC) status |= m_ltp->BUSY_deadtime_duration(5);
  else if (a_Busy_deadtime_duration == RCDLTPdal::LTPExpertBypassedConfig::Busy_deadtime_duration::_10BC) status |= m_ltp->BUSY_deadtime_duration(6);
  else if (a_Busy_deadtime_duration == RCDLTPdal::LTPExpertBypassedConfig::Busy_deadtime_duration::_11BC) status |= m_ltp->BUSY_deadtime_duration(7);
    
  // Trigger Type
  if (a_TriggerType_fifotrigger == RCDLTPdal::LTPExpertBypassedConfig::TriggerType_fifotrigger::L1A) status |= m_ltp->TTYPE_fifotrigger_L1A();
  else if (a_TriggerType_fifotrigger == RCDLTPdal::LTPExpertBypassedConfig::TriggerType_fifotrigger::TestTrigger1) status |= m_ltp->TTYPE_fifotrigger_TR1();
  else if (a_TriggerType_fifotrigger == RCDLTPdal::LTPExpertBypassedConfig::TriggerType_fifotrigger::TestTrigger2) status |= m_ltp->TTYPE_fifotrigger_TR2();
  else if (a_TriggerType_fifotrigger == RCDLTPdal::LTPExpertBypassedConfig::TriggerType_fifotrigger::TestTrigger3) status |= m_ltp->TTYPE_fifotrigger_TR3();

  if (a_TriggerType_fifodelay == RCDLTPdal::LTPExpertBypassedConfig::TriggerType_fifodelay::_2BC) status |= m_ltp->TTYPE_fifodelay_2bc();
  else if (a_TriggerType_fifodelay == RCDLTPdal::LTPExpertBypassedConfig::TriggerType_fifodelay::_3BC) status |= m_ltp->TTYPE_fifodelay_3bc();
  else if (a_TriggerType_fifodelay == RCDLTPdal::LTPExpertBypassedConfig::TriggerType_fifodelay::_4BC) status |= m_ltp->TTYPE_fifodelay_4bc();
  else if (a_TriggerType_fifodelay == RCDLTPdal::LTPExpertBypassedConfig::TriggerType_fifodelay::_5BC) status |= m_ltp->TTYPE_fifodelay_5bc();

  if (a_TriggerType_regfile_address == RCDLTPdal::LTPExpertBypassedConfig::TriggerType_regfile_address::File0) status |= m_ltp->TTYPE_regfile_address_0();
  else if (a_TriggerType_regfile_address == RCDLTPdal::LTPExpertBypassedConfig::TriggerType_regfile_address::File1) status |= m_ltp->TTYPE_regfile_address_1();
  else if (a_TriggerType_regfile_address == RCDLTPdal::LTPExpertBypassedConfig::TriggerType_regfile_address::File2) status |= m_ltp->TTYPE_regfile_address_2();
  else if (a_TriggerType_regfile_address == RCDLTPdal::LTPExpertBypassedConfig::TriggerType_regfile_address::File3) status |= m_ltp->TTYPE_regfile_address_3();
 
  if (a_TriggerType_eclout_enabled) status |= m_ltp->TTYPE_enable_eclout();
  else status |= m_ltp->TTYPE_disable_eclout();
 
  if (a_TriggerType_local == RCDLTPdal::LTPExpertBypassedConfig::TriggerType_local::Patgen) status |= m_ltp->TTYPE_local_from_pg();
  else if (a_TriggerType_local == RCDLTPdal::LTPExpertBypassedConfig::TriggerType_local::Vme) status |= m_ltp->TTYPE_local_from_vme();

  if (a_TriggerType_eclout_from == RCDLTPdal::LTPExpertBypassedConfig::TriggerType_eclout_from::Local) status |= m_ltp->TTYPE_eclout_from_local();
  else if (a_TriggerType_eclout_from == RCDLTPdal::LTPExpertBypassedConfig::TriggerType_eclout_from::Linkin) status |= m_ltp->TTYPE_eclout_from_linkin();

  status |= m_ltp->TTYPE_write_regfile0(a_TriggerType_regfile0_data);
  status |= m_ltp->TTYPE_write_regfile1(a_TriggerType_regfile1_data);
  status |= m_ltp->TTYPE_write_regfile2(a_TriggerType_regfile2_data);
  status |= m_ltp->TTYPE_write_regfile3(a_TriggerType_regfile3_data);

  // Inhibit time value
  status |= m_ltp->INH_writeShadow(a_Inhibit_delay);

  // setup pattern generator
  m_use_patgen = false;

  std::string a_PatGen_runmode = ltpConfig->get_PatGen_runmode();
  ERS_LOG(sout << "PatGen_runmode = \"" << a_PatGen_runmode << "\"");
  m_PatGen_runmode = a_PatGen_runmode;
  std::string a_PatGen_filename = ltpConfig->get_PatGen_filename();
  ERS_LOG(sout << "PatGen_filename = \"" << a_PatGen_filename << "\"");
  
  u_int a_PatGen_fixed_value = ltpConfig->get_PatGen_fixed_value();
  ERS_LOG(sout << "PatGen_fixed_value = 0x" << std::hex << a_PatGen_fixed_value << std::dec);

  if (a_PatGen_runmode == RCDLTPdal::LTPExpertBypassedConfig::PatGen_runmode::Off) {
    status |= m_ltp->PG_runmode_notused();
    m_use_patgen = false;
  }
  else {

    ERS_LOG(sout << "Pattern generator mode is not OFF, therefore it is being used");
    m_use_patgen = true;
    ERS_LOG(sout << "m_use_patgen = " << m_use_patgen);

    // load patterns
    ERS_LOG(sout << "Reset pattern generator");
    status |= m_ltp->PG_reset();

    if (a_PatGen_runmode != RCDLTPdal::LTPExpertConfig::PatGen_runmode::Fixed_Value) {
      // first try if there is an LTPPatternBase linked
      const RCDLTPdal::LTPPatternBase* pattern = ltpConfig->get_PatGen_pattern();
      if (pattern != nullptr) {
	this->loadPattern(pattern);
      } else { // this else clause should become obsolete in the next tdaq version

	ERS_LOG(sout << ">>> OBSOLETE: loading a pattern via the PatGen_pattern attribute is obsolete and will be dropped in the next release! Please use an object of class type LTPPatternBase instead");
	ERS_LOG(sout << "Load pattern from file: \"" << a_PatGen_filename << "\"");
	status |= m_ltp->PG_runmode_load();
    
	int lstatus = m_ltp->PG_load_memory_from_file(&a_PatGen_filename);
	if ( lstatus != 0 ) {
	  ostringstream text;
	  text << sout << "Could not load pattern to pattern generator memory. PatGen_filename = " << a_PatGen_filename;
	  ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
	}
      }
    }

    // copied runmode into member variable, set run mode in connect
    status |= m_ltp->PG_reset();
    status |= m_ltp->PG_set_fixed_value(a_PatGen_fixed_value);
  }

  if (status != 0) {
    ostringstream text;
    text << sout << "VME error, status="<< status << "(0x" << std::hex << status << std::dec << ")";
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
  }

  ERS_LOG(sout << "Done");
}  


// ----------------------------------------------------------------------------------
void  LTPModule::configurePatgen(const RCDLTPdal::LTPPatgenConfig* ltpConfig) {
  std::string sout = m_UID + "@LTPModule::configurePatgen() ";
  ERS_LOG(sout << "Entered");

  // dump configuration object
  ERS_LOG(sout << "called with LTPPatgenConfig = " << ltpConfig);
  ltpConfig->print(1,true,std::cout);

  // configure according to master with relevant signals from pattern generator

  m_use_patgen = true;

  // attributes from LTPPatgenConfig
  m_boardReset = ltpConfig->get_BoardReset();
  ERS_LOG(sout << "BoardReset = " << m_boardReset);
  std::string a_Trigger = ltpConfig->get_Trigger();
  ERS_LOG(sout << "Trigger = \"" << a_Trigger << "\"");
  std::string a_PatGen_runmode = ltpConfig->get_PatGen_runmode();
  ERS_LOG(sout << "PatGen_runmode = \"" << a_PatGen_runmode << "\"");
  std::string a_PatGen_filename = ltpConfig->get_PatGen_filename();
  ERS_LOG(sout << "PatGen_filename = \"" << a_PatGen_filename << "\"");

  // copy runmode into member variable, set run mode in connect
  m_PatGen_runmode = a_PatGen_runmode;

  int status = 0;

  // reset
  if (m_boardReset) {
    status |= m_ltp->Reset();
  }

  // bc
  status |= m_ltp->BC_local_intern();
  status |= m_ltp->BC_linkout_from_local();
  status |= m_ltp->BC_lemoout_from_local();

  // orbit
  status |= m_ltp->ORB_local_intern();
  status |= m_ltp->ORB_linkout_from_local();
  status |= m_ltp->ORB_lemoout_from_local();

  // link out from local for all signals
  status |= m_ltp->IO_linkout_from_local(LTP::L1A);
  status |= m_ltp->IO_lemoout_from_local(LTP::L1A);
  status |= m_ltp->IO_local_pg(LTP::L1A);
  status |= m_ltp->IO_disable_pg_gating(LTP::L1A);
  status |= m_ltp->IO_enable_busy_gating(LTP::L1A);
  status |= m_ltp->IO_disable_inhibit_gating(LTP::L1A);

  status |= m_ltp->IO_linkout_from_local(LTP::TR1);
  status |= m_ltp->IO_lemoout_from_local(LTP::TR1);
  status |= m_ltp->IO_local_pg(LTP::TR1);
  status |= m_ltp->IO_disable_pg_gating(LTP::TR1);
  status |= m_ltp->IO_enable_busy_gating(LTP::TR1);
  status |= m_ltp->IO_disable_inhibit_gating(LTP::TR1);

  status |= m_ltp->IO_linkout_from_local(LTP::TR2);
  status |= m_ltp->IO_lemoout_from_local(LTP::TR2);
  status |= m_ltp->IO_local_pg(LTP::TR2); 
  status |= m_ltp->IO_disable_pg_gating(LTP::TR2);
  status |= m_ltp->IO_enable_busy_gating(LTP::TR2);
  status |= m_ltp->IO_disable_inhibit_gating(LTP::TR2);

  status |= m_ltp->IO_linkout_from_local(LTP::TR3);
  status |= m_ltp->IO_lemoout_from_local(LTP::TR3);
  status |= m_ltp->IO_local_pg(LTP::TR3);
  status |= m_ltp->IO_disable_pg_gating(LTP::TR3);
  status |= m_ltp->IO_enable_busy_gating(LTP::TR3);
  status |= m_ltp->IO_disable_inhibit_gating(LTP::TR3);

  status |= m_ltp->IO_linkout_from_local(LTP::BG0);
  status |= m_ltp->IO_lemoout_from_local(LTP::BG0);
  status |= m_ltp->IO_local_pg(LTP::BG0);
  status |= m_ltp->IO_disable_pg_gating(LTP::BG0);
  status |= m_ltp->IO_disable_busy_gating(LTP::BG0);
  status |= m_ltp->IO_disable_inhibit_gating(LTP::BG0);

  status |= m_ltp->IO_linkout_from_local(LTP::BG1);
  status |= m_ltp->IO_lemoout_from_local(LTP::BG1);
  status |= m_ltp->IO_local_pg(LTP::BG1);
  status |= m_ltp->IO_disable_pg_gating(LTP::BG1);
  status |= m_ltp->IO_disable_busy_gating(LTP::BG1);
  status |= m_ltp->IO_disable_inhibit_gating(LTP::BG1);

  status |= m_ltp->IO_linkout_from_local(LTP::BG2);
  status |= m_ltp->IO_lemoout_from_local(LTP::BG2);
  status |= m_ltp->IO_local_pg(LTP::BG2);
  status |= m_ltp->IO_disable_pg_gating(LTP::BG2);
  status |= m_ltp->IO_disable_busy_gating(LTP::BG2);
  status |= m_ltp->IO_disable_inhibit_gating(LTP::BG2);

  status |= m_ltp->IO_linkout_from_local(LTP::BG3);
  status |= m_ltp->IO_lemoout_from_local(LTP::BG3);
  status |= m_ltp->IO_local_pg(LTP::BG3);
  status |= m_ltp->IO_disable_pg_gating(LTP::BG3);
  status |= m_ltp->IO_disable_busy_gating(LTP::BG3);
  status |= m_ltp->IO_disable_inhibit_gating(LTP::BG3);

  // busy
  status |= m_ltp->BUSY_linkout_from_local();
  // configure busy mask according to values obtained from database during setup()
  m_Busy_current[0] = m_Busy_local_with_linkin;
  m_Busy_current[1] = m_Busy_local_with_ttlin;
  m_Busy_current[2] = m_Busy_local_with_nimin;
  this->updateBusy();

  status |= m_ltp->BUSY_disable_from_pg();
  status |= m_ltp->BUSY_local_without_deadtime();
  status |= m_ltp->BUSY_lemoout_busy_only();
 
  // trigger type
  status |= m_ltp->TTYPE_linkout_from_local();
  status |= m_ltp->TTYPE_eclout_from_local();
  status |= m_ltp->TTYPE_enable_eclout();
  status |= m_ltp->TTYPE_local_from_pg();
  status |= m_ltp->TTYPE_write_regfile0(0x0000);
  status |= m_ltp->TTYPE_write_regfile1(0x0000);
  status |= m_ltp->TTYPE_write_regfile2(0x0000);
  status |= m_ltp->TTYPE_write_regfile3(0x0000);
  status |= m_ltp->TTYPE_reset_fifo();

  // cal req
  status |= m_ltp->CAL_source_pg();
  status |= m_ltp->CAL_enable_testpath();
 
  // counter
  if (a_Trigger == RCDLTPdal::LTPPatgenConfig::Trigger::L1A) {
    ERS_LOG(sout << "Trigger = L1A");
    status |= m_ltp->TTYPE_fifotrigger_L1A();
  }
  else if (a_Trigger == RCDLTPdal::LTPPatgenConfig::Trigger::TestTrigger1) {
    ERS_LOG(sout << "Trigger = TestTrigger1");
    status |= m_ltp->TTYPE_fifotrigger_TR1();
  }
  else if (a_Trigger == RCDLTPdal::LTPPatgenConfig::Trigger::TestTrigger2) {
    ERS_LOG(sout << "Trigger = TestTrigger2");
    status |= m_ltp->TTYPE_fifotrigger_TR2();
  }
  else if (a_Trigger == RCDLTPdal::LTPPatgenConfig::Trigger::TestTrigger3) {
    ERS_LOG(sout << "Trigger = TestTrigger3");
    status |= m_ltp->TTYPE_fifotrigger_TR3();
  } else {
    ERS_LOG(sout << "unknown Trigger = \"" << a_Trigger << "\"");
  }

  ERS_LOG(sout << "m_use_patgen = " << m_use_patgen);
  
  // load patterns
  ERS_LOG(sout << "Reset pattern generator");
  status |= m_ltp->PG_reset();

  if (a_PatGen_runmode != RCDLTPdal::LTPExpertConfig::PatGen_runmode::Fixed_Value) {
    // first try if there is an LTPPatternBase linked
    const RCDLTPdal::LTPPatternBase* pattern = ltpConfig->get_PatGen_pattern();
    if (pattern != nullptr) {
      this->loadPattern(pattern);
    } else { // this else clause should become obsolete in the next tdaq version
      
      ERS_LOG(sout << ">>> OBSOLETE: loading a pattern via the PatGen_pattern attribute is obsolete and will be dropped in the next release! Please use an object of class type LTPPatternBase instead");
      ERS_LOG(sout << "Load pattern from file: \"" << a_PatGen_filename << "\"");
      status |= m_ltp->PG_runmode_load();
      
      int lstatus = m_ltp->PG_load_memory_from_file(&a_PatGen_filename);
      if ( lstatus != 0 ) {
	ostringstream text;
	text << sout << "Could not load pattern to pattern generator memory. PatGen_filename = " << a_PatGen_filename;
	ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
      }
    }
  }

  // copied runmode into member variable, set run mode in connect
  status |= m_ltp->PG_reset();
  status |= m_ltp->PG_set_fixed_value(0x0000);

  status |= m_ltp->COUNTER_reset();
  status |= m_ltp->TTYPE_reset_fifo();

  if (status != 0) {
    ostringstream text;
    text << sout << "VME error, status="<< status << "(0x" << std::hex << status << std::dec << ")";
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
  }

  ERS_LOG(sout << "Done");
}  


// ----------------------------------------------------------------------------------
void  LTPModule::configureBypass(const RCDLTPdal::LTPBypassConfig* ltpConfig) {
  std::string sout = m_UID + "@LTPModule::configureBypass() ";
  ERS_LOG(sout << "Entered");

  // dump configuration object
  ERS_LOG(sout << "called with LTPBypassConfig = " << ltpConfig);
  ltpConfig->print(1,true,std::cout);

  // configure for bypass
  
  m_use_patgen = false;

  int status = 0;

  // no reset at the beginning

  // bc
  status |= m_ltp->BC_linkout_from_linkin();

  // orbit
  status |= m_ltp->ORB_linkout_from_linkin();

  // link in to link out for all signals
  status |= m_ltp->IO_linkout_from_linkin(LTP::L1A);
  status |= m_ltp->IO_linkout_from_linkin(LTP::TR1);
  status |= m_ltp->IO_linkout_from_linkin(LTP::TR2);
  status |= m_ltp->IO_linkout_from_linkin(LTP::TR3);
  status |= m_ltp->IO_linkout_from_linkin(LTP::BG0);
  status |= m_ltp->IO_linkout_from_linkin(LTP::BG1);
  status |= m_ltp->IO_linkout_from_linkin(LTP::BG2);
  status |= m_ltp->IO_linkout_from_linkin(LTP::BG3);

  // busy
  status |= m_ltp->BUSY_linkout_from_linkin ();

  // trigger type
  status |= m_ltp->TTYPE_linkout_from_linkin();

  // cal req
  status |= m_ltp->CAL_source_link();
  status |= m_ltp->CAL_enable_testpath();
 
  if (status != 0) {
    ostringstream text;
    text << sout << "VME error, status="<< status << "(0x" << std::hex << status << std::dec << ")";
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
  }

  ERS_LOG(sout << "Done");
}  

// ----------------------------------------------------------------------------------
void  LTPModule::configurePatgenBypassed(const RCDLTPdal::LTPPatgenBypassedConfig* ltpConfig) {
  std::string sout = m_UID + "@LTPModule::configurePatgenBypassed() ";
  ERS_LOG(sout << "Entered");

  // dump configuration object
  ERS_LOG(sout << "called with LTPPatgenBypassedConfig = " << ltpConfig);
  ltpConfig->print(1,true,std::cout);

  // configure according to master with relevant signals from pattern generator,
  // but not touching the linkout

  m_use_patgen = true;

  // attributes from LTPPatgenConfig

  std::string a_Trigger = ltpConfig->get_Trigger();
  ERS_LOG(sout << "Trigger = \"" << a_Trigger << "\"");
  std::string a_PatGen_runmode = ltpConfig->get_PatGen_runmode();
  ERS_LOG(sout << "PatGen_runmode = \"" << a_PatGen_runmode << "\"");
  std::string a_PatGen_filename = ltpConfig->get_PatGen_filename();
  ERS_LOG(sout << "PatGen_filename = \"" << a_PatGen_filename << "\"");

  // copy runmode into member variable, set run mode in connect
  m_PatGen_runmode = a_PatGen_runmode;

  int status = 0;

  // bc
  status |= m_ltp->BC_local_intern();
  status |= m_ltp->BC_lemoout_from_local();

  // orbit
  status |= m_ltp->ORB_local_intern();
  status |= m_ltp->ORB_lemoout_from_local();

  // link out from local for all signals
  status |= m_ltp->IO_lemoout_from_local(LTP::L1A);
  status |= m_ltp->IO_local_pg(LTP::L1A);
  status |= m_ltp->IO_disable_pg_gating(LTP::L1A);
  status |= m_ltp->IO_enable_busy_gating(LTP::L1A);
  status |= m_ltp->IO_disable_inhibit_gating(LTP::L1A);

  status |= m_ltp->IO_lemoout_from_local(LTP::TR1);
  status |= m_ltp->IO_local_pg(LTP::TR1);
  status |= m_ltp->IO_disable_pg_gating(LTP::TR1);
  status |= m_ltp->IO_enable_busy_gating(LTP::TR1);
  status |= m_ltp->IO_disable_inhibit_gating(LTP::TR1);

  status |= m_ltp->IO_lemoout_from_local(LTP::TR2);
  status |= m_ltp->IO_local_pg(LTP::TR2); 
  status |= m_ltp->IO_disable_pg_gating(LTP::TR2);
  status |= m_ltp->IO_enable_busy_gating(LTP::TR2);
  status |= m_ltp->IO_disable_inhibit_gating(LTP::TR2);

  status |= m_ltp->IO_lemoout_from_local(LTP::TR3);
  status |= m_ltp->IO_local_pg(LTP::TR3);
  status |= m_ltp->IO_disable_pg_gating(LTP::TR3);
  status |= m_ltp->IO_enable_busy_gating(LTP::TR3);
  status |= m_ltp->IO_disable_inhibit_gating(LTP::TR3);

  status |= m_ltp->IO_lemoout_from_local(LTP::BG0);
  status |= m_ltp->IO_local_pg(LTP::BG0);
  status |= m_ltp->IO_disable_pg_gating(LTP::BG0);
  status |= m_ltp->IO_disable_busy_gating(LTP::BG0);
  status |= m_ltp->IO_disable_inhibit_gating(LTP::BG0);

  status |= m_ltp->IO_lemoout_from_local(LTP::BG1);
  status |= m_ltp->IO_local_pg(LTP::BG1);
  status |= m_ltp->IO_disable_pg_gating(LTP::BG1);
  status |= m_ltp->IO_disable_busy_gating(LTP::BG1);
  status |= m_ltp->IO_disable_inhibit_gating(LTP::BG1);

  status |= m_ltp->IO_lemoout_from_local(LTP::BG2);
  status |= m_ltp->IO_local_pg(LTP::BG2);
  status |= m_ltp->IO_disable_pg_gating(LTP::BG2);
  status |= m_ltp->IO_disable_busy_gating(LTP::BG2);
  status |= m_ltp->IO_disable_inhibit_gating(LTP::BG2);

  status |= m_ltp->IO_lemoout_from_local(LTP::BG3);
  status |= m_ltp->IO_local_pg(LTP::BG3);
  status |= m_ltp->IO_disable_pg_gating(LTP::BG3);
  status |= m_ltp->IO_disable_busy_gating(LTP::BG3);
  status |= m_ltp->IO_disable_inhibit_gating(LTP::BG3);

  // busy
  // configure busy mask according to values obtained from database during setup()
  m_Busy_current[0] = m_Busy_local_with_linkin;
  m_Busy_current[1] = m_Busy_local_with_ttlin;
  m_Busy_current[2] = m_Busy_local_with_nimin;
  this->updateBusy();

  status |= m_ltp->BUSY_disable_from_pg();
  status |= m_ltp->BUSY_local_without_deadtime();
  status |= m_ltp->BUSY_lemoout_busy_only();
 
  // trigger type
  status |= m_ltp->TTYPE_eclout_from_local();
  status |= m_ltp->TTYPE_enable_eclout();
  status |= m_ltp->TTYPE_local_from_pg();
  status |= m_ltp->TTYPE_write_regfile0(0x0000);
  status |= m_ltp->TTYPE_write_regfile1(0x0000);
  status |= m_ltp->TTYPE_write_regfile2(0x0000);
  status |= m_ltp->TTYPE_write_regfile3(0x0000);
  status |= m_ltp->TTYPE_reset_fifo();

  // cal req
  status |= m_ltp->CAL_source_pg();
  status |= m_ltp->CAL_enable_testpath();
 
  // counter
  if (a_Trigger == RCDLTPdal::LTPPatgenBypassedConfig::Trigger::L1A) {
    ERS_LOG(sout << "Trigger = L1A");
    status |= m_ltp->TTYPE_fifotrigger_L1A();
  }
  else if (a_Trigger == RCDLTPdal::LTPPatgenBypassedConfig::Trigger::TestTrigger1) {
    ERS_LOG(sout << "Trigger = TestTrigger1");
    status |= m_ltp->TTYPE_fifotrigger_TR1();
  }
  else if (a_Trigger == RCDLTPdal::LTPPatgenBypassedConfig::Trigger::TestTrigger2) {
    ERS_LOG(sout << "Trigger = TestTrigger2");
    status |= m_ltp->TTYPE_fifotrigger_TR2();
  }
  else if (a_Trigger == RCDLTPdal::LTPPatgenBypassedConfig::Trigger::TestTrigger3) {
    ERS_LOG(sout << "Trigger = TestTrigger3");
    status |= m_ltp->TTYPE_fifotrigger_TR3();
  } else {
    ERS_LOG(sout << "unknown Trigger = \"" << a_Trigger << "\"");
  }
  
  ERS_LOG(sout << "m_use_patgen = " << m_use_patgen);
  
  // load patterns
  ERS_LOG(sout << "Reset pattern generator");
  status |= m_ltp->PG_reset();

  if (a_PatGen_runmode != RCDLTPdal::LTPExpertConfig::PatGen_runmode::Fixed_Value) {
    // first try if there is an LTPPatternBase linked
    const RCDLTPdal::LTPPatternBase* pattern = ltpConfig->get_PatGen_pattern();
    if (pattern != nullptr) {
      this->loadPattern(pattern);
    } else { // this else clause should become obsolete in the next tdaq version
      
      ERS_LOG(sout << ">>> OBSOLETE: loading a pattern via the PatGen_pattern attribute is obsolete and will be dropped in the next release! Please use an object of class type LTPPatternBase instead");
      ERS_LOG(sout << "Load pattern from file: \"" << a_PatGen_filename << "\"");
      status |= m_ltp->PG_runmode_load();
      
      int lstatus = m_ltp->PG_load_memory_from_file(&a_PatGen_filename);
      if ( lstatus != 0 ) {
	ostringstream text;
	text << sout << "Could not load pattern to pattern generator memory. PatGen_filename = " << a_PatGen_filename;
	ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
      }
    }
  }
  // copied runmode into member variable, set run mode in connect
  status |= m_ltp->PG_reset();
  status |= m_ltp->PG_set_fixed_value(0x00);

  status |= m_ltp->COUNTER_reset();
  status |= m_ltp->TTYPE_reset_fifo();

  if (status != 0) {
    ostringstream text;
    text << sout << "VME error, status="<< status << "(0x" << std::hex << status << std::dec << ")";
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
  }

  ERS_LOG(sout << "Done");
}  


// ----------------------------------------------------------------------------------
void  LTPModule::configureLemoBypassed(const RCDLTPdal::LTPLemoBypassedConfig* ltpConfig) {
  std::string sout = m_UID + "@LTPModule::configureLemoBypassed() ";
  ERS_LOG(sout << "Entered");

  // dump configuration object
  ERS_LOG(sout << "called with LTPLemoBypassedConfig = " << ltpConfig);
  ltpConfig->print(1,true,std::cout);

  m_use_patgen = false;
  //   m_ecr_at_resume = true;

  // configuration attributes
  std::string a_Trigger = ltpConfig->get_Trigger();
  ERS_LOG(sout << "Trigger = " << a_Trigger);
  u_short a_TriggerType = 0xff && ltpConfig->get_TriggerType();
  ERS_LOG(sout << "TriggerType = 0x" << std::hex <<  a_TriggerType << std::dec << " (" << a_TriggerType << ")");

  //   <attribute name="Trigger" type="enum" range="L1A,TestTrigger1,TestTrigger2,TestTrigger3" init-value="L1A" is-not-null="yes"/>
  //   <attribute name="TriggerType" description="trigger type (8-bit) to be sent for each trigger" type="u8" format="hex" init-value="0"/>

  int status = 0;

  // bc
  status |= m_ltp->BC_local_intern();
  status |= m_ltp->BC_lemoout_from_local();

  // orbit
  status |= m_ltp->ORB_local_intern();
  status |= m_ltp->ORB_lemoout_from_local();

  // link out from local for all signals
  status |= m_ltp->IO_lemoout_from_local(RCD::LTP::L1A);
  status |= m_ltp->IO_local_lemo_pfn(RCD::LTP::L1A);
  status |= m_ltp->IO_enable_busy_gating(RCD::LTP::L1A);
  status |= m_ltp->IO_disable_inhibit_gating(RCD::LTP::L1A);
  status |= m_ltp->IO_disable_pg_gating(RCD::LTP::L1A);
  status |= m_ltp->IO_pfn_width_1bc(RCD::LTP::L1A);

  status |= m_ltp->IO_lemoout_from_local(RCD::LTP::TR1);
  status |= m_ltp->IO_local_lemo_pfn(RCD::LTP::TR1);
  status |= m_ltp->IO_enable_busy_gating(RCD::LTP::TR1);
  status |= m_ltp->IO_disable_inhibit_gating(RCD::LTP::TR1);
  status |= m_ltp->IO_disable_pg_gating(RCD::LTP::TR1);
  status |= m_ltp->IO_pfn_width_1bc(RCD::LTP::TR1);

  status |= m_ltp->IO_lemoout_from_local(RCD::LTP::TR2);
  status |= m_ltp->IO_local_lemo_pfn(RCD::LTP::TR2);
  status |= m_ltp->IO_enable_busy_gating(RCD::LTP::TR2);
  status |= m_ltp->IO_disable_inhibit_gating(RCD::LTP::TR2);
  status |= m_ltp->IO_disable_pg_gating(RCD::LTP::TR2);
  status |= m_ltp->IO_pfn_width_1bc(RCD::LTP::TR2);

  status |= m_ltp->IO_lemoout_from_local(RCD::LTP::TR3);
  status |= m_ltp->IO_local_lemo_pfn(RCD::LTP::TR3);
  status |= m_ltp->IO_enable_busy_gating(RCD::LTP::TR3);
  status |= m_ltp->IO_disable_inhibit_gating(RCD::LTP::TR3);
  status |= m_ltp->IO_disable_pg_gating(RCD::LTP::TR3);
  status |= m_ltp->IO_pfn_width_1bc(RCD::LTP::TR3);

  status |= m_ltp->IO_lemoout_from_local(RCD::LTP::BG0);
  status |= m_ltp->IO_local_lemo_pfn(RCD::LTP::BG0);
  status |= m_ltp->IO_disable_busy_gating(RCD::LTP::BG0);
  status |= m_ltp->IO_disable_inhibit_gating(RCD::LTP::BG0);
  status |= m_ltp->IO_disable_pg_gating(RCD::LTP::BG0);
  status |= m_ltp->IO_pfn_width_1bc(RCD::LTP::BG0);

  status |= m_ltp->IO_lemoout_from_local(RCD::LTP::BG1);
  status |= m_ltp->IO_local_lemo_pfn(RCD::LTP::BG1);
  status |= m_ltp->IO_disable_busy_gating(RCD::LTP::BG1);
  status |= m_ltp->IO_disable_inhibit_gating(RCD::LTP::BG1);
  status |= m_ltp->IO_disable_pg_gating(RCD::LTP::BG1);
  status |= m_ltp->IO_pfn_width_1bc(RCD::LTP::BG1);

  status |= m_ltp->IO_lemoout_from_local(RCD::LTP::BG2);
  status |= m_ltp->IO_local_lemo_pfn(RCD::LTP::BG2);
  status |= m_ltp->IO_disable_busy_gating(RCD::LTP::BG2);
  status |= m_ltp->IO_disable_inhibit_gating(RCD::LTP::BG2);
  status |= m_ltp->IO_disable_pg_gating(RCD::LTP::BG2);
  status |= m_ltp->IO_pfn_width_1bc(RCD::LTP::BG2);

  status |= m_ltp->IO_lemoout_from_local(RCD::LTP::BG3);
  status |= m_ltp->IO_local_lemo_pfn(RCD::LTP::BG3);
  status |= m_ltp->IO_disable_busy_gating(RCD::LTP::BG3);
  status |= m_ltp->IO_disable_inhibit_gating(RCD::LTP::BG3);
  status |= m_ltp->IO_disable_pg_gating(RCD::LTP::BG3);
  status |= m_ltp->IO_pfn_width_1bc(RCD::LTP::BG3);

  // busy

  // configure busy mask according to values obtained from database during setup()
  m_Busy_current[0] = m_Busy_local_with_linkin;
  m_Busy_current[1] = m_Busy_local_with_ttlin;
  m_Busy_current[2] = m_Busy_local_with_nimin;
  this->updateBusy();

  status |= m_ltp->BUSY_disable_from_pg();
  // add deadtime of 
  status |= m_ltp->BUSY_local_with_deadtime();
  status |= m_ltp->BUSY_deadtime_duration(1); // corresponds to 5BC of deadtime
  status |= m_ltp->BUSY_lemoout_busy_only();

  // trigger type
  status |= m_ltp->TTYPE_eclout_from_local();
  status |= m_ltp->TTYPE_enable_eclout();
  status |= m_ltp->TTYPE_local_from_vme();
  status |= m_ltp->TTYPE_regfile_address_0();
  status |= m_ltp->TTYPE_write_regfile0(a_TriggerType);
  status |= m_ltp->TTYPE_write_regfile1(0x0000);
  status |= m_ltp->TTYPE_write_regfile2(0x0000);
  status |= m_ltp->TTYPE_write_regfile3(0x0000);
  status |= m_ltp->TTYPE_fifodelay_2bc();
  status |= m_ltp->TTYPE_reset_fifo();

  // cal req
  status |= m_ltp->CAL_source_link();
  status |= m_ltp->CAL_enable_testpath();
 
  // counter
  if (a_Trigger == RCDLTPdal::LTPLemoBypassedConfig::Trigger::L1A) {
    ERS_LOG(sout << "Trigger = L1A");
    status |= m_ltp->TTYPE_fifotrigger_L1A();
    status |= m_ltp->BUSY_deadtime_source_L1A();
  }
  else if (a_Trigger == RCDLTPdal::LTPLemoBypassedConfig::Trigger::TestTrigger1) {
    ERS_LOG(sout << "Trigger = TestTrigger1");
    status |= m_ltp->TTYPE_fifotrigger_TR1();
    status |= m_ltp->BUSY_deadtime_source_TR1();
  }
  else if (a_Trigger == RCDLTPdal::LTPLemoBypassedConfig::Trigger::TestTrigger2) {
    ERS_LOG(sout << "Trigger = TestTrigger2");
    status |= m_ltp->TTYPE_fifotrigger_TR2();
    status |= m_ltp->BUSY_deadtime_source_TR2();
  }
  else if (a_Trigger == RCDLTPdal::LTPLemoBypassedConfig::Trigger::TestTrigger3) {
    ERS_LOG(sout << "Trigger = TestTrigger3");
    status |= m_ltp->TTYPE_fifotrigger_TR3();
    status |= m_ltp->BUSY_deadtime_source_TR3();
  } else {
    ostringstream text;
    text << sout << "unknown Trigger = \"" << a_Trigger << "\"";
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
  }
  
  status |= m_ltp->COUNTER_reset();

  // pattern generator
  status |= m_ltp->PG_reset();
  status |= m_ltp->PG_runmode_notused();

  if (status != 0) {
    ostringstream text;
    text << sout << "VME error, status="<< status << "(0x" << std::hex << status << std::dec << ")";
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
  }

  ERS_LOG(sout << "Done");
}  

// ----------------------------------------------------------------------------------
void LTPModule::loadPattern(const RCDLTPdal::LTPPatternBase* pattern) {
  std::string sout = m_UID + "@LTPModule::loadPattern() ";

  int status = 0;

  if (pattern != nullptr) {

    if (const RCDLTPdal::LTPPattern* p = m_confDB->cast<RCDLTPdal::LTPPattern>(pattern)) {
      ERS_LOG(sout << "Identified " << pattern->UID() << "@LTPPatternBase as LTPPattern");

      std::vector<std::string> patternvector = p->get_Pattern();
      int lstatus = m_ltp->PG_load_memory(patternvector);
      if ( lstatus != 0 ) {
	ostringstream text;
	text << sout << "Could not load pattern vector to pattern generator memory. size = " << patternvector.size();
	ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
      }
      
    } else if (const RCDLTPdal::LTPDeadtimePattern* p = m_confDB->cast<RCDLTPdal::LTPDeadtimePattern>(pattern)) {
      ERS_LOG(sout << "Identified " << pattern->UID() << "@LTPPatternBase as LTPDeadtimePattern");

      uint16_t sdt = p->get_SimpleDeadtime();
      ERS_LOG(sout << p->UID() << "@LTPDeadtimePattern has parameter SimpleDeadtime = \"" << sdt << "\"");

      // #  ICC C     TT     O i
      // # Bnaa aBBB BTT     r c
      // # uhll lGGG GYYT TTLb i
      // # siRR Rooo oPPT TT1i t
      // # yb21 0321 0103 21At y
      // #
      // # 0000 0010 0000 0000 1

      // define pattern of length simple deadtime
      std::vector<std::string> v;
      // use BGo2 as deadtime output
      std::string s = "0000 0010 0000 0000 1";
      // create gap according to l1a_delay
      for (u_int i = 0; i < sdt; ++i) v.push_back(s);
      
      int lstatus = m_ltp->PG_load_memory(v);
      if ( lstatus != 0 ) {
	ostringstream text;
	text << sout << "Could not load pattern vector to pattern generator memory. size = " << v.size();
	ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
      }

    } else if (const RCDLTPdal::LTPPatternFile* p = m_confDB->cast<RCDLTPdal::LTPPatternFile>(pattern)) {
      ERS_LOG(sout << "Identified " << pattern->UID() << "@LTPPatternBase as LTPPatternFile");
      string filename = p->get_Filename();
      ERS_LOG(sout << p->UID() << "@LTPPatternFile has parameter Filename = \"" << filename << "\"");

      status |= m_ltp->PG_runmode_load();
      int lstatus = m_ltp->PG_load_memory_from_file(&filename);
      if ( lstatus != 0 ) {
	ostringstream text;
	text << sout << "Could not load pattern to pattern generator memory. filename = " << filename;
	ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
      }

    } else {
      // error
      ostringstream text;
      text << "Could not identify " << pattern->UID() << "@LTPPatternBase as know class type";
      ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
    }

  } else {
    // error
    ostringstream text;
    text << sout << "pattern = nullptr";
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
  }

}

// ----------------------------------------------------------------------------------
daq::trigger::HoldTriggerInfo LTPModule::hold(const std::string& dm) {
  string sout = m_UID + "@LTPModule::hold() ";
  ERS_LOG(sout << "called with argument \"" << dm << "\"");

  m_in_running_state = false;

  m_masterbusy->dump();
  ERS_LOG(sout << "Setting Run Control Busy");
  m_masterbusy->setRunControlBusy();
  m_masterbusy->dump();

  uint32_t necr(0);
  if (m_useECR) {
    m_masterecr->stopAutomaticECR();
    while (m_masterecr->ECRInProgress());
    
    m_masterecr->publishToIS();

    necr = m_masterecr->numberOfECRs();    
    ERS_LOG(sout << "Number of ECR generated = 0x" << std::hex << necr << std::dec << " = " << necr);
  }
  
  m_masterecr->pollL1ID();
  uint32_t l1id, lbn;
  m_masterecr->getLatestL1ID(l1id, lbn); 
  ERS_LOG(sout << " last L1ID = 0x" << std::hex << l1id << std::dec);

  daq::trigger::HoldTriggerInfo trg_info;
  trg_info.ecrCounter = necr;
  trg_info.lastValidEL1ID = l1id;

  return trg_info;
}

// ----------------------------------------------------------------------------------
void LTPModule::resume(const std::string& dm) {
  std::string sout = m_UID + "@LTPModule::resume() ";
  ERS_LOG(sout << "called with argument \"" << dm << "\"");
  
  // read counter and log value
  u_int cntValue = 0;
  m_ltp->COUNTER_read_32(&cntValue);
  ERS_LOG(sout << " counter value = "<<cntValue);
  
  if (m_useECR) {
    // stop automatic ECR generation before issuing one ECR by hand
    m_masterecr->stopAutomaticECR();
    while (m_masterecr->ECRInProgress());
    ERS_LOG(sout << " bevore issuing an ECR we sleep for 1 sec, just to make sure everybody is ready!");
    sleep(1);
    if (m_ecr_at_resume) {
      m_masterecr->issueECR();
      while (m_masterecr->ECRInProgress());
    }
  }
  ERS_LOG(sout << "unsetting RunControl Busy");
  m_masterbusy->unsetRunControlBusy();

  if (m_useECR && m_ecr_periodic) {
    m_masterecr->startAutomaticECR();
  }

  m_in_running_state = true;
  
  m_masterbusy->dump();
  ERS_LOG(sout << "Done");
  return;
}

// ----------------------------------------------------------------------------------
void LTPModule::setL1Prescales(uint32_t l1p) {
  string sout = m_UID + "@LTPModule::setL1Prescales() ";
  ERS_LOG(sout << "called with l1p = " << l1p << ". Nothing to be done for LTP.");
  
  return;
}

// ----------------------------------------------------------------------------------
void LTPModule::setHLTPrescales(uint32_t hltp) {
  string sout = m_UID + "@LTPModule::setHLTPrescales() ";
  ERS_LOG(sout << "called with hltp = " << hltp << ". Nothing to be done for LTP.");
  return;
}

// ----------------------------------------------------------------------------------
void LTPModule::setPrescales(uint32_t l1p, uint32_t hltp) {
  string sout = m_UID + "@LTPModule::setPrescales() ";
  ERS_LOG(sout << "called with l1p = " << l1p << ", hltp = " << hltp << ". Nothing to be done for LTP.");
  return;
}


// ----------------------------------------------------------------------------------
void LTPModule::increaseLumiBlock(uint32_t runno) {
  string sout = m_UID + "@LTPModule::increaseLumiBlock() ";
  ERS_LOG(sout << "called with run number = " << runno);

  try {
    m_masterlumiblock->requestIncreaseLBN();
  } catch(LTPModuleFailedIncreaseLBN& ex) {
    ERS_LOG(sout << "Could not increaseLumiBlock. Throwing exception ::TRIGGER::CommandExecutionFailed");
    throw ::TRIGGER::CommandExecutionFailed(ex.what());
  }

  ERS_LOG(sout << "done");
  return;
}

// ----------------------------------------------------------------------------------
void LTPModule::setBunchGroup(uint32_t bg) {
  string sout = m_UID + "@LTPModule::setBunchGroup() ";
  ERS_LOG(sout << "called with bg = " << bg << ". Nothing to be done for LTP.");

  return;
}

// ----------------------------------------------------------------------------------
void LTPModule::setConditionsUpdate(uint32_t folderIndex, uint32_t lb) {
  string sout = m_UID + "@LTPModule::setConditionsUpdate() ";
  ERS_LOG(sout << "called with folderIndex = " << folderIndex << ", lb = " << lb << ". Nothing to be done for LTP.");
  return;
}


// ----------------------------------------------------------------------------------
void LTPModule::setLumiBlockInterval(uint32_t seconds) {
  string sout = m_UID + "@LTPModule::setLumiBlockInterval() ";
  ERS_LOG(sout << "called with seconds = = " << seconds);
  
  m_masterlumiblock->setPeriod(seconds*1000);

  return;
}


// ----------------------------------------------------------------------------------
void LTPModule::setMinLumiBlockLength(uint32_t seconds) {
  string sout = m_UID + "@LTPModule::setMinLumiBlockLength() ";
  ERS_LOG(sout << "called with seconds = = " << seconds);
  
  m_masterlumiblock->setMinimumDistance(seconds*1000);
  
  return;
}

// ----------------------------------------------------------------------------------
void LTPModule::updateBusy() {
  string sout = m_UID + "@LTPModule::updateBusy() ";
  ERS_LOG(sout << "ENTERED");
  int status = 0;
  int i = 0;
  for (vector<bool>::const_iterator it = m_Busy_current.begin();
       it != m_Busy_current.end();
       ++it) {
    if (m_Busy_current[i]) {
      if (i==0) {
	ERS_LOG(sout << "Enabling busy for linkin (\"" << m_Busy_uid[0] << "\")");
	status |= m_ltp->BUSY_local_with_linkin();
      } else if (i==1) {
	ERS_LOG(sout << "Enabling busy for ttlin (\"" << m_Busy_uid[1] << "\")");
	status |= m_ltp->BUSY_local_with_ttlin();
      } else if (i==2) {
	ERS_LOG(sout << "Enabling busy for nimin (\"" << m_Busy_uid[2] << "\")");
	status |= m_ltp->BUSY_local_with_nimin();
      }
    } else {
      if (i==0) {
	ERS_LOG(sout << "Disabling busy for linkin (\"" << m_Busy_uid[0] << "\")");
	status |= m_ltp->BUSY_local_without_linkin();
      } else if (i==1) {
	ERS_LOG(sout << "Disabling busy for ttlin (\"" << m_Busy_uid[1] << "\")");
	status |= m_ltp->BUSY_local_without_ttlin();
      } else if (i==2) {
	ERS_LOG(sout << "Disabling busy for nimin (\"" << m_Busy_uid[2] << "\")");
	status |= m_ltp->BUSY_local_without_nimin();
      } 
    }
    ++i;
  }
  ERS_LOG(sout << "RETURN");
}

// ----------------------------------------------------------------------------------
void LTPModule::disableBusy(string& argument) {
  string sout = m_UID + "@LTPModule::disableBusy() ";
  ERS_LOG(sout << "called with argument = \"" << argument << "\"");

  // arg is a comma-separated list of busy channels
  std::vector<std::string> busychannels = this->parseCommaDeliminatedStrings(argument);
  
  // loop over the list and disable the corresponding busy channels
  for (std::vector<std::string>::const_iterator it1 = busychannels.begin();
       it1 != busychannels.end();
       ++it1) {
    // find corresponding busy channel
    int ichannel = 0;
    for (std::vector<string>::const_iterator it2 = m_Busy_uid.begin();
	 it2 != m_Busy_uid.end();
	 ++it2, ++ichannel) {
      if (*it1 == *it2) {
	// found matching names
	ERS_LOG(sout << "- found and disabling matching channel " << *it1);
	m_Busy_current[ichannel] = false;
      }
    }
  }

  this->updateBusy();
}

// ----------------------------------------------------------------------------------
void LTPModule::enableBusy(string& argument) {
  string sout = m_UID + "@LTPModule::disableBusy() ";
  ERS_LOG(sout << "called with argument = \"" << argument << "\"");


  // arg is a comma-separated list of busy channels
  std::vector<std::string> busychannels = this->parseCommaDeliminatedStrings(argument);
  
  // loop over the list and disable the corresponding busy channels
  for (std::vector<std::string>::const_iterator it1 = busychannels.begin();
       it1 != busychannels.end();
       ++it1) {
    // find corresponding busy channel
    int ichannel = 0;
    for (std::vector<string>::const_iterator it2 = m_Busy_uid.begin();
	 it2 != m_Busy_uid.end();
	 ++it2, ++ichannel) {
      if (*it1 == *it2) {
	// found matching names
	ERS_LOG(sout << "- found and enabling matching channel " << *it1);
	m_Busy_current[ichannel] = true;
      }
    }
  }
  this->updateBusy();
}

// ----------------------------------------------------------------------------------
std::vector<std::string> LTPModule::parseCommaDeliminatedStrings(std::string s) {
  std::vector<std::string> helper;
  std::string temp;

  while (s.find(",", 0) != std::string::npos)
    {
      size_t  pos = s.find(",", 0); 
      temp = s.substr(0, pos);      
      s.erase(0, pos + 1);
      helper.push_back(temp);         
    }
  helper.push_back(s);
  return helper;
}

extern "C" 
{
  extern ReadoutModule *createLTPModule ();
}

ReadoutModule *createLTPModule ()
{
  return (new LTPModule ());
}

