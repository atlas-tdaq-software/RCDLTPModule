#include <unistd.h>
#include <ctype.h>
#include <stdlib.h>
#include <iostream>

#include "cmdl/cmdargs.h"

#include "RCDLtp/RCDLtp.h"

using namespace RCD;

void print_verbose(std::string text, bool status) {
  std::cout << ">>> " << text.c_str() << " ";
  for (int i = 50-text.length(); i >=0; --i) {
    std::cout << ".";
  }
  std::cout << " ";
  if (status) {
    std::cout << "[ OK ]";
  }
  else {
    std::cout << "[FAIL]";
  }
  std::cout << std::endl;
}

int main(int argc, char ** argv)
{
  // Declare arguments
  CmdArgBool verbose_cmd ('v', "verbose", "Turn on verbose mode", CmdArg::isOPT);
  CmdArgInt address_cmd('a', "base-address", "vme-base-address", "VME base address", CmdArg::isREQ);
  CmdArgBool endofdaisychain_cmd('e', "end-of-daisychain", "LTP is at the end of a daisy-chain", CmdArg::isOPT);

  // Declare command object and its argument-iterator
  CmdLine cmd(*argv, &verbose_cmd, &address_cmd, &endofdaisychain_cmd, nullptr);
  CmdArgvIter arg_iter(--argc, ++argv);
 
  cmd.description( "This program checks that the LTP with vme base address (option -a) is set to slave mode.");
    
  // Parse arguments
  cmd.parse(arg_iter);
  bool verbose(verbose_cmd);
  int address(address_cmd);
  bool endofdaisychain(endofdaisychain_cmd);

  if (verbose) {
    std::cout << "VME base address = 0x" << std::hex << address << std::dec << std::endl;
  }

  int ret = 0;
  
  // create an LTP object
  LTP* ltp = new LTP();
  int ltp_open =  ltp->Open(address);

  bool status = false;
  status = ltp->CheckCERNID(); if (!status) ret = 1;
  if (verbose) print_verbose("CERNID",status);

  if (ret==1 || !status) {
    if (verbose) {
      std::cout << ">>> Cound not open VME at address = 0x" << std::hex << address << std::dec << std::endl;
    }
    exit (-1);
  }


  // check BC
  if (!endofdaisychain) {
    status = ltp->BC_linkout_is_from_linkin(); if (!status) ret |= 2;
    if (verbose) print_verbose("BC linkout is from linkin",status);
  }

  status = ltp->BC_lemoout_is_from_linkin(); if (!status) ret |= 2;
  if (verbose) print_verbose("BC lemoout is from linkin",status);
  status = ltp->BC_selected_is_running   (); if (!status) ret |= 2;
  if (verbose) print_verbose("BC selected clock is running",status);

  // check Orbit
  if (!endofdaisychain) {
    status = ltp->ORB_linkout_is_from_linkin(); if (!status) ret |= 4;
  if (verbose) print_verbose("Orbit linkout is from linkin",status);
  }
  status = ltp->ORB_lemoout_is_from_linkin(); if (!status) ret |= 4;
  if (verbose) print_verbose("Orbit lemoout is from linkin",status);
  status = ltp->ORB_selected_is_running(); if (!status) ret |= 4;
  if (verbose) print_verbose("Orbit selected input is running",status);

  // check L1A
  if (!endofdaisychain) {
    status = ltp->IO_linkout_is_from_linkin(LTP::L1A); if (!status) ret |= 8;
  if (verbose) print_verbose("L1A linkout is from linkin",status);
  }
  status = ltp->IO_lemoout_is_from_linkin(LTP::L1A); if (!status) ret |= 8;
  if (verbose) print_verbose("L1A lemoout is from linkin",status);
  status = ltp->IO_busy_gating_is_enabled(LTP::L1A); if (status) ret |= 8;
  if (verbose) print_verbose("L1A busy gating disabled",!status);
  status = ltp->IO_inhibit_timer_gating_is_enabled(LTP::L1A); if (status) ret |= 8;
  if (verbose) print_verbose("L1A inhibit timer gating disabled",!status);
  status = ltp->IO_pg_gating_is_enabled  (LTP::L1A); if (status) ret |= 8;
  if (verbose) print_verbose("L1A patgen gating disabled",!status);
  
  // check busy
  status = ltp->BUSY_linkout_is_from_local        (); if (!status) ret |= 16;
  if (verbose) print_verbose("BUSY linkout from local",status);

  if (!endofdaisychain) {
    status = ltp->BUSY_linkin_is_summed_with_local  (); if (!status) ret |= 16;
    if (verbose) print_verbose("BUSY sum local with linkin",status);
  }
//   status = ltp->BUSY_ttlin_is_summed_with_local   (); if (!status) ret |= 16;
//   status = ltp->BUSY_nimin_is_summed_with_local   (); if (!status) ret |= 16;
  
//   status = ltp->BUSY_deadtime_is_summed_with_local(); if (status) ret |= 16;
//   if (verbose) print_verbose("BUSY do not use deadtime",!status);
  status = ltp->BUSY_constant_level_is_asserted   (); if (status) ret |= 16;
  if (verbose) print_verbose("BUSY constant level disabled",!status);
//   status = ltp->BUSY_from_pg_is_enabled           (); if (status) ret |= 16;
//   if (verbose) print_verbose("BUSY",status);

  if (verbose) {
    std::cout << std::endl;
  }
  status = (ret==0);
  if (verbose) print_verbose("SUMMARY",status);

  return ret;
}


