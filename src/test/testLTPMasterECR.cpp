#include <unistd.h>
#include <ctype.h>
#include <stdlib.h>
#include <iostream>
#include <time.h>

#include "cmdl/cmdargs.h"

#include "RCDLtp/RCDLtp.h"

#include "RCDLTPModule/LTPMasterBusy.h"
#include "RCDLTPModule/LTPMasterECR.h"

#include "ipc/core.h"

using namespace RCD;
using namespace std;



void check_for_l1id_consistency(std::vector<uint32_t> l1id_list, std::vector<uint32_t> lbn_list) {
  size_t size_l1id = l1id_list.size();
  size_t size_lbn  = lbn_list.size();

  if (size_l1id != size_lbn) {
    printf("ERROR: size l1id_list %d  !=  size lbn_list %d\n", static_cast<int>(size_l1id), static_cast<int>(size_lbn)); 
    return;
  }
  for (size_t i = 0; i<size_l1id; ++i) {
    printf("LBN  %5d  L1ID 0x%08x\n", lbn_list[i], l1id_list[i]);
  }

  return;
}

int main(int argc, char ** argv)
{
  try {
    IPCCore::init( argc, argv );
  } catch(daq::ipc::CannotInitialize& ex){
    ers::fatal(ex);
    abort();
  } catch(daq::ipc::AlreadyInitialized& ex){
    ers::warning(ex);
  } catch( daq::ipc::Exception & ex ) {
    ers::fatal(ex);
  }


  // Declare arguments
  CmdArgBool verbose_cmd ('v', "verbose", "Turn on verbose mode", CmdArg::isOPT);
  CmdArgInt address_cmd('a', "base-address", "vme-base-address", "VME base address", CmdArg::isREQ);
  CmdArgStr partition_cmd('p', "partition", "partition", "name of ipc partition", CmdArg::isREQ);

  // Declare command object and its argument-iterator
  CmdLine cmd(*argv, &verbose_cmd, &address_cmd, &partition_cmd, nullptr);
  CmdArgvIter arg_iter(--argc, ++argv);
 
  cmd.description( "This program tests the LTPMasterECR class.");
    
  // Parse arguments
  cmd.parse(arg_iter);
  bool verbose(verbose_cmd);
  int address(address_cmd);
  string partition(partition_cmd);

  IPCPartition p(partition);

  if (verbose) {
    std::cout << "VME base address = 0x" << std::hex << address << std::dec << std::endl;
  }

  int ret = 0;
  
  // create an LTP object
  LTP* ltp = new LTP();
  ltp->Open(address);

  bool status = false;
  status = ltp->CheckCERNID(); if (!status) ret = 1;

  cout << "Creating MasterBusy" << endl;
  LTPMasterBusy masterbusy(*ltp, p);
  cout << "Creating MasterECR" << endl;
  LTPMasterECR masterecr(500, *ltp, masterbusy, p);

  // set automatic mode
  // set to 1ms deadtime before and after the ECR
  // set period to 0.5sec
  cout << "MasterECR: setting period to 500" << endl;
  cout << "MasterECR: setting deadtime to 40000, 40000" << endl;
  masterecr.setDeadtime(40000,40000);

  srand (time(nullptr));

  while(true) {
    masterecr.pollL1ID();
    // wait for a random time between 0 and 1.5 seconds
    u_int ret = ts_open(1, TS_DUMMY);
    if (ret) rcc_error_print(stdout, ret);
    u_int time_to_wait_musec = rand() % 1500000;
    ret = ts_delay(time_to_wait_musec);
    if (ret) rcc_error_print(stdout, ret);

    // issue an ecr by hand
    masterecr.pollL1ID();
    masterecr.issueECR();
    masterecr.pollL1ID();

    // read out L1ID and have it check for consistency
    std::vector<uint32_t> l1id_list, lbn_list;
    masterecr.pollL1ID();
    masterecr.getL1IDList(l1id_list, lbn_list);
    masterecr.pollL1ID();
    check_for_l1id_consistency(l1id_list, lbn_list);
    masterecr.pollL1ID();
  }
  
  return ret;
}


