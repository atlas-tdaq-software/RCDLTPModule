#include <unistd.h>
#include <ctype.h>
#include <stdlib.h>
#include <iostream>

#include "cmdl/cmdargs.h"

#include "RCDLtp/RCDLtp.h"

#include "RCDLTPModule/LTPMasterBusy.h"

#include "ipc/core.h"

using namespace RCD;
using namespace std;

void testBusyPattern(LTPMasterBusy& masterbusy, bool t, bool r, bool l, bool e, bool h, bool s) 
{
  t ? masterbusy.setTriggerBusy()    : masterbusy.unsetTriggerBusy();
  r ? masterbusy.setRunControlBusy() : masterbusy.unsetRunControlBusy();
  l ? masterbusy.setLumiBlockBusy()  : masterbusy.unsetLumiBlockBusy();
  e ? masterbusy.setECRBusy()        : masterbusy.unsetECRBusy();
  h ? masterbusy.setHLTCounterBusy() : masterbusy.unsetHLTCounterBusy();
  h ? masterbusy.setHLTSVBusy()      : masterbusy.unsetHLTSVBusy();

  // check expectations
  bool errors(false);

  bool t_r(false), r_r(false), l_r(false), e_r(false), h_r(false), s_r;
  bool global_e = t || r || l || e || h || s;
  bool global_ltp = masterbusy.ltpConstantBusyAsserted();
  bool global1_r = masterbusy.isBusy();
  uint32_t rc(0);
  bool global2_r = masterbusy.isBusy(t_r,rc,l_r,e_r,h_r,s_r);
  r_r = (rc > 0)? true : false;
  if (global_e != global1_r) {
    cout << "ERROR: testBusyPattern("<<t<<","<<r<<","<<l<<","<<e<<","<<h<<","<<s<<") global1 failed" << endl;
    errors=true;
  } 
  if (global_e != global2_r) {
    cout << "ERROR: testBusyPattern("<<t<<","<<r<<","<<l<<","<<e<<","<<h<<","<<s<<") global2 failed" << endl;
    errors=true;
  } 
  if (global_e != global_ltp) {
    cout << "ERROR: testBusyPattern("<<t<<","<<r<<","<<l<<","<<e<<","<<h<<","<<s<<") global_ltp failed" << endl;
    errors=true;
  } 
  if (t_r != t) {
    cout << "ERROR: testBusyPattern("<<t<<","<<r<<","<<l<<","<<e<<","<<h<<","<<s<<") TriggerBusy failed" << endl;
    errors=true;
  } 
  if (r_r != r) {
    cout << "ERROR: testBusyPattern("<<t<<","<<r<<","<<l<<","<<e<<","<<h<<","<<s<<") RunControlBusy failed" << endl;
    errors=true;
  } 
  if (l_r != l) {
    cout << "ERROR: testBusyPattern("<<t<<","<<r<<","<<l<<","<<e<<","<<h<<","<<s<<") LumiBlockBusy failed" << endl;
    errors=true;
  } 
  if (e_r != e) {
    cout << "ERROR: testBusyPattern("<<t<<","<<r<<","<<l<<","<<e<<","<<h<<","<<s<<") ECRBusy failed" << endl;
    errors=true;
  } 
  if (h_r != h) {
    cout << "ERROR: testBusyPattern("<<t<<","<<r<<","<<l<<","<<e<<","<<h<<","<<s<<") HLTCounterBusy failed" << endl;
    errors=true;
  } 
  if (s_r != s) {
    cout << "ERROR: testBusyPattern("<<t<<","<<r<<","<<l<<","<<e<<","<<h<<","<<s<<") HLTSVBusy failed" << endl;
    errors=true;
  } 
  if (!errors) {
    cout << "SUCCESS: testBusyPattern("<<t<<","<<r<<","<<l<<","<<e<<","<<h<<","<<s<<")" << endl;
  }
  return;
}

int main(int argc, char ** argv)
{
  try {
    IPCCore::init( argc, argv );
  } catch(daq::ipc::CannotInitialize& ex){
    ers::fatal(ex);
    abort();
  } catch(daq::ipc::AlreadyInitialized& ex){
    ers::warning(ex);
  } catch( daq::ipc::Exception & ex ) {
    ers::fatal(ex);
  }

  // Declare arguments
  CmdArgBool verbose_cmd ('v', "verbose", "Turn on verbose mode", CmdArg::isOPT);
  CmdArgInt address_cmd('a', "base-address", "vme-base-address", "VME base address", CmdArg::isREQ);
  CmdArgStr partition_cmd('p', "partition", "partition", "name of ipc partition", CmdArg::isREQ);
 
  // Declare command object and its argument-iterator
  CmdLine cmd(*argv, &verbose_cmd, 
	      &address_cmd, 
	      &partition_cmd,
	      nullptr);
  CmdArgvIter arg_iter(--argc, ++argv);
 
  cmd.description( "This program tests the LTPMasterBusy class.");
    
  // Parse arguments
  cmd.parse(arg_iter);
  bool verbose(verbose_cmd);
  int address(address_cmd);
  string partition(partition_cmd);

  if (verbose) {
    std::cout << "VME base address = 0x" << std::hex << address << std::dec << std::endl;
  }

  IPCPartition p(partition);

  int ret = 0;
  
  // create an LTP object
  LTP* ltp = new LTP();
  int ltp_open =  ltp->Open(address);

  bool status = false;
  status = ltp->CheckCERNID(); 
  if (!status) ret = 1;

  LTPMasterBusy masterbusy(*ltp, p);

  testBusyPattern(masterbusy, false, false, false, false, false, false);
  testBusyPattern(masterbusy, false, false, false, false, true , false);
  testBusyPattern(masterbusy, false, false, false, true , false, false);
  testBusyPattern(masterbusy, false, false, false, true , true , false);
  testBusyPattern(masterbusy, false, false, true , false, false, false);
  testBusyPattern(masterbusy, false, false, true , false, true , false);
  testBusyPattern(masterbusy, false, false, true , true , false, false);
  testBusyPattern(masterbusy, false, false, true , true , true , false);
  testBusyPattern(masterbusy, false, true , false, false, false, false);
  testBusyPattern(masterbusy, false, true , false, false, true , false);
  testBusyPattern(masterbusy, false, true , false, true , false, false);
  testBusyPattern(masterbusy, false, true , false, true , true , false);
  testBusyPattern(masterbusy, false, true , true , false, false, false);
  testBusyPattern(masterbusy, false, true , true , false, true , false);
  testBusyPattern(masterbusy, false, true , true , true , false, false);
  testBusyPattern(masterbusy, false, true , true , true , true , false);
  testBusyPattern(masterbusy, true , false, false, false, false, true );
  testBusyPattern(masterbusy, true , false, false, false, true , true );
  testBusyPattern(masterbusy, true , false, false, true , false, true );
  testBusyPattern(masterbusy, true , false, false, true , true , true );
  testBusyPattern(masterbusy, true , false, true , false, false, true );
  testBusyPattern(masterbusy, true , false, true , false, true , true );
  testBusyPattern(masterbusy, true , false, true , true , false, true );
  testBusyPattern(masterbusy, true , false, true , true , true , true );
  testBusyPattern(masterbusy, true , true , false, false, false, true );
  testBusyPattern(masterbusy, true , true , false, false, true , true );
  testBusyPattern(masterbusy, true , true , false, true , false, true );
  testBusyPattern(masterbusy, true , true , false, true , true , true );
  testBusyPattern(masterbusy, true , true , true , false, false, true );
  testBusyPattern(masterbusy, true , true , true , false, true , true );
  testBusyPattern(masterbusy, true , true , true , true , false, true );
  testBusyPattern(masterbusy, true , true , true , true , true , true );

  return ret;
}


