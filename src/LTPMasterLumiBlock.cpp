#include <string>
#include <iostream>
#include <ctime>

#include "RCDLTPModule/Exceptions.h"
#include "RCDLTPModule/LTPMasterLumiBlock.h"
#include "RCDLTPModule/LTPMasterBusy.h"

#include "ers/ers.h"
#include "is/info.h"
#include "is/infodictionary.h"

#include "TTCInfo/LumiBlock.h"
#include "TTCInfo/LBSettings.h"
#include "TTCInfo/LB_IS.h"

LTPMasterLumiBlock::LTPMasterLumiBlock(LTPMasterBusy& mb, LTPMasterTTC2LAN& mt, IPCPartition& p) 
  : ScheduledUserAction(250), // 0.25 sec hardcoded
    m_mb(&mb),
    m_mt(&mt),
    m_ipcpartition(p),
    m_lbchange_in_progress(false),
    m_lb_period(60000000000ull),
    m_lb_mindistance(10000000000ull),
    m_next_lb_update(0),
    m_next_lb_update_is_a_request(false),
    m_veto_lb_update(m_lb_mindistance),
    m_periodic_lumiblocks_enabled(false),
    m_lumiblocks_enabled(false),
    m_current_lb_starttime(0),
    m_lbn(0),
    m_runnumber(0),
    m_nl1a_last(0),
    m_dictname("RunParams.LumiBlock")
{

}

LTPMasterLumiBlock::~LTPMasterLumiBlock() 
{

}


void LTPMasterLumiBlock::requestIncreaseLBN() 
{
  uint64_t now = this->getTimeNow();

  m_mutex.lock();
  // take requests only when lumi blocks are enabled, i.e. during a run
  if (m_lumiblocks_enabled) {
    if (now >= m_veto_lb_update) {
      this->increaseLBN();
      now = this->getTimeNow();
      m_veto_lb_update = now+m_lb_mindistance;
      m_next_lb_update_is_a_request = false;
      m_next_lb_update = now+m_lb_period;
    } else {
      ERS_LOG("Received LB request at " << now << ", waiting until " << m_veto_lb_update);
      m_next_lb_update_is_a_request = true;
      m_next_lb_update=m_veto_lb_update;
    }
  } else {
    std::ostringstream text;
    text << "LTPMasterLumiBlock::requestIncreaseLBN(): failed to increase LBN";
    m_mutex.unlock();
    throw LTP::LTPModule::LTPModuleFailedIncreaseLBN(ERS_HERE, text.str() , "");
  }
  m_mutex.unlock();
}

void LTPMasterLumiBlock::getCurrentLBN(uint32_t& lbn, uint64_t& timestamp)
{
  m_mutex.lock();
  lbn = m_lbn;
  timestamp = m_current_lb_starttime;
  m_mutex.unlock();
}

void LTPMasterLumiBlock::setPeriod(uint32_t period_ms)
{
  std::string sout("LTPMasterLumiBlock::setPeriod() ");
  
  m_mutex.lock();
  m_lb_period = static_cast<uint64_t>(period_ms)*1000000ull;

  // publish settings to IS
  try {
    this->publishLBSettingsToIS();
  } catch (LTP::LTPModule::LTPModulePublishToIS& ex) {
    std::ostringstream text;
    text << sout << "Could not publish to IS: LTPModulePublishToIS ex.what()=" << ex.what();
    ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
  }

  m_mutex.unlock();
}

void LTPMasterLumiBlock::setMinimumDistance(uint32_t distance_ms)
{
  std::string sout("LTPMasterLumiBlock::setMinimumDistance() ");
  m_mutex.lock();
  m_lb_mindistance = static_cast<uint64_t>(distance_ms)*1000000ull;

  // publish settings to IS
  try {
    this->publishLBSettingsToIS();
  } catch (LTP::LTPModule::LTPModulePublishToIS& ex) {
    std::ostringstream text;
    text << sout << "Could not publish to IS: LTPModulePublishToIS ex.what()=" << ex.what();
    ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
  }

  m_mutex.unlock();

}

void LTPMasterLumiBlock::enablePeriodicLumiBlocks()
{
  m_mutex.lock();
  m_periodic_lumiblocks_enabled = true;
  m_mutex.unlock();
}

void LTPMasterLumiBlock::disablePeriodicLumiBlocks()
{
  m_mutex.lock();
  m_periodic_lumiblocks_enabled = false;
  m_mutex.unlock();
}

void LTPMasterLumiBlock::enableLumiBlocks()
{
  m_mutex.lock();
  m_lumiblocks_enabled = true;
  m_mutex.unlock();
}

void LTPMasterLumiBlock::disableLumiBlocks()
{
  m_mutex.lock();
  m_lumiblocks_enabled = false;
  m_mutex.unlock();
}

void LTPMasterLumiBlock::increaseLBN() 
{
  std::string sout("LTPMasterLumiBlock::increaseLBN() ");
  m_lbchange_in_progress = true;

  // record start time for profiling
  uint64_t benchmark_start = this->getTimeNow();

  // set busy
  m_mb->setLumiBlockBusy();

  // wait until the busy is really raised
  while(!m_mb->isLumiBlockBusy()) {}

  //
  // read out whatever needs reading out
  //
  // ask the TTC2LAN about the counter data

  // publish counter data for the previous lumiblock
  // publish settings to IS
  try {
    this->publishCounterDataToIS();
  } catch (LTP::LTPModule::LTPModulePublishToIS& ex) {
    std::ostringstream text;
    text << sout << "Could not publish to IS: LTPModulePublishToIS ex.what()=" << ex.what();
    ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
  }
  
  // increase LBN
  ++m_lbn;
  // record timestamp
  m_current_lb_starttime = this->getTimeNow();
  // send out IS information
  this->publishLumiBlockToIS();

  ERS_LOG("Issuing new lumiblock with RN=" << m_runnumber << ", LBN=" << m_lbn << " and Time=" << m_current_lb_starttime);

  // release busy
  m_mb->unsetLumiBlockBusy();

  // record end time
  uint64_t benchmark_stop = this->getTimeNow();
  // make a log entry on timing performance
  ERS_LOG("Transition Benchmark for RN=" << m_runnumber << ", LBN=" << m_lbn << " time=" << benchmark_stop-benchmark_start << " ns");

  m_lbchange_in_progress = false;
  return;
}


void LTPMasterLumiBlock::reactTo() {
  m_mutex.lock();

  // increase LB only if it was a specific request (and staged because of the veto)
  // or we are in periodic lumiblock mode and it is time
  bool increaseLB = false;
  if (m_periodic_lumiblocks_enabled || m_next_lb_update_is_a_request) {
    uint64_t now = this->getTimeNow();
    if ((now>=m_next_lb_update) && (now>=m_veto_lb_update)) {
      increaseLB = true;
    }
  }

  if (increaseLB) {
    // only increase LBN in case LumiBlocks are enabled. If they are not, then
    // the run is over and it is not a problem
    if (m_lumiblocks_enabled) {
      this->increaseLBN();
    } 
    uint64_t now = this->getTimeNow();
    m_next_lb_update = now+m_lb_period;
    m_next_lb_update_is_a_request = false;
    m_veto_lb_update = now+m_lb_mindistance;
  } else {
    // do nothing
  }

  m_mutex.unlock();
  return;
}

uint64_t LTPMasterLumiBlock::getTimeNow() {
  struct timeval tv;
  gettimeofday(&tv, nullptr);
  return(static_cast<uint64_t>(tv.tv_sec)*1000000000ull + static_cast<uint64_t>(tv.tv_usec*1000));
}

uint32_t LTPMasterLumiBlock::getRunNumber() {
  uint32_t rn(0);
  m_mutex.lock();
  rn = m_runnumber;
  m_mutex.unlock();
  return(rn);
}


void LTPMasterLumiBlock::reset(uint32_t run_number) {
  std::string sout("LTPMasterLumiBlock::reset() ");
  ERS_LOG(sout << "called with argument run_number = " << run_number);
  // The very first lumiblock with LBN=0 is published by the RootController
  // at the start of run. We read this from IS and feed the run number, start time,
  // and startlbn into the masterlumiblock action
  // we also perform a few cross-checks

  // read from IS
  // there is one pitfall here: the RootController publishes without history.
  // if there is one programme which publishes with history once, then
  // we'll always poll this value (with tag>0) rather than the one
  // from the RootController. To safe-guard against this, we read explicitely
  // the tag 0
  LumiBlock is_entry;
  ISInfoDictionary dict(m_ipcpartition);
  try {
    dict.getValue(m_dictname, 0, is_entry);
  } catch (CORBA::SystemException& ex) {
    ERS_LOG(sout << "Could not retrieve RunParams.LumiBlock publication: CORBA::SystemException ex._name()=" << ex._name());
    std::ostringstream text;
    text << sout << "could not retrieve RunParams.LumiBlock publication for LBN=0. ";
    throw LTP::LTPModule::LTPModuleFailedFirstLumiBlock(ERS_HERE, text.str() , ex._name());
  } catch (daq::is::Exception & ex) {
    ERS_LOG(sout << "Could not retrieve RunParams.LumiBlock publication: daq::is::Exception & ex ex._name()=" << ex.what());
    std::ostringstream text;
    text << sout << "could not retrieve RunParams.LumiBlock publication for LBN=0. ";
    throw LTP::LTPModule::LTPModuleFailedFirstLumiBlock(ERS_HERE, text.str(), ex.what());
  } catch (std::exception& ex) { 
    ERS_LOG(sout << "Could not retrieve RunParams.LumiBlock publication: std::exception& ex ex._name()=" << ex.what());
    std::ostringstream text;
    text << sout << "could not retrieve RunParams.LumiBlock publication for LBN=0. ";
    throw LTP::LTPModule::LTPModuleFailedFirstLumiBlock(ERS_HERE, text.str(), ex.what());
  } catch (...) {
    ERS_LOG(sout << "Could not retrieve RunParams.LumiBlock publication: unknown exception");
    std::ostringstream text;
    text << sout << "could not retrieve RunParams.LumiBlock publication for LBN=0. ";
    throw LTP::LTPModule::LTPModuleFailedFirstLumiBlock(ERS_HERE, text.str(), "Unknown Exception");
  }

  // is_entry.RunNumber = m_runnumber;
  uint32_t start_runnumber = is_entry.RunNumber;
  uint32_t start_lbn = is_entry.LumiBlockNumber;
  uint64_t start_of_run_time = is_entry.Time;
  ERS_LOG(sout << "Retrieved from IS (RunParams.LumiBlock): RunNumber = " << start_runnumber);
  ERS_LOG(sout << "Retrieved from IS (RunParams.LumiBlock): LumiBlockNumber = " << start_lbn);
  ERS_LOG(sout << "Retrieved from IS (RunParams.LumiBlock): Time = " << start_of_run_time);


  // take those values as starting point ... 
  m_mutex.lock();
  m_current_lb_starttime = start_of_run_time;
  m_lbchange_in_progress = false;
  m_next_lb_update = start_of_run_time+m_lb_period;
  m_next_lb_update_is_a_request = false;
  m_veto_lb_update = start_of_run_time+m_lb_mindistance;
  m_lbn = start_lbn;
  m_runnumber = start_runnumber;
  m_periodic_lumiblocks_enabled = false;
  m_lumiblocks_enabled = false;
  m_nl1a_last = 0;
  m_mutex.unlock();

  // make a few consistency checks
  if (start_runnumber != run_number) {
    std::ostringstream text;
    text << sout << "Retrieved inconsistent run number from RunParams.LumiBlock. Run number from LBN=0 " << start_runnumber << ", inconsistent with expectation from RunParams " << run_number;
    throw LTP::LTPModule::LTPModuleFailedFirstLumiBlock(ERS_HERE, text.str(), "");
  }
  if (start_lbn != 0 ) {
    std::ostringstream text;
    text << sout << "Retrieved inconsistent start LBN from RunParams.LumiBlock. Got LBN = " << start_lbn << ", inconsistent with expectation 0";
    throw LTP::LTPModule::LTPModuleFailedFirstLumiBlock(ERS_HERE, text.str(), "");
  }
  uint64_t now = getTimeNow();
  // make comparison of time stamps
  int64_t diff = static_cast<int64_t>(start_of_run_time)-static_cast<int64_t>(now);
  ERS_LOG(sout << "Time difference between timestamps SOR minus now = " <<  diff << " nanoseconds");

  return;
}


void LTPMasterLumiBlock::publishLumiBlockToIS() {

  // this method should only be called by other methods locking m_mutex
  // this means also, that this method should not use m_mutex nor call
  // other methods that use m_mutex
  std::string sout("LTPMasterLumiBlock::publishLumiBlockToIS() ");
  try {
    LumiBlock is_entry;
    is_entry.RunNumber = m_runnumber;
    is_entry.LumiBlockNumber = m_lbn;
    is_entry.Time = m_current_lb_starttime;

    ISInfoDictionary dict(m_ipcpartition);
    dict.checkin(m_dictname, is_entry, false);
  } catch (CORBA::SystemException& ex) {
    ERS_LOG(sout << "Could not publish to IS: CORBA::SystemException ex._name()=" << ex._name());

    throw LTP::LTPModule::LTPModulePublishToIS(ERS_HERE, "LTPMasterLumiBlock::publishLumiBlockToIS() ", ex._name());

  } catch (daq::is::Exception & ex) {
    ERS_LOG(sout << "Could not publish to IS: daq::is::Exception ex.what()=" << ex.what());
    throw LTP::LTPModule::LTPModulePublishToIS(ERS_HERE, "LTPMasterLumiBlock::publishLumiBlockToIS() ", ex.what());
  } catch (std::exception& ex) {
    ERS_LOG(sout << "Could not publish to IS: std::exception ex.what()=" << ex.what());
    throw LTP::LTPModule::LTPModulePublishToIS(ERS_HERE, "LTPMasterLumiBlock::publishLumiBlockToIS() ", ex.what());
  } catch (...) {
    ERS_LOG(sout << "Could not publish to IS. Unknown exception. Rethrowing.");
    throw LTP::LTPModule::LTPModulePublishToIS(ERS_HERE, "LTPMasterLumiBlock::publishLumiBlockToIS() ", "Unknown Exception");
  }
  return;
}

void LTPMasterLumiBlock::publishCounterDataToIS() {

  // this method should only be called by other methods locking m_mutex
  // this means also, that this method should not use m_mutex nor call
  // other methods that use m_mutex
  std::string sout("LTPMasterLumiBlock::publishCounterDataToIS() ");
  try {
    LB_IS is_entry;
    is_entry.runnumber =  m_runnumber;
    is_entry.LBN = m_lbn;
    is_entry.Starttime = m_current_lb_starttime;
    
    bool first_l1id_is_set = m_mt->firstL1IDIsSet();
    if (!first_l1id_is_set) {
      is_entry.Start_L1ID = 0xffffffff;
      is_entry.Stop_L1ID = 0xffffffff;
    } else {
      is_entry.Start_L1ID = m_mt->getFirstL1ID();
      is_entry.Stop_L1ID = m_mt->getLastL1ID();
      m_mt->unsetFirstL1ID();
    }
    uint64_t nl1a = m_mt->getNumberOfTriggers();
    is_entry.L1A_counter = static_cast<uint32_t>(nl1a-m_nl1a_last);
    m_nl1a_last = nl1a;

    ISInfoDictionary dict(m_ipcpartition);
    char helper[1000];
    sprintf(helper, "Monitoring.LB_LTP_%d_%05d", m_runnumber, m_lbn);
    dict.checkin(helper, is_entry, true);
  } catch (CORBA::SystemException& ex) {
    ERS_LOG(sout << "Could not publish to IS: CORBA::SystemException ex._name()=" << ex._name());

    throw LTP::LTPModule::LTPModulePublishToIS(ERS_HERE, "LTPMasterLumiBlock::publishCounterDataToIS() ", ex._name());

  } catch (daq::is::Exception & ex) {
    ERS_LOG(sout << "Could not publish to IS: daq::is::Exception ex.what()=" << ex.what());
    throw LTP::LTPModule::LTPModulePublishToIS(ERS_HERE, "LTPMasterLumiBlock::publishCounterDataToIS() ", ex.what());
  } catch (std::exception& ex) {
    ERS_LOG(sout << "Could not publish to IS: std::exception ex.what()=" << ex.what());
    throw LTP::LTPModule::LTPModulePublishToIS(ERS_HERE, "LTPMasterLumiBlock::publishCounterDataToIS() ", ex.what());
  } catch (...) {
    ERS_LOG(sout << "Could not publish to IS. Unknown exception. Rethrowing.");
    throw LTP::LTPModule::LTPModulePublishToIS(ERS_HERE, "LTPMasterLumiBlock::publishCounterDataToIS() ", "Unknown Exception");
  }
  return;
}

void LTPMasterLumiBlock::publishLBSettingsToIS() {

  // this method should only be called by other methods locking m_mutex
  // this means also, that this method should not use m_mutex nor call
  // other methods that use m_mutex
  std::string sout("LTPMasterLumiBlock::publishLBSettingsToIS() ");
  try {
    LBSettings is_entry;
    is_entry.SwitchingMode = LBSettings::TIME;
    is_entry.Interval =  static_cast<u_int>(m_lb_period/1000000000ull);
    is_entry.MinInterval =  static_cast<u_int>(m_lb_mindistance/1000000000ull);

    ISInfoDictionary dict(m_ipcpartition);
    dict.checkin("RunParams.LBSettings", is_entry, true);
  } catch (CORBA::SystemException& ex) {
    ERS_LOG(sout << "Could not publish to IS: CORBA::SystemException ex._name()=" << ex._name());

    throw LTP::LTPModule::LTPModulePublishToIS(ERS_HERE, "LTPMasterLumiBlock::publishLBSettingsToIS() ", ex._name());

  } catch (daq::is::Exception & ex) {
    ERS_LOG(sout << "Could not publish to IS: daq::is::Exception ex.what()=" << ex.what());
    throw LTP::LTPModule::LTPModulePublishToIS(ERS_HERE, "LTPMasterLumiBlock::publishLBSettingsToIS() ", ex.what());
  } catch (std::exception& ex) {
    ERS_LOG(sout << "Could not publish to IS: std::exception ex.what()=" << ex.what());
    throw LTP::LTPModule::LTPModulePublishToIS(ERS_HERE, "LTPMasterLumiBlock::publishLBSettingsToIS() ", ex.what());
  } catch (...) {
    ERS_LOG(sout << "Could not publish to IS. Unknown exception. Rethrowing.");
    throw LTP::LTPModule::LTPModulePublishToIS(ERS_HERE, "LTPMasterLumiBlock::publishLBSettingsToIS() ", "Unknown Exception");
  }

  return;
}
