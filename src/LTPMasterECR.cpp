#include "RCDLtp/RCDLtp.h"
#include "RCDLTPModule/Exceptions.h"
#include "RCDLTPModule/LTPMasterECR.h"
#include "RCDLTPModule/LTPMasterBusy.h"

#include "is/info.h"
#include "is/infodictionary.h"
#include "TTCInfo/ECR.h"

using namespace std;

LTPMasterECR::LTPMasterECR(uint32_t period_ms, 
			   LTPMasterBusy& mb, 
			   IPCPartition& p) 
  : ScheduledUserAction(period_ms), 
    m_mb(&mb),
    m_ltp(nullptr),
    m_ipcpartition(p),
    m_lbn_list(0),
    m_l1id_list(0),
    m_nl1a(0),
    m_last_ec(0),
    m_necr(0),
    m_ecr_in_progress(false),
    m_automatic_mode(false),
    m_pre_ecr_deadtime_bc(0),
    m_post_ecr_deadtime_bc(0),
    m_latest_l1id(0x00ffffff),
    m_lbn(0)
{

}

LTPMasterECR::LTPMasterECR(uint32_t period_ms, 
			   RCD::LTP& ltp, 
			   LTPMasterBusy& mb, 
			   IPCPartition& p) 
  : ScheduledUserAction(period_ms),
    m_mb(&mb),
    m_ltp(&ltp),
    m_ipcpartition(p),
    m_lbn_list(0),
    m_l1id_list(0),
    m_necr(0),
    m_ecr_in_progress(false),
    m_automatic_mode(false),
    m_pre_ecr_deadtime_bc(0),
    m_post_ecr_deadtime_bc(0),
    m_latest_l1id(0x00ffffff),
    m_lbn(0)
{

}

LTPMasterECR::~LTPMasterECR() {

}

void LTPMasterECR::setLTP(RCD::LTP& ltp) {
  m_mutex.lock();
  m_ltp = &ltp;
  m_mutex.unlock();
}


void LTPMasterECR::issueECR() {
  m_mutex.lock();
  m_ecr_in_progress = true;

  u_int vme_ret_code(0);

  // start timer
  u_int ret = ts_open(1, TS_DUMMY);
  if (ret) rcc_error_print(stdout, ret);

  // raise busy
  m_mb->setECRBusy();

  // remember current time
  tstamp t0;
  ret = ts_clock(&t0);
  if (ret) rcc_error_print(stdout, ret);

  // setup ltp bgo-1 channel to VME
  vme_ret_code |= m_ltp->IO_linkout_from_local    (RCD::LTP::BG1);
  vme_ret_code |= m_ltp->IO_lemoout_from_local    (RCD::LTP::BG1);
  vme_ret_code |= m_ltp->IO_local_lemo_pfn        (RCD::LTP::BG1);
  vme_ret_code |= m_ltp->IO_disable_busy_gating   (RCD::LTP::BG1);
  vme_ret_code |= m_ltp->IO_disable_inhibit_gating(RCD::LTP::BG1);
  vme_ret_code |= m_ltp->IO_disable_pg_gating     (RCD::LTP::BG1);
  vme_ret_code |= m_ltp->IO_pfn_width_2bc         (RCD::LTP::BG1);

  // wait pre-ecr deadtime
  // duration in nanoseconds
  float time_so_far;
  tstamp t1;
  ret = ts_clock(&t1);
  if (ret) rcc_error_print(stdout, ret);
  time_so_far = ts_duration(t0, t1);
  float pre_ecr_deadtime_musec = static_cast<float>(m_pre_ecr_deadtime_bc)*0.025;
  u_int time_to_wait_musec = static_cast<u_int>(pre_ecr_deadtime_musec - 1000000.*time_so_far);
  ERS_DEBUG(2, "Wait-time before ECR = " << time_to_wait_musec << " musec");
  if ( (time_to_wait_musec > pre_ecr_deadtime_musec) || (time_to_wait_musec <=0) ) {
    ERS_LOG( "Wait-time before ECR = " << time_to_wait_musec << " musec; Waited already later than 1 millisecond, therefore don't wait");
    time_to_wait_musec = 0;
  }
  ret = ts_delay(time_to_wait_musec);
  if (ret) rcc_error_print(stdout, ret);

  this->readCounter();

  // reset event counter value
  vme_ret_code |= m_ltp->COUNTER_reset();

  
  ERS_LOG("Issuing ECR now! Event counter is already reset and system should be busy.");
  // issue ecr
  m_ltp->IO_sw_pulse(RCD::LTP::BG1);
  ret = ts_clock(&t0);
  ++m_necr;

  // wait post-ecr deadtime
  ret = ts_clock(&t1);
  if (ret) rcc_error_print(stdout, ret);
  
  // duration in nanoseconds
  time_so_far = ts_duration(t0, t1);
  float post_ecr_deadtime_musec = static_cast<float>(m_post_ecr_deadtime_bc)*0.025;
  time_to_wait_musec = static_cast<u_int>(post_ecr_deadtime_musec - 1000000.*time_so_far);
  ERS_DEBUG(2, "Wait-time after ECR = " << time_to_wait_musec << " musec");
  if ( (time_to_wait_musec > post_ecr_deadtime_musec) || (time_to_wait_musec <=0) ) {
    ERS_LOG( "Wait-time after ECR = " << time_to_wait_musec << " musec; Waited already later than 1 millisecond, therefore don't wait");
    time_to_wait_musec = 0;
  }
  ret = ts_delay(time_to_wait_musec);
  if (ret) rcc_error_print(stdout, ret);

  ret = ts_close(TS_DUMMY);
  if (ret) rcc_error_print(stdout, ret);

  // release busy
  m_mb->unsetECRBusy();
  m_ecr_in_progress = false;
  m_mutex.unlock();

  this->publishToIS();
  return;
}


// returns true, if the class is in the progress of issueing an ECR
bool LTPMasterECR::ECRInProgress() {
  bool ret(false);
  m_mutex.lock();
  ret = m_ecr_in_progress;
  m_mutex.unlock();
  return(ret);
}

void LTPMasterECR::startAutomaticECR() {
  m_mutex.lock();
  this->startTimer();
  m_automatic_mode = true;
  m_mutex.unlock();
  return;
}

void LTPMasterECR::stopAutomaticECR() {
  m_mutex.lock();
  m_automatic_mode = false;
  m_mutex.unlock();
  return;
}

bool LTPMasterECR::isInAutomaticMode() {
  bool ret(false);
  m_mutex.lock();
  ret = m_automatic_mode;
  m_mutex.unlock();
  return(ret);
}

uint32_t LTPMasterECR::numberOfECRs() {
  uint32_t necr(0);
  m_mutex.lock();
  necr=m_necr;
  m_mutex.unlock();
  return(necr);
}

void LTPMasterECR::resetNumberOfECRs(uint32_t necr) {
  m_mutex.lock();
  m_necr = necr;
  m_mutex.unlock();
  return;
}

void LTPMasterECR::reset() {
  m_mutex.lock();
  m_lbn_list.clear();
  m_l1id_list.clear();
  m_nl1a = 0;
  m_last_ec = 0;
  m_necr = 0;
  m_latest_l1id = 0x00ffffff;
  m_lbn = 0;
  m_mutex.unlock();
}

void LTPMasterECR::setDeadtime(uint32_t pre_ecr_deadtime_bc, uint32_t post_ecr_deadtime_bc) {
  // cout << "Setting deadtime ";
  m_mutex.lock();
  m_pre_ecr_deadtime_bc = pre_ecr_deadtime_bc;
  m_post_ecr_deadtime_bc = post_ecr_deadtime_bc;
  m_mutex.unlock();
  // cout << "done." << endl;
  return;
}

void LTPMasterECR::readCounter() {
  // read latest event counter value
  u_int ec(0);
  u_int vme_ret_code = m_ltp->COUNTER_read_32(&ec);
  if (vme_ret_code != 0) {
    char help[1000];
    sprintf(help, "return code = %d (0x%08x)", vme_ret_code, vme_ret_code);
    const std::string reason = help;
    throw LTP::LTPModule::VMEError(ERS_HERE, " LTPMasterECR::pollL1ID()", reason.c_str());
  }

  // calculate L1ID
  if (ec==0) {
    ec = 0xffffffu;
  } else {
    ec = ec-1;
  }
  uint32_t ecrc = m_necr & 0xff;
  uint32_t l1id = ((ecrc<<24)&0xff000000u) | (ec & 0x00ffffffu);
  if (l1id != m_latest_l1id) {
    m_l1id_list.push_back(l1id);
    m_nl1a += (ec-m_last_ec);
    m_last_ec=ec;
    m_latest_l1id = l1id;
    m_lbn_list.push_back(m_lbn);
  }
}

void LTPMasterECR::pollL1ID() {
  // cout << "LTPMasterECR::pollL1ID() ";
  m_mutex.lock();
  this->readCounter();
  m_mutex.unlock();
  // cout << "done." << endl;
  return;
}

void LTPMasterECR::getL1IDList(std::vector<uint32_t>& l1id_list, std::vector<uint32_t>& lbn_list) {
  m_mutex.lock();
  l1id_list = m_l1id_list;
  lbn_list = m_lbn_list;
  m_l1id_list.clear();
  m_lbn_list.clear();
  m_mutex.unlock();
  return;
}

void LTPMasterECR::resetL1IDList() {
  m_mutex.lock();
  m_l1id_list.clear();
  m_latest_l1id = 0x00ffffff;
  m_lbn_list.clear();
  m_lbn=0;
  m_mutex.unlock();
  return;
}

size_t LTPMasterECR::getSizeL1IDList() {
  size_t s(0);
  m_mutex.lock();
  s = m_l1id_list.size();
  m_mutex.unlock();
  return(s);
}


void LTPMasterECR::getLatestL1ID(uint32_t& l1id, uint32_t& lbn) {
  m_mutex.lock();
  l1id = m_latest_l1id;
  lbn = m_lbn;
  m_mutex.unlock();
  return;
}

void LTPMasterECR::reactTo() {
  bool automode(false);
  m_mutex.lock();
  automode=m_automatic_mode;
  m_mutex.unlock();
  if (automode) {
    this->issueECR();
  }
  
  this->publishToIS();

  return;
}

void LTPMasterECR::publishToIS() {

  string sout("LTPMasterECR::publishToIS() ");
  try {
    m_mutex.lock();
    ECR is_entry;
    uint32_t necr = m_necr;
    is_entry.ECRCounter = necr;
    is_entry.ECRCounter8Bit = static_cast<unsigned char>(necr&0xff);
    is_entry.LatestL1ID = m_latest_l1id;
    m_mutex.unlock();
    ISInfoDictionary dict(m_ipcpartition);
    dict.checkin("RunParams.ECR", is_entry, true);
  } catch (CORBA::SystemException& ex) {
    ERS_LOG(sout << "Could not publish to IS: CORBA::SystemException ex._name()=" << ex._name());

    throw LTP::LTPModule::LTPModulePublishToIS(ERS_HERE, "LTPMasterECR::publishToIS() ", ex._name());

  } catch (daq::is::Exception & ex) {
    ERS_LOG(sout << "Could not publish to IS: daq::is::Exception ex.what()=" << ex.what());
    throw LTP::LTPModule::LTPModulePublishToIS(ERS_HERE, "LTPMasterECR::publishToIS() ", ex.what());
  } catch (std::exception& ex) {
    ERS_LOG(sout << "Could not publish to IS: std::exception ex.what()=" << ex.what());
    throw LTP::LTPModule::LTPModulePublishToIS(ERS_HERE, "LTPMasterECR::publishToIS() ", ex.what());
  } catch (...) {
    ERS_LOG(sout << "Could not publish to IS. Unknown exception. Rethrowing.");
    throw LTP::LTPModule::LTPModulePublishToIS(ERS_HERE, "LTPMasterECR::publishToIS() ", "Unknown Exception");
  }

  return;
}

void LTPMasterECR::setLBN(uint32_t lbn) {
  m_mutex.lock();
  m_lbn = lbn;
  m_mutex.unlock();
}
