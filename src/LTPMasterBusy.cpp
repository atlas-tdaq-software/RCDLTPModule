#include "RCDLTPModule/Exceptions.h"
#include "RCDLTPModule/LTPMasterBusy.h"
#include "RCDLtp/RCDLtp.h"

#include "is/info.h"
#include "is/infodictionary.h"
#include "TTCInfo/GLOBALBUSY.h"

LTPMasterBusy::LTPMasterBusy(IPCPartition& p ) 
  : m_ltp(nullptr),
    m_ipcpartition(p),
    m_bc_trigger(true),
    m_bc_runcontrol(1),
    m_bc_lumiblock(false),
    m_bc_ecr(false),
    m_bc_hltcounter(false),
    m_bc_hltsv(false),
    m_globalbusy(true)
{ 
  this->reset();
}

LTPMasterBusy::LTPMasterBusy(RCD::LTP& ltp,
			     IPCPartition& p ) 
  : m_ltp(&ltp),
    m_ipcpartition(p),
    m_bc_trigger(false),
    m_bc_runcontrol(1),
    m_bc_lumiblock(false),
    m_bc_ecr(false),
    m_bc_hltcounter(false),
    m_bc_hltsv(false),
    m_globalbusy(true)
{ 
  this->reset();
}

LTPMasterBusy::~LTPMasterBusy() {

}

void LTPMasterBusy::setLTP(RCD::LTP& ltp) {
  m_mutex.lock();
  m_ltp = &ltp;
  m_mutex.unlock();
}

void LTPMasterBusy::reset() {
  m_mutex.lock();
  // set trigger busy to true to be sure triggers a blocked at the start
  m_bc_trigger=true;
  m_bc_runcontrol=1;
  m_bc_lumiblock=false;
  m_bc_ecr=false;
  m_bc_hltcounter=false;
  m_bc_hltsv=false;
  m_globalbusy=true;

  this->updateGlobalBusy();
  this->publishToIS();
  m_mutex.unlock();
}

void LTPMasterBusy::setTriggerBusy() {
  m_mutex.lock();
  m_bc_trigger = true;
  this->updateGlobalBusy();
  this->publishToIS();
  m_mutex.unlock();
  return;
}

void LTPMasterBusy::unsetTriggerBusy() {
  m_mutex.lock();
  m_bc_trigger = false;
  this->updateGlobalBusy();
  this->publishToIS();
  m_mutex.unlock();
  return;
}

void LTPMasterBusy::setRunControlBusy() {
  m_mutex.lock();
  ++m_bc_runcontrol;
  this->updateGlobalBusy();
  this->publishToIS();
  m_mutex.unlock();
  return;
}

void LTPMasterBusy::setRunControlBusy(int counter) {
  m_mutex.lock();
  m_bc_runcontrol = counter;
  this->updateGlobalBusy();
  this->publishToIS();
  m_mutex.unlock();
  return;
}

void LTPMasterBusy::unsetRunControlBusy() {
  m_mutex.lock();
  if (m_bc_runcontrol>0) {
    --m_bc_runcontrol;
  }
  this->updateGlobalBusy();
  this->publishToIS();
  m_mutex.unlock();
  return;
}


void LTPMasterBusy::setLumiBlockBusy() {
  m_mutex.lock();
  m_bc_lumiblock = true;
  this->updateGlobalBusy();
  this->publishToIS();
  m_mutex.unlock();
  return;
}

void LTPMasterBusy::unsetLumiBlockBusy() {
  m_mutex.lock();
  m_bc_lumiblock = false;
  this->updateGlobalBusy();
  this->publishToIS();
  m_mutex.unlock();
  return;
}

void LTPMasterBusy::setECRBusy() {
  m_mutex.lock();
  m_bc_ecr = true;
  this->updateGlobalBusy();
  this->publishToIS();
  m_mutex.unlock();
  return;
}

void LTPMasterBusy::unsetECRBusy() {
  m_mutex.lock();
  m_bc_ecr = false;
  this->updateGlobalBusy();
  this->publishToIS();
  m_mutex.unlock();
  return;
}

void LTPMasterBusy::setHLTCounterBusy() {
  m_mutex.lock();
  m_bc_hltcounter = true;
  this->updateGlobalBusy();
  this->publishToIS();
  m_mutex.unlock();
  return;
}

void LTPMasterBusy::unsetHLTCounterBusy() {
  m_mutex.lock();
  m_bc_hltcounter = false;
  this->updateGlobalBusy();
  this->publishToIS();
  m_mutex.unlock();
  return;
}

void LTPMasterBusy::setHLTSVBusy() {
  m_mutex.lock();
  m_bc_hltsv = true;
  this->updateGlobalBusy();
  this->publishToIS();
  m_mutex.unlock();
  return;
}

void LTPMasterBusy::unsetHLTSVBusy() {
  m_mutex.lock();
  m_bc_hltsv = false;
  this->updateGlobalBusy();
  this->publishToIS();
  m_mutex.unlock();
  return;
}

void LTPMasterBusy::updateGlobalBusy() {
  // this method should only be called by other methods locking m_mutex
  // this means also, that this method should not use m_mutex nor call
  // other methods that use m_mutex
  m_globalbusy = m_bc_trigger || 
    m_bc_runcontrol || 
    m_bc_lumiblock || 
    m_bc_ecr || 
    m_bc_hltcounter ||
    m_bc_hltsv;
  unsigned int ret(0);
  if (m_globalbusy) {
    ret = m_ltp->BUSY_enable_constant_level();
  } else {
    ret =m_ltp->BUSY_disable_constant_level();
  }
  if (ret!=0) {
    char help[1000];
    sprintf(help, "return code = %d (0x%08x)", ret, ret);
    const std::string reason = help;
    throw LTP::LTPModule::VMEError(ERS_HERE, "LTPMasterBusy::updateGlobalBusy()", reason.c_str());
  }
  return;
}

bool LTPMasterBusy::isBusy() {
  m_mutex.lock();
  bool globalbusy = m_globalbusy;
  m_mutex.unlock();
  return(globalbusy);
}

bool LTPMasterBusy::isTriggerBusy() {
  m_mutex.lock();
  bool trigger = m_bc_trigger;
  m_mutex.unlock();
  return(trigger);
}


uint32_t LTPMasterBusy::isRunControlBusy() {
  m_mutex.lock();
  uint32_t runcontrol = m_bc_runcontrol;
  m_mutex.unlock();
  return(runcontrol);
}

bool LTPMasterBusy::isLumiBlockBusy() {
  m_mutex.lock();
  bool lumiblock = m_bc_lumiblock;
  m_mutex.unlock();
  return(lumiblock);
}

bool LTPMasterBusy::isECRBusy() {
  m_mutex.lock();
  bool ecr = m_bc_ecr;
  m_mutex.unlock();
  return(ecr);
}

bool LTPMasterBusy::isHLTCounterBusy() {
  m_mutex.lock();
  bool hltcounter = m_bc_hltcounter;
  m_mutex.unlock();
  return(hltcounter);
}

bool LTPMasterBusy::isHLTSVBusy() {
  m_mutex.lock();
  bool hltsv = m_bc_hltsv;
  m_mutex.unlock();
  return(hltsv);
}

bool LTPMasterBusy::isBusy(bool& trigger,
			   uint32_t& runcontrol, 
			   bool& lumiblock, 
			   bool& ecr,
			   bool& hltcounter,
			   bool& hltsv) {
  m_mutex.lock();
  trigger    = m_bc_trigger   ;
  runcontrol = m_bc_runcontrol;
  lumiblock  = m_bc_lumiblock ;
  ecr        = m_bc_ecr       ;
  hltsv      = m_bc_hltsv;
  bool globalbusy = m_globalbusy;
  m_mutex.unlock();
  return(globalbusy);
}
		      

void LTPMasterBusy::publishToIS() {
  // this method should only be called by other methods locking m_mutex
  // this means also, that this method should not use m_mutex nor call
  // other methods that use m_mutex
  string sout("LTPMasterBusy::publishToIS() ");
  try {
    GLOBALBUSY is_entry;
    is_entry.MasterTriggerBusy = m_bc_trigger;
    is_entry.RunControlBusy    = m_bc_runcontrol;
    is_entry.LumiBlockBusy     = m_bc_lumiblock;
    is_entry.ECRBusy           = m_bc_ecr;
    is_entry.HLTCounterBusy    = m_bc_hltcounter;
    is_entry.HLTSVBusy         = m_bc_hltsv;

    ISInfoDictionary dict(m_ipcpartition);
    dict.checkin("RunParams.GlobalBusy", is_entry, true);
  } catch (CORBA::SystemException& ex) {
    ERS_LOG(sout << "Could not publish to IS: CORBA::SystemException ex._name()=" << ex._name());

    throw LTP::LTPModule::LTPModulePublishToIS(ERS_HERE, "LTPMasterBusy::publishToIS() ", ex._name());

  } catch (daq::is::Exception & ex) {
    ERS_LOG(sout << "Could not publish to IS: daq::is::Exception ex.what()=" << ex.what());
    throw LTP::LTPModule::LTPModulePublishToIS(ERS_HERE, "LTPMasterBusy::publishToIS() ", ex.what());
  } catch (std::exception& ex) {
    ERS_LOG(sout << "Could not publish to IS: std::exception ex.what()=" << ex.what());
    throw LTP::LTPModule::LTPModulePublishToIS(ERS_HERE, "LTPMasterBusy::publishToIS() ", ex.what());
  } catch (...) {
    ERS_LOG(sout << "Could not publish to IS. Unknown exception. Rethrowing.");
    throw LTP::LTPModule::LTPModulePublishToIS(ERS_HERE, "LTPMasterBusy::publishToIS() ", "Unknown Exception");
  }

  return;
}


bool LTPMasterBusy::ltpConstantBusyAsserted() {
  bool ret(false);

  m_mutex.lock();
  ret = m_ltp->BUSY_constant_level_is_asserted();
  m_mutex.unlock();

  return(ret);
}

void LTPMasterBusy::dump() {
  std::string sout("LTPMasterBusy::dump() ");
  ERS_LOG(sout << "OR of all busies    : " << (this->isBusy()? "busy" : "not busy"));
  ERS_LOG(sout << " - trigger busy     : " << (this->isTriggerBusy()? "busy" : "not busy"));
  ERS_LOG(sout << " - run control busy : " << (this->isRunControlBusy()? "busy" : "not busy"));
  ERS_LOG(sout << " - lumi block busy  : " << (this->isLumiBlockBusy()? "busy" : "not busy"));
  ERS_LOG(sout << " - ECR busy         : " << (this->isECRBusy()? "busy" : "not busy"));
  ERS_LOG(sout << " - HLTCounter busy  : " << (this->isHLTCounterBusy()? "busy" : "not busy"));
  ERS_LOG(sout << " - HLTSV busy       : " << (this->isHLTSVBusy()? "busy" : "not busy"));
  return;
}
